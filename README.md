# My GG Survey
##
GG Survey adalah aplikasi mobile yang menghandle survey marketing campaign activity event yang di tanyakan ke pengunjung. misalnya di event music, dsb. aplikasi ini di install di tablet yang di bawa-bawa oleh SPG pada saat campaign event untuk menangkap response dari pengunjung terhadap awareness product dan event yang di adakan.
aplikasi ini bisa dijalankan offline (tanpa harus terkoneksi ke jaringan). sebelumnya daftar pertanyaan dan master event akan di download terlebih dahulu di device baru di gunakan.
 
## Screen Example 
login <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myggsurvey/myggsurvey_login.png?raw=true)
Pilih Event yang berlangsung <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myggsurvey/myggsurvey_event_list.png?raw=true)
download daftar pertanyaan <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myggsurvey/myggsurvey_menu.png?raw=true)
memulai survey <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myggsurvey/myggsurvey_start.png?raw=true)
Contoh pertanyaan <br>
![alt text](https://gitlab.com/mulyo.lucky29/images/-/raw/main/myggsurvey/myggsurvey_example.png?raw=true)

## License
**Copyright 2022 Lucky Mulyo**

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.