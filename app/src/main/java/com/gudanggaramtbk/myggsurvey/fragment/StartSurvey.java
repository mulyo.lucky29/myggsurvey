package com.gudanggaramtbk.myggsurvey.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.core.app.NavUtils;
import androidx.fragment.app.Fragment;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterTextbox;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.model.CSurveySet;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import java.util.List;


public class StartSurvey extends Fragment implements View.OnClickListener{
    Button          Cmd_Start_Survey;
    Button          Cmd_Cancel_Survey;
    Activity        pActivity;
    Context         ctx;
    MySQLiteHelper  odbHelper;
    CEvent          oCParamEvent;
    TextView        Txt_ssv_open_naration;
    TextView        Txt_ssv_survey_tag;
    TextView        Txt_ssv_event_id;
    TextView        Txt_ssv_event_name;
    TextView        Txt_ssv_event_desc;
    TextView        Txt_ssv_event_start_date;
    TextView        Txt_ssv_event_end_date;
    TextView        Txt_ssv_attribute1;
    TextView        Txt_ssv_attribute2;
    TextView        Txt_ssv_attribute3;
    TextView        Txt_ssv_attribute4;
    String          strOpeningNaration;
    String          strClosingNaration;
    String          strNotValidNaration;
    String          strDeviceID;

    String          StrSessionName;
    String          StrEventArea;
    Bundle          bundleParam;

    public StartSurvey(MySQLiteHelper dbHelper, CEvent CParamEvent){
        odbHelper     = dbHelper;
        oCParamEvent  = CParamEvent;
    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        CustomListAdapterTextbox adapter;
        View view = inflater.inflate(R.layout.fragment_start_survey, container, false);
        String strOpeningOverride;

        pActivity = getActivity();
        ctx  = pActivity.getApplicationContext();

        // get bundle param sent from TakeSurveyActivity
        bundleParam = this.getArguments();
        StrSessionName = bundleParam.getString("PIC_NAME");
        StrEventArea   = bundleParam.getString("EVENT_AREA");
        strDeviceID    = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);

        Log.d("[GudangGaram]", "StartSurvey :: strDeviceID = " +  strDeviceID);


        Txt_ssv_open_naration    = (TextView) view.findViewById(R.id.txt_ssv_open_naration);
        Txt_ssv_survey_tag       = (TextView) view.findViewById(R.id.txt_ssv_survey_tag);
        Txt_ssv_event_id         = (TextView) view.findViewById(R.id.txt_ssv_event_id);
        Txt_ssv_event_name       = (TextView) view.findViewById(R.id.txt_ssv_event_name);
        Txt_ssv_event_desc       = (TextView) view.findViewById(R.id.txt_ssv_event_desc);
        Txt_ssv_event_start_date = (TextView) view.findViewById(R.id.txt_ssv_event_start_date);
        Txt_ssv_event_end_date   = (TextView) view.findViewById(R.id.txt_ssv_event_end_date);
        Txt_ssv_attribute1       = (TextView) view.findViewById(R.id.txt_ssv_attribute1);
        Txt_ssv_attribute2       = (TextView) view.findViewById(R.id.txt_ssv_attribute2);
        Txt_ssv_attribute3       = (TextView) view.findViewById(R.id.txt_ssv_attribute3);
        Txt_ssv_attribute4       = (TextView) view.findViewById(R.id.txt_ssv_attribute4);

        Cmd_Start_Survey  = (Button) view.findViewById(R.id.cmd_ssv_start_survey);
        Cmd_Cancel_Survey = (Button) view.findViewById(R.id.cmd_ssv_cancel_survey);

        Cmd_Cancel_Survey.setOnClickListener(this);
        Cmd_Start_Survey.setOnClickListener(this);

        List<CSurveySet> oSurveySet = odbHelper.GetSurveySet(oCParamEvent.getEventSurveyTag());
        strOpeningNaration  = oSurveySet.get(0).getOpeningNaration().replace("$presenter_name",StrSessionName);
        strClosingNaration  = oSurveySet.get(0).getClosingNaration();
        strNotValidNaration = oSurveySet.get(0).getNotValidNaration();

        Log.d("[GudangGaram]", "StartSurvey :: strNotValidNaration > " + strNotValidNaration);


        oCParamEvent.setParam_Opening_Naration(strOpeningNaration);
        oCParamEvent.setParam_Closing_Naration(strClosingNaration);
        oCParamEvent.setParam_NotValid_Naration(strNotValidNaration);

        Txt_ssv_open_naration.setText(strOpeningNaration);
        Txt_ssv_survey_tag.setText(oCParamEvent.getEventSurveyTag());
        Txt_ssv_event_id.setText(oCParamEvent.getEventID());
        Txt_ssv_event_name.setText(oCParamEvent.getEventName());
        Txt_ssv_event_desc.setText(oCParamEvent.getEventDescription());
        Txt_ssv_event_start_date.setText(oCParamEvent.getEventStartDate());
        Txt_ssv_event_end_date.setText(oCParamEvent.getEventEndDate());
        Txt_ssv_attribute1.setText(oCParamEvent.getAttribute1());
        Txt_ssv_attribute2.setText(oCParamEvent.getAttribute2());
        Txt_ssv_attribute3.setText(oCParamEvent.getAttribute3());
        Txt_ssv_attribute4.setText(oCParamEvent.getAttribute4());

        return view;
    }

    @Override
    public void onClick (View v){
        switch (v.getId()) {
            case R.id.cmd_ssv_start_survey:
                EH_CMD_START_SURVEY();
                break;
            case R.id.cmd_ssv_cancel_survey:
                EH_CMD_OUT_FROM_SURVEY();
                break;
        }
    }

    private void EH_CMD_OUT_FROM_SURVEY(){
        try{
            Intent upIntent = NavUtils.getParentActivityIntent(pActivity);
            // send intent parameter to mainactivity
            upIntent.putExtra("PIC_NAME", StrSessionName);
            upIntent.putExtra("EVENT_AREA", StrEventArea);
            NavUtils.navigateUpTo(pActivity, upIntent);
        }
        catch(Exception e){
        }
    }

    private void EH_CMD_START_SURVEY(){
        Log.d("[GudangGaram]", "StartSurvey :: EH_CMD_START_SURVEY");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(pActivity);
        alertDialogBuilder
                .setTitle("Konfirmasi Start Survey")
                .setMessage("Apakah Anda Akan Memulai Survey Ini ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                EH_CMD_DO_START();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
        Log.d("[GudangGaram]", "StartSurvey :: EH_CMD_START_SURVEY END");
    }

    private void EH_CMD_DO_START(){
        // go to fragment Q1 .. N
        // log start time
        Log.d("[GudangGaram]", "StartSurvey :: EH_CMD_DO_START");
        try{
            // go to fragment PReuse
            Fragment freuse = new PReuse(odbHelper, oCParamEvent, strDeviceID);
            // passing bundleParam to PReuse
            freuse.setArguments(bundleParam);
            moveToFragment(freuse);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "StartSurvey :: EH_CMD_DO_START Exception :" + e.getMessage().toString());
        }
    }

    private void moveToFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }
}