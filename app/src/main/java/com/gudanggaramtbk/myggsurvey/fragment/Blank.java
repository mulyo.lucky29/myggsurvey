package com.gudanggaramtbk.myggsurvey.fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.gudanggaramtbk.myggsurvey.R;


public class Blank extends Fragment implements View.OnClickListener{
    public Blank(){
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        return view;
    }

    @Override
    public void onClick (View v){
    }

    private void moveToFragment(Fragment fragment) {
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
    }
}