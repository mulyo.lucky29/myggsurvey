package com.gudanggaramtbk.myggsurvey.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterCheckBoxGroup;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterHRadioGroup;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterMRadioGroup;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterRadioGroup;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterTextbox;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.model.CSelectedAnswer;
import com.gudanggaramtbk.myggsurvey.model.CSurveyD;
import com.gudanggaramtbk.myggsurvey.model.CSurveyH;
import com.gudanggaramtbk.myggsurvey.util.ConstructAnswer;
import com.gudanggaramtbk.myggsurvey.util.General;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.util.PreLoadData;
import com.gudanggaramtbk.myggsurvey.util.QueueInnerLoop;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PReuse extends Fragment implements View.OnClickListener{
        Button              fragment_btn_prev;
        Button              fragment_btn_next;
        RecyclerView        content_answer;
        //ListView            content_answer;
        Activity            pActivity;
        Context             ctx;
        List<CAnswerOption> list_answer_set;
        List<CAnswerOption> RSlist_answer_set;
        TextView            SectionLabel;
        TextView            InterruptInfo;
        TextView            Question;
        TextView            QuestionHint;
        WebView             QuestionHintWbv;
        TextView            ErrorMessage;
        String              strQuestion;
        String              strQuestionHint;
        String              strErrorMessage;
        String              strSectionLabel;
        String              strInterruptInfo;
        Integer             intInterruptDuration;
        String              strInterruptReplaceAble;
        String              AnswerType;
        String              InterruptDuration;
        String              DefaultNext;
        String              CurrQtag;
        PreLoadData         pd;
        CSelectedAnswer     sa;
        QueueInnerLoop      innerloop;
        String              innerloop_block_next;
        String              Start_Date_Per_Question;
        String              Curr_Date_Per_Question;
        String              End_Date_Per_Question;
        CSurveyH            tempAnswerH;
        List<CSurveyD>      tempAnswerD;
        String              StrDeviceID;
        String              StrSessionID;
        String              StrSurveyTag;
        Integer             Seq_No;
        General             general;
        MySQLiteHelper      odbHelper;
        String              oStrSurveyTag;
        CEvent              oParamEvent;
        LinearLayout        layPrImage;
        LinearLayout        layPrLoading;
        LinearLayout        layPrButton;
        LinearLayout        layPrTitle;
        LinearLayout        layPrHint;
        LinearLayout        layPrMessage;
        LinearLayout        layPrAnswer;
        ProgressBar         PrProgessBar;
        String StrTagSelected;
        String StrSessionName;
        String StrEventArea;
        Bundle bundleParam;
        String html_hint_opening;
        String html_hint_closing;
        String CoherenceWith;
        Boolean hasQueue;
        String AfterQueueEndGoTo;

        public PReuse(MySQLiteHelper dbHelper, CEvent CParamEvent, String ostrDeviceID) {
                Log.d("[GudangGaram]", "PReuse : SurveyTag (" + CParamEvent.getEventSurveyTag() + ")");

                String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
                odbHelper       = dbHelper;
                oStrSurveyTag   = CParamEvent.getEventSurveyTag();
                // initial current position Question Tag
                CurrQtag                = "Q1";
                oParamEvent             = CParamEvent;
                pd                      = new PreLoadData(dbHelper, oStrSurveyTag);
                sa                      = new CSelectedAnswer();
                innerloop               = new QueueInnerLoop();
                innerloop.setParameter(odbHelper, oStrSurveyTag);

                general                 = new General();
                Seq_No                  = 1;
                Start_Date_Per_Question = sysdate;
                Curr_Date_Per_Question  = sysdate;
                html_hint_opening       = "<html><body><p style='font-size:55px;'>";
                html_hint_closing       = "</p></body></html>";
                StrTagSelected          = "";

                // -------- init for temp selected answer Header & Detail -----------
                StrDeviceID   = ostrDeviceID;
                StrSurveyTag  = pd.GetSurveyTagOfQuestionByQTag(CurrQtag);

                StrSessionID = general.md5(StrDeviceID + "-" + oParamEvent.getInterviewerName() + sysdate);
                Log.d("[GudangGaram]", "PReuse :: SessionID Unencrypted > " + StrDeviceID + "-" + oParamEvent.getInterviewerName() + sysdate);
                Log.d("[GudangGaram]", "PReuse :: SessionID Encrypted   > " + StrSessionID);

            tempAnswerD = new ArrayList<CSurveyD>();
                // save header session
                tempAnswerH = new CSurveyH();
                tempAnswerH.setSurveyHID(StrSessionID);
                tempAnswerH.setStatus("Rec");
                tempAnswerH.setStart_Date(Start_Date_Per_Question);
                tempAnswerH.setDevice_ID(StrDeviceID);
                tempAnswerH.setSurvey_Tag(StrSurveyTag);
                tempAnswerH.setEventID(oParamEvent.getEventID());
                tempAnswerH.setEventName(oParamEvent.getEventName());
                tempAnswerH.setInterviewerName(oParamEvent.getInterviewerName());
                tempAnswerH.setEventAttribute1(oParamEvent.getAttribute1());
                tempAnswerH.setEventAttribute2(oParamEvent.getAttribute2());
                tempAnswerH.setEventAttribute3(oParamEvent.getAttribute3());
                tempAnswerH.setEventAttribute4(oParamEvent.getAttribute4());

                RSlist_answer_set = new ArrayList<CAnswerOption>();
                hasQueue = false;
                AfterQueueEndGoTo = "";
        }

        @Override
        public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
                View view = inflater.inflate(R.layout.fragment_preuse, container, false);
                pActivity         = getActivity();
                ctx               = pActivity.getApplicationContext();

                // get bundle param sent from fragment StartSurvey
                bundleParam = this.getArguments();
                StrSessionName = bundleParam.getString("PIC_NAME");
                StrEventArea   = bundleParam.getString("EVENT_AREA");

                tempAnswerH.setInterviewerName(StrSessionName);

                layPrImage        = (LinearLayout) view.findViewById(R.id.pr_image);
                layPrLoading      = (LinearLayout) view.findViewById(R.id.pr_loading);
                layPrButton       = (LinearLayout) view.findViewById(R.id.pr_button);
                layPrTitle        = (LinearLayout) view.findViewById(R.id.pr_title);
                layPrHint         = (LinearLayout) view.findViewById(R.id.pr_hint);
                layPrMessage      = (LinearLayout) view.findViewById(R.id.pr_message);
                layPrAnswer       = (LinearLayout) view.findViewById(R.id.pr_answer);
                PrProgessBar      = (ProgressBar)  view.findViewById(R.id.pr_progressbar);

                SectionLabel      = (TextView) view.findViewById(R.id.txt_section_label);
                InterruptInfo     = (TextView) view.findViewById(R.id.txt_interrupt_info);
                Question          = (TextView) view.findViewById(R.id.txt_question_value);
                QuestionHint      = (TextView) view.findViewById(R.id.txt_question_hint);
                QuestionHintWbv   = (WebView)  view.findViewById(R.id.txt_question_hint_wbv);
                ErrorMessage      = (TextView) view.findViewById(R.id.txt_error_message);
                fragment_btn_prev = (Button)   view.findViewById(R.id.cmd_pr_cancel);
                fragment_btn_next = (Button)   view.findViewById(R.id.cmd_pr_next);

                content_answer    = (RecyclerView) view.findViewById(R.id.lvi_answer_set);
                //content_answer    = (ListView) view.findViewById(R.id.lvi_answer_set2);


                WebSettings settings = QuestionHintWbv.getSettings();
                settings.setMinimumFontSize(22);
                settings.setLoadWithOverviewMode(true);
                settings.setUseWideViewPort(true);
                settings.setBuiltInZoomControls(true);
                settings.setSupportZoom(true);

                // --- render initial fragment
                RenderFragment(CurrQtag, view, "", "");

                fragment_btn_prev.setOnClickListener(this);
                fragment_btn_next.setOnClickListener(this);

                return view;
        }

        @Override
        public void onClick (View v){
                switch (v.getId()) {
                        case R.id.cmd_pr_cancel:
                                EH_CMD_CANCEL();
                                break;
                        case R.id.cmd_pr_next:
                                EH_CMD_NEXT(v);
                                break;
                }
        }

        public void RenderFragment(String qTag, View view, String qSelectedTag,String strSelectedTag){
                Log.d("[GudangGaram]", "PReuse :: RenderFragment(" + qTag + ") Parameters : ");
                Log.d("[GudangGaram]", "PReuse :: RenderFragment >> strSelectedTag : " + strSelectedTag);
                Log.d("[GudangGaram]", "PReuse :: RenderFragment >> qSelectedTag   : " + qSelectedTag);

                String StrReplacedQuestion;
                String StrDisplayTag;

                //StrDisplayTag = strSelectedTag.replace(qSelectedTag,qTag);
                //Log.d("[GudangGaram]", "PReuse :: RenderFragment(" + qTag + ") : strDisplayTag  > " + StrDisplayTag);

                if(qTag.length() > 0){
                        // set Question Value
                        strSectionLabel      = pd.GetSectionByQTag(qTag);
                        strInterruptInfo     = pd.GetInterruptInfoByQTag(qTag);
                        intInterruptDuration = pd.GetInterruptDurationByQTag(qTag);
                        strQuestion          = pd.GetQuestionValueByQTag(qTag);
                        strQuestionHint      = pd.GetQuestionHintByQTag(qTag);
                        AnswerType           = pd.GetAnswerTypeOfQuestionByQtag(qTag);
                        list_answer_set      = pd.GetAnswerOptionByQtag(qTag, qSelectedTag, strSelectedTag);
                        innerloop_block_next = pd.GetInnerLoopBlockNextByQtag(qTag).trim();

                        Log.d("[GudangGaram]", "PReuse :: RenderFragment >> list_answer_set.size()   : " + list_answer_set.size());
                        Log.d("[GudangGaram]", "PReuse :: RenderFragment >> innerloop_block_next     : " + innerloop_block_next);
                        if(!innerloop_block_next.equals("")){
                                AfterQueueEndGoTo = innerloop_block_next;
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment >> AfterQueueEndGoTo     : " + AfterQueueEndGoTo);
                        }

                        // render Section label
                        try{
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render Section label");
                                if(strSectionLabel.trim().length() > 0){
                                        SectionLabel.setVisibility(View.VISIBLE);
                                        SectionLabel.setText(strSectionLabel);
                                        SectionLabel.setTextSize(24);
                                }
                                else{
                                        SectionLabel.setVisibility(View.GONE);
                                }
                        } catch(Exception e){
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render Section label Exception :" + e.getMessage().toString());
                        }
                        // render interrupt info label
                        try{
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render interrupt info label");
                                if(strInterruptInfo.trim().length() > 0){
                                    if(strInterruptInfo.contains(getResources().getString(R.string.variable_sign))){
                                        // if contain var signed replace variable with value setup in attribute 1 to 4 in each event
                                        strInterruptReplaceAble = odbHelper.getReplacedSentenceOverVariable(oParamEvent.getEventID(),strInterruptInfo);
                                    }
                                    else{
                                        // else if not then dont search and convert, its just only takes time
                                        strInterruptReplaceAble = strQuestion;
                                    }
                                    InterruptInfo.setText(strInterruptReplaceAble);
                                    InterruptInfo.setTextSize(30);
                                    InterruptInfo.setGravity(Gravity.CENTER_VERTICAL);

                                    if(intInterruptDuration < 0){
                                         // set default as 10 sec if not mentioned (-1)
                                        intInterruptDuration = 10;
                                    }
                                    display_interrupt_screen(intInterruptDuration);
                                }
                        } catch(Exception e){
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render interrupt info label Exception : " + e.getMessage().toString());
                        }
                        // render question label
                        try{
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render question label");
                                if(strQuestion.contains(getResources().getString(R.string.variable_sign))){
                                        // if contain var signed replace variable with value setup in attribute 1 to 4 in each event
                                        StrReplacedQuestion = odbHelper.getReplacedSentenceOverVariable(oParamEvent.getEventID(),strQuestion);
                                        Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render question label > StrReplacedQuestion : " + StrReplacedQuestion);
                                }
                                else{
                                        // else if not then dont search and convert, its just only takes time
                                        StrReplacedQuestion = strQuestion;
                                }
                                Question.setText(StrReplacedQuestion);
                        } catch(Exception e){
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render question label Exception :" + e.getMessage().toString());
                        }
                        // render question hint label
                        try{
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render question hint label");
                                if(strQuestionHint.trim().length() == 0){
                                        QuestionHint.setBackgroundDrawable(null);
                                        QuestionHintWbv.setVisibility(view.GONE);
                                }
                                else{
                                        QuestionHint.setBackgroundColor(Color.GRAY);
                                        QuestionHintWbv.setBackgroundColor(Color.GRAY);

                                        if(strQuestionHint.matches(".*\\<[^>]+>.*")) {
                                                // if contain html tag
                                                if(strQuestionHint.contains("<table>")){
                                                        // hide textview
                                                        QuestionHint.setVisibility(View.GONE);
                                                        QuestionHintWbv.setVisibility(View.VISIBLE);

                                                        //QuestionHintWbv.setMinimumHeight(400);

                                                        // use web view to render
                                                        QuestionHintWbv.setWebViewClient(new WebViewClient());
                                                        QuestionHintWbv.loadDataWithBaseURL("",html_hint_opening + strQuestionHint + html_hint_closing, "text/html", "utf-8","");
                                                        QuestionHintWbv.getSettings().setJavaScriptEnabled(true);
                                                }
                                                else{
                                                        QuestionHint.setVisibility(View.VISIBLE);
                                                        QuestionHintWbv.setVisibility(View.GONE);
                                                        // just use text view render html tag
                                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                                                QuestionHint.setText(Html.fromHtml(strQuestionHint, Html.FROM_HTML_MODE_COMPACT));
                                                        } else {
                                                                QuestionHint.setText(Html.fromHtml(strQuestionHint));
                                                        }
                                                }

                                        }
                                        else{
                                                QuestionHint.setVisibility(View.VISIBLE);
                                                QuestionHintWbv.setVisibility(View.GONE);
                                                QuestionHint.setText(strQuestionHint);
                                        }
                                }
                        } catch(Exception e){
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render question hint label Exception : " + e.getMessage().toString());
                        }
                        // render error message
                        try{
                            Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render error message");
                            ErrorMessage.setText(strErrorMessage);
                        } catch(Exception e){
                            Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render error message Exception : " + e.getMessage().toString());
                        }
                        // render list answer
                        try{
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render list answer");
                                if(list_answer_set != null){
                                        Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render list answer count -> " + list_answer_set.size());
                                        if(list_answer_set.size() > 0) {
                                                // filled Question Option by Question Tag
                                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render list answer -> " + AnswerType);
                                                if (AnswerType.equals("TextBox")) {
                                                        CustomListAdapterTextbox adapter;
                                                        // load custom adapter
                                                        adapter = new CustomListAdapterTextbox(ctx, odbHelper, view, list_answer_set,oParamEvent.getEventID());
                                                        adapter.notifyDataSetChanged();
                                                        RSlist_answer_set = adapter.getList_pilihan();
                                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ctx);
                                                        content_answer.setLayoutManager(layoutManager);
                                                        content_answer.setAdapter(adapter);
                                                }
                                                else if (AnswerType.equals("RadioGroup")) {
                                                        // load custom adapter
                                                        CustomListAdapterRadioGroup adapter;
                                                        adapter = new CustomListAdapterRadioGroup(ctx, odbHelper, view, list_answer_set, oParamEvent.getEventID());
                                                        adapter.notifyDataSetChanged();
                                                        RSlist_answer_set = adapter.getList_pilihan();
                                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ctx);
                                                        content_answer.setLayoutManager(layoutManager);
                                                        content_answer.setAdapter(adapter);
                                                }
                                                else if(AnswerType.equals("HRadioGroup")){
                                                        // load custom adapter
                                                        CustomListAdapterHRadioGroup adapter;
                                                        adapter = new CustomListAdapterHRadioGroup(ctx, odbHelper, view, list_answer_set, oParamEvent.getEventID());
                                                        adapter.notifyDataSetChanged();
                                                        RSlist_answer_set = adapter.getList_pilihan();
                                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ctx);
                                                        content_answer.setLayoutManager(layoutManager);
                                                        content_answer.setAdapter(adapter);
                                                }
                                                else if (AnswerType.equals("CheckBoxGroup")) {
                                                        // load custom adapter
                                                        CustomListAdapterCheckBoxGroup adapter;
                                                        adapter = new CustomListAdapterCheckBoxGroup(ctx, odbHelper, view, list_answer_set, oParamEvent.getEventID());
                                                        adapter.notifyDataSetChanged();
                                                        RSlist_answer_set = adapter.getList_pilihan();
                                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ctx);
                                                        content_answer.setLayoutManager(layoutManager);
                                                        content_answer.setAdapter(adapter);
                                                }
                                                else if (AnswerType.equals("MRadioGroup")) {
                                                        // load custom adapter
                                                        CustomListAdapterMRadioGroup adapter;
                                                        adapter = new CustomListAdapterMRadioGroup(ctx, odbHelper, view, list_answer_set, oParamEvent.getEventID());
                                                        adapter.notifyDataSetChanged();
                                                        RSlist_answer_set = adapter.getList_pilihan();
                                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ctx);
                                                        content_answer.setLayoutManager(layoutManager);
                                                        content_answer.setAdapter(adapter);
                                                }
                                                else{
                                                        Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render list answer -> " + AnswerType);
                                                }
                                        }
                                }
                                else{
                                        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ctx);
                                        content_answer.setLayoutManager(layoutManager);
                                }
                        }
                        catch (Exception e){
                                Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") render list answer Exception : " + e.getMessage().toString());
                        }
                }
                else{
                        Log.d("[GudangGaram]", "PReuse :: RenderFragment (" + qTag + ") Failed qTag Blank");
                }
        }

        private void SaveNodeAnswer(){
                String strQuestionReplacement;
                Log.d("[GudangGaram]", "PReuse :: SaveNodeAnswer");
                try{
                        // get end date as sysdate
                        End_Date_Per_Question   = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());

                        // save any information gathered to CSurveyD object
                        CSurveyD answD = new CSurveyD();
                        answD.setSurveyHID(StrSessionID);
                        answD.setSeq_No(Seq_No.toString());
                        answD.setQuestion_Tag(CurrQtag);
                        try{
                                if(pd.GetQuestionValueByQTag(CurrQtag).contains(getResources().getString(R.string.variable_sign))){
                                        // if contain var signed replace variable with value setup in attribute 1 to 4 in each event
                                        strQuestionReplacement = odbHelper.getReplacedSentenceOverVariable(oParamEvent.getEventID(),strQuestion);
                                }
                                else{
                                        // else if not then dont search and convert, its just only takes time
                                        strQuestionReplacement = strQuestion;
                                }
                                answD.setQuestion_Value(strQuestionReplacement);
                        }
                        catch(Exception e){
                        }
                        answD.setStart_Date(Start_Date_Per_Question);
                        answD.setEnd_Date(End_Date_Per_Question);

                        if(tempAnswerH.getStatus().equals("Cancelled")){
                                // set cancel date
                                answD.setCancel_Date(End_Date_Per_Question);
                                // nullkan jawaban tag
                                answD.setAnswer_Option_Tag("-");
                                // nullkan jawaban value
                                answD.setAnswer_Option_Value("-");
                        }
                        else{
                                answD.setAnswer_Option_Tag(sa.getSelectedTag());
                                answD.setAnswer_Option_Value(sa.getSelectedValue());
                                // if other then fill this attribute for answer others
                                answD.setAttribute1(sa.getSelectedAttribute());
                                Log.d("[GudangGaram]", "PReuse :: SaveNodeAnswer > Other Value :" +  sa.getSelectedAttribute());
                                // construct List<List<CAnswerOption>> from selectedTag
                        }
                        answD.setCreated_Date(End_Date_Per_Question);

                        //answ.setAttribute1();
                        tempAnswerD.add(answD);
                        // end date prev become start date next question
                        Start_Date_Per_Question = End_Date_Per_Question;
                }
                catch(Exception e){
                        Log.d("[GudangGaram]", "PReuse :: SaveNodeAnswer Exception : " + e.getMessage().toString());
                }
        }

        public void EH_CMD_CANCEL(){
            Log.d("[GudangGaram]", "PReuse :: EH_CMD_CANCEL");
            try{
                String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
                tempAnswerH.setStatus("Cancelled");
                tempAnswerH.setCancel_Date(sysdate);
                SaveNodeAnswer();
                // redirect to End Survey Fragment
                EH_CMD_GO_TO_END_SURVEY();
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "PReuse :: EH_CMD_CANCEL Exception :" + e.getMessage().toString());
            }
        }
        private void EH_CMD_NEXT(View v){
                String strTempCurr = "";
                Boolean CheckIsTheLastNode = false;

                Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT");

                //  hide first keyborard if appear previously
                hideKeyboard(pActivity);
                ConstructAnswer ca = new ConstructAnswer(odbHelper,oStrSurveyTag);
                // collect answer from Recycle
                sa = ca.CollectAnswer(content_answer,AnswerType, RSlist_answer_set);
                if(sa == null){
                        Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT : sa is null");
                        blank_answer_handler(AnswerType);
                }
                else {
                        Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT : sa is not null");
                        if(sa.getOtherFlag().trim().equals("Y") && sa.getSelectedAttribute().trim().equals("")){
                                Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT : other value flag detected but not filled text field");
                                blank_answer_handler("OtherSelected");
                        }
                        else {
                                try {
                                        Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT : selected value : " + sa.getSelectedValue());
                                        if (sa.getSelectedValue().length() == 0) {
                                                blank_answer_handler(AnswerType);
                                        } else {
                                                StrTagSelected = sa.getSelectedTag();
                                                display_error(StrTagSelected);
                                                // save node result
                                                SaveNodeAnswer();
                                                // hold current tag before asking next route
                                                strTempCurr = CurrQtag;
                                                // find if its having interloop if so then push to queue
                                                if (!innerloop_block_next.equals("")) {
                                                        if (!(sa.getDenialFlag() == null)) {
                                                                Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT : has innerloop but has denial");
                                                                // ignore innerloop
                                                                //innerloop.push_to_queue(StrTagSelected);
                                                                hasQueue = false;
                                                        } else {
                                                                Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT : has innerloop");
                                                                innerloop.push_to_queue(StrTagSelected);
                                                                hasQueue = true;
                                                        }
                                                } else {
                                                        Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT : has no innerloop");
                                                }
                                                // get next route
                                                CurrQtag = logic_next_route(CurrQtag, sa.getDenialFlag(), sa.getDenialNextQuestion());
                                                Log.d("[GudangGaram]", "PReuse :: logic_next_route result -> " + CurrQtag);
                                                // QE then go to (End of Survey) ------------
                                                if (CurrQtag.equals("QE")) {
                                                        // check if forced end coz not valid answer or its just only end of question
                                                        CheckIsTheLastNode = pd.IsThisNodeTheLastQuestion(strTempCurr);
                                                        if (!CheckIsTheLastNode) {
                                                                Log.d("[GudangGaram]", "PReuse :: logic_next_route result -> EndOfSurvey : Not Valid");
                                                                // override status to no valid answer on header
                                                                tempAnswerH.setStatus("Not Valid");
                                                        }
                                                        // redirect to end survey fragment
                                                        Log.d("[GudangGaram]", "PReuse :: logic_next_route result -> EndOfSurvey");
                                                        EH_CMD_GO_TO_END_SURVEY();
                                                } else {
                                                        Log.d("[GudangGaram]", "PReuse :: logic_next_route result -> RenderFragment(" + CurrQtag + ")");
                                                        // counter sequence question number
                                                        Seq_No = Seq_No + 1;
                                                        RenderFragment(CurrQtag, v, strTempCurr, StrTagSelected);
                                                }
                                        }
                                } catch (Exception e) {
                                        blank_answer_handler(AnswerType);
                                        Log.d("[GudangGaram]", "PReuse :: EH_CMD_NEXT Exception : " + e.getMessage().toString());
                                }
                        }
                }
        }
        private void EH_CMD_GO_TO_END_SURVEY(){
                Log.d("[GudangGaram]", "PReuse :: EH_CMD_GO_TO_END_SURVEY");
                try{
                        // set End Date for header marking
                        String sysdate = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
                        tempAnswerH.setEnd_Date(sysdate);
                        Fragment fend = new EndSurvey(oParamEvent);
                        // send bundleparam to fragment EndSurvey
                        fend.setArguments(bundleParam);
                        ((EndSurvey) fend).set_jawaban(tempAnswerH, tempAnswerD);
                        moveToFragment(fend);
                }
                catch(Exception e){
                        Log.d("[GudangGaram]", "PReuse :: EH_CMD_GO_TO_END_SURVEY Exception :" + e.getMessage());
                }
        }

        public String logic_next_route(String strCurrQtag, String strDenialFlag, String strDenialNextRoute) {
                Log.d("[GudangGaram]", "PReuse :: logic_next_route(" + strCurrQtag + ") : ");
                Log.d("[GudangGaram]", "PReuse :: strDenialFlag      : " + strDenialFlag);
                Log.d("[GudangGaram]", "PReuse :: strDenialNextRoute : " + strDenialNextRoute);

                Integer innerloopcount;
                String next_route = "";
                String next_route_default = "";
                String next_route_override = "";
                String next_route_innerloop = "";
                String next_route_denial = strDenialNextRoute;
                String dequeueNode = "";
                Boolean jump_to_normal_route = false;

                // check queue size
                innerloopcount = innerloop.getQueueSize();
                Log.d("[GudangGaram]", "PReuse :: logic_next_route(" + strCurrQtag + ") has Innerloop " +  innerloop.getQueueLine() + " queue size    : " + innerloopcount);
                if (innerloopcount > 0) {
                        // get first queuers as next route
                        next_route_innerloop = innerloop.getQueueHead();
                        if(next_route_innerloop.equals(strCurrQtag)){
                                jump_to_normal_route = true;
                                Log.d("[GudangGaram]", "PReuse :: logic_next_route : jump_to_normal_route");
                        }
                        else{
                                next_route = next_route_innerloop;
                                jump_to_normal_route = false;
                                Log.d("[GudangGaram]", "PReuse :: logic_next_route : jump_to_innerloop_route > " + next_route);
                        }
                }
                else{
                        if(hasQueue == true){
                                Log.d("[GudangGaram]", "PReuse :: Has Queue Detected But Empty");
                                Log.d("[GudangGaram]", "PReuse :: logic_next_route : jump_to_Innerloop_Block_Next : > " + AfterQueueEndGoTo);
                                next_route = AfterQueueEndGoTo;
                                hasQueue = false;
                                Log.d("[GudangGaram]", "PReuse :: Reset To Become Has No Queue");
                                AfterQueueEndGoTo = "";
                        }
                        else{
                            // check for denial if exist then take denial route next
                            if(!(strDenialFlag == null)){
                                    Log.d("[GudangGaram]", "PReuse :: Has No Queue Detected But Has Denial Flag > " + strDenialFlag);
                                    if(strDenialFlag.trim().equals("Y")){
                                            jump_to_normal_route = false;
                                            next_route = next_route_denial;
                                            Log.d("[GudangGaram]", "PReuse :: logic_next_route :: jump_to_Denial_Next_Route > " + next_route_denial);
                                    }
                            }
                            else{
                                Log.d("[GudangGaram]", "PReuse :: Has No Queue Detected");
                                jump_to_normal_route = true;
                                Log.d("[GudangGaram]", "PReuse :: logic_next_route : jump_to_normal_route");
                            }
                        }
                }

                if(jump_to_normal_route.equals(true)){
                        Log.d("[GudangGaram]", "PReuse :: logic_next_route : use_normal_route");
                        next_route_default   = pd.GetDefaultNextQuestionByQTag(strCurrQtag);
                        try{
                                // get next default route
                                Log.d("[GudangGaram]", "PReuse :: logic_next_route : next_route_default   -> " + next_route_default);
                                // get answer selected (Next Tag Override) if its mentioned then do override from default next route
                                next_route_override = sa.getOverrideNextQuestion();
                                Log.d("[GudangGaram]", "PReuse :: logic_next_route : next_route_override  -> " + next_route_override);
                                if(next_route_override.length() == 0){
                                        Log.d("[GudangGaram]", "PReuse :: logic_next_route : NextOverride Not Mentioned -> Use The Same As next_route_default ");
                                        next_route = next_route_default;
                                }
                                else{
                                        if(!next_route_override.equals(next_route_default)){
                                                Log.d("[GudangGaram]", "PReuse :: logic_next_route : NextOverride Mentioned -> Use next_route_override ");
                                                next_route = next_route_override;
                                        }
                                        else{
                                                Log.d("[GudangGaram]", "PReuse :: logic_next_route : NextOverride Mentioned And Same With next_route_default ");
                                                next_route = next_route_default;
                                        }
                                }
                        }
                        catch(Exception e){
                                next_route = next_route_default;
                                Log.d("[GudangGaram]", "PReuse :: logic_next_route Exception ->" + e.getMessage().toString());
                        }
                }

                try{
                        if(innerloopcount > 0){
                                dequeueNode = pd.GetInnerLoopDequeueNodeByQTag(next_route);
                                Log.d("[GudangGaram]", "PReuse :: Dequeue(" + next_route + ") > " + dequeueNode);
                                if(!(dequeueNode.equals(""))){
                                        Log.d("[GudangGaram]", "PReuse :: Dequeue before > " + innerloop.getQueueLine());
                                        // do dequeue node
                                        String delement = innerloop.dequeue();
                                        Log.d("[GudangGaram]", "PReuse :: Dequeue do     >" + delement);
                                        Log.d("[GudangGaram]", "PReuse :: Dequeue after  > " + innerloop.getQueueLine());

                                }
                        }
                }
                catch(Exception e){
                        Log.d("[GudangGaram]", "PReuse :: Dequeue Exception : " + e.getMessage().toString());
                }

                return next_route;
        }


        public void display_error(final String errm){
                new CountDownTimer(2000, 1000) {
                        public void onTick(long millisUntilFinished) {
                                ErrorMessage.setText(errm);

                        }
                        public void onFinish() {
                                ErrorMessage.setText("");
                        }
                }.start();
        }

        private void blank_answer_handler(String strAnswerType) {
                Log.d("[GudangGaram]", "PReuse :: blank_answer_handler");
                try {
                        if (strAnswerType.equals("TextBox")) {
                                display_error("Isian harap Di Isi");
                        }
                        else if (strAnswerType.equals("RadioGroup")) {
                                display_error("Silahkan di pilih Salah Satu");
                        }
                        else if (strAnswerType.equals("CheckBoxGroup")) {
                                display_error("Silahkan di pilih Paling Tidak Satu");
                        }
                        else if(strAnswerType.equals("MRadioGroup")) {
                                display_error("Harus Di Isi per butir pertanyaannya");
                        }
                        else if(strAnswerType.equals("OtherSelected")){
                                display_error("Other Value Harus diisi");
                        }
                        else{
                                display_error("Tipe Jawaban Tidak Terdefinisi");
                        }
                } catch (Exception e) {
                        Log.d("[GudangGaram]", "PReuse :: blank_answer_handler Exception : " + e.getMessage().toString());
                }
        }

        private void moveToFragment(Fragment fragment) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_main, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
        }

        public static void hideKeyboard(Activity activity) {
                InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                // check if no view has focus:
                View currentFocusedView = activity.getCurrentFocus();
                if (currentFocusedView != null) {
                        inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                }
        }

        private void display_interrupt_screen(Integer delaySec){
                Log.d("[GudangGaram]", "PReuse :: display_interrupt_screen");
                try{
                        new CountDownTimer(delaySec * 1000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                        Log.d("[GudangGaram]", "PReuse :: display_interrupt_screen : onTick");
                                        show_interrupt_screen();
                                }
                                public void onFinish() {
                                        Log.d("[GudangGaram]", "PReuse :: display_interrupt_screen : onFinish");
                                        hide_interrupt_screen();
                                }
                        }.start();
                }
                catch(Exception e){
                        Log.d("[GudangGaram]", "PReuse :: display_interrupt_screen Exception : " + e.getMessage());
                }
        }

        private void show_interrupt_screen(){
                Log.d("[GudangGaram]", "PReuse :: show_interrupt_screen : ");
                try{
                        layPrImage.setVisibility(View.VISIBLE);
                        layPrImage.setGravity(Gravity.CENTER_VERTICAL);
                        layPrLoading.setVisibility(View.GONE);

                        SectionLabel.setVisibility(View.GONE);
                        InterruptInfo.setVisibility(View.VISIBLE);

                        layPrButton.setVisibility(View.GONE);
                        layPrTitle.setVisibility(View.GONE);
                        layPrHint.setVisibility(View.GONE);
                        layPrMessage.setVisibility(View.GONE);
                        layPrAnswer.setVisibility(View.GONE);
                }
                catch(Exception e){
                        Log.d("[GudangGaram]", "PReuse :: show_interrupt_screen Exception : " + e.getMessage());
                }
        }

        private void hide_interrupt_screen(){
                Log.d("[GudangGaram]", "PReuse :: hide_interrupt_screen : ");
                try{
                        layPrImage.setVisibility(View.GONE);
                        SectionLabel.setVisibility(View.GONE);
                        layPrLoading.setVisibility(View.GONE);
                        InterruptInfo.setVisibility(View.GONE);
                        layPrLoading.setVisibility(View.GONE);
                        layPrButton.setVisibility(View.VISIBLE);
                        layPrTitle.setVisibility(View.VISIBLE);
                        layPrHint.setVisibility(View.VISIBLE);
                        layPrMessage.setVisibility(View.VISIBLE);
                        layPrAnswer.setVisibility(View.VISIBLE);
                }
                catch(Exception e){
                        Log.d("[GudangGaram]", "PReuse :: hide_interrupt_screen Exception : " + e.getMessage());
                }
        }

}
