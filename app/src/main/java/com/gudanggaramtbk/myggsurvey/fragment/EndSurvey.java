package com.gudanggaramtbk.myggsurvey.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.activity.DownloadSurveyActivity;
import com.gudanggaramtbk.myggsurvey.activity.MainActivity;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterTextbox;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.model.CSurveyD;
import com.gudanggaramtbk.myggsurvey.model.CSurveyH;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;

import java.util.ArrayList;
import java.util.List;

public class EndSurvey extends Fragment implements View.OnClickListener{
    Button          Cmd_Submit_Survey;
    Button          Cmd_Cancel_Survey;
    Activity        pActivity;
    Context         ctx;
    MySQLiteHelper  dbHelper;
    CSurveyH        kumpulan_jawaban_header;
    List<CSurveyD>  kumpulan_jawaban;
    CEvent          oCEventParam;
    TextView        Txt_sse_closing_naration;
    String          StrSessionName;
    String          StrEventArea;
    Bundle          bundleParam;

    public EndSurvey(CEvent cEventParam){
        kumpulan_jawaban_header = new CSurveyH();
        kumpulan_jawaban = new ArrayList<CSurveyD>();
        oCEventParam = cEventParam;
    }

    public EndSurvey(Context pctx,CEvent cEventParam){
        ctx = pctx;
        oCEventParam = cEventParam;
    }

    public void set_jawaban(CSurveyH kjh, List<CSurveyD> kj){
        kumpulan_jawaban_header = kjh;
        kumpulan_jawaban        = kj;
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        CustomListAdapterTextbox adapter;
        View view = inflater.inflate(R.layout.fragment_end_survey, container, false);
        pActivity = getActivity();
        ctx  = pActivity.getApplicationContext();
        dbHelper = new MySQLiteHelper(ctx);
        dbHelper.getWritableDatabase();

        Log.d("[GudangGaram]", "EndSurvey :: onCreateView");

        // get bundle param sent from fragment StartSurvey
        bundleParam = this.getArguments();
        StrSessionName = bundleParam.getString("PIC_NAME");
        StrEventArea   = bundleParam.getString("EVENT_AREA");

        Txt_sse_closing_naration = (TextView) view.findViewById(R.id.txt_sse_closing_naration);
        Cmd_Submit_Survey = (Button) view.findViewById(R.id.cmd_submit_survey);
        Cmd_Cancel_Survey = (Button) view.findViewById(R.id.cmd_cancel_survey);


        Log.d("[GudangGaram]", "EndSurvey :: onCreateView > " + kumpulan_jawaban_header.getStatus());

        // write closing naration distinct from not valid or cancel or valid naration
        if(kumpulan_jawaban_header.getStatus().equals("Rec")){
            Txt_sse_closing_naration.setText(oCEventParam.getParam_Closing_Naration());
        }
        else{
            Txt_sse_closing_naration.setText(oCEventParam.getParam_NotValid_Naration());
        }

        Cmd_Submit_Survey.setOnClickListener(this);
        Cmd_Cancel_Survey.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick (View v){
        switch (v.getId()) {
            case R.id.cmd_submit_survey:
                EH_CMD_SUBMIT(pActivity);
                break;
            case R.id.cmd_cancel_survey:
                EH_CMD_CANCEL(pActivity);
                break;
        }
    }

    public void EH_CMD_SUBMIT(final Activity pxActivity){
        Log.d("[GudangGaram]", "EndSurvey :: EH_CMD_SUBMIT");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(pxActivity);
        alertDialogBuilder
                .setTitle("Konfirmasi Submission Survey")
                .setMessage("Submit Survey Ini ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                EH_CMD_DO_SUBMIT(pxActivity);
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void EH_CMD_CANCEL(final Activity pxActivity){
        Log.d("[GudangGaram]", "EndSurvey :: EH_CMD_CANCEL");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(pxActivity);
        alertDialogBuilder
                .setTitle("Konfirmasi Keluar Survey")
                .setMessage("Cancel Dan Keluar dari Survey Ini ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                EH_BACK_TO_LIST_EVENT(pxActivity);
                            }
                        })
                .setNegativeButton("Batal",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void EH_CMD_DO_SUBMIT(Activity pxActivity){
        Log.d("[GudangGaram]", "EndSurvey :: EH_CMD_DO_SUBMIT");
        try{
            // save to database
            EH_CMD_SAVE();
            // back to home page
            EH_CMD_BACK_TO_MAIN();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EndSurvey :: EH_CMD_DO_SUBMIT Exception :" + e.getMessage().toString());
        }
    }

    private void EH_CMD_SAVE(){
        Log.d("[GudangGaram]", "EndSurvey :: EH_CMD_SAVE");
        try{
            //load from memory list and save to database
            // save header to database
            dbHelper.AddSurveyHeader(kumpulan_jawaban_header);
            // save detail to database
            for(CSurveyD obj : kumpulan_jawaban){
                dbHelper.AddSurveyDetail(obj);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EndSurvey :: EH_CMD_SAVE Exception : " + e.getMessage().toString());
        }
    }
    private void EH_BACK_TO_LIST_EVENT(Activity pxActivity){
        Log.d("[GudangGaram]", "EndSurvey :: EH_BACK_TO_HOME");
        try{
            Intent upIntent = NavUtils.getParentActivityIntent(pxActivity);
            // send intent parameter to
            upIntent.putExtra("PIC_NAME", StrSessionName);
            upIntent.putExtra("EVENT_AREA", StrEventArea);
            NavUtils.navigateUpTo(pxActivity, upIntent);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EndSurvey :: EH_BACK_TO_HOME Exception :" + e.getMessage().toString());
        }
    }
    private void EH_CMD_BACK_TO_MAIN(){
        Log.d("[GudangGaram]", "EndSurvey :: EH_CMD_BACK_TO_MAIN");
        try{
            Intent I_Main;
            I_Main = new Intent(pActivity, MainActivity.class);
            // send intent parameter to mainactivity
            I_Main.putExtra("PIC_NAME", StrSessionName);
            I_Main.putExtra("EVENT_AREA", StrEventArea);
            startActivity(I_Main);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EndSurvey :: EH_CMD_BACK_TO_MAIN Exception : " + e.getMessage());
        }
    }

}