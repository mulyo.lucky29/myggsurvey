package com.gudanggaramtbk.myggsurvey.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Messages {
    private Context ctx;

    public Messages(Context pctx){
        this.ctx = pctx;
    }
    public void Show(String Title, String Message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder
                .setTitle(Title)
                .setMessage(Message)
                .setCancelable(false)
                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
