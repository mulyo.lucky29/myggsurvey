package com.gudanggaramtbk.myggsurvey.util;

public interface IPreferences {
    boolean contains(String key);
    int getInt(String key, int defValue);
    String getString(String key, String defValue);
    void putInt(String key, int value);
    void putString(String key, String value);
}