package com.gudanggaramtbk.myggsurvey.util;

import android.util.Log;
import java.util.LinkedList;
import java.util.Queue;

public class QueueInnerLoop {
    private MySQLiteHelper odbHelper;
    private String StrSurveySet;
    private Queue<String>  queueLine;

    public QueueInnerLoop(){
        queueLine = new LinkedList<>();
        Log.d("[GudangGaram]", "QueueInnerLoop :: queueLine > " + queueLine);
    }

    public void setParameter(MySQLiteHelper dbHelper, String strSurveySet){
        odbHelper = dbHelper;
        StrSurveySet = strSurveySet;
    }

    public Queue<String> getQueueLine() {
        return queueLine;
    }

    public String getQueueHead(){
        String result = null;
        try{
            result = queueLine.peek();
        }
        catch(Exception e){
        }
        return result;
    }

    public boolean findInQueue(String strElement){
        boolean result = false;
        try{
            if(queueLine.contains(strElement) == true){
                result = true;
            }
            else{
                result = false;
            }
        }
        catch(Exception e){

        }
        return result;
    }

    public Integer getQueueSize(){
        Integer result = 0;
        try{
            result = queueLine.size();
        }
        catch(Exception e){
        }
        return result;
    }


    public void enqueue(String strElement) {
        String result = null;
        Log.d("[GudangGaram]", "QueueInnerLoop :: enqueue");
        try{
            if(strElement.length() > 0) {
                queueLine.add(strElement);
                Log.d("[GudangGaram]", "QueueInnerLoop :: enqueue(" + strElement + ")");
                Log.d("[GudangGaram]", "QueueInnerLoop :: queueLine > " + queueLine);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "QueueInnerLoop :: enqueue Exception : " + e.getMessage().toString());
        }
    }

    public String dequeue(){
        Log.d("[GudangGaram]", "QueueInnerLoop :: dequeue");
        String result = null;
        try{
            result = queueLine.poll();
            Log.d("[GudangGaram]", "QueueInnerLoop :: queueLine > " + queueLine);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "QueueInnerLoop :: dequeue Exception : " + e.getMessage().toString());
        }
        return result;
    }

    public void push_to_queue(String strDelimted){
        Log.d("[GudangGaram]", "QueueInnerLoop :: push_to_queue Parameters: ");
        Log.d("[GudangGaram]", "QueueInnerLoop :: push_to_queue : strDelimited > " + strDelimted);

        String [] delim;
        String tmpTag;
        String replacetmpTag;

        String innerloop_next_route;
        try{
            if(strDelimted.length() > 0) {
                if (strDelimted.contains(";")) {
                    Log.d("[GudangGaram]", "QueueInnerLoop :: push_to_queue contain delimter ");
                    delim = strDelimted.split("\\;");
                    if (delim.length > 0) {
                        for (int i = 0; i < delim.length; i++) {
                            tmpTag = delim[i].trim();
                            // find innerloop next def based on tmpTag
                            innerloop_next_route = odbHelper.GetInnerLoopNextDefByOptionATag(StrSurveySet, tmpTag);
                            Log.d("[GudangGaram]", "QueueInnerLoop :: push_to_queue : >> innerloop_next_route : " + innerloop_next_route);
                            if(!innerloop_next_route.equals("")){
                                Log.d("[GudangGaram]", "QueueInnerLoop :: push_to_queue >> " + innerloop_next_route);
                                // push to queue line
                                enqueue(innerloop_next_route);
                            }
                        }
                    }
                }
                else {
                    Log.d("[GudangGaram]", "QueueInnerLoop :: push_to_queue not contain delimter ");
                    innerloop_next_route = odbHelper.GetInnerLoopNextDefByOptionATag(StrSurveySet, strDelimted);
                    if(!innerloop_next_route.equals("")){
                        // directly push to queue line
                        enqueue(innerloop_next_route);
                    }
                }
            }
            else{
                Log.d("[GudangGaram]", "QueueInnerLoop :: push_to_queue nothing to push");
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "QueueInnerLoop :: push_to_queue Exception : " + e.getMessage().toString());
        }
    }
}
