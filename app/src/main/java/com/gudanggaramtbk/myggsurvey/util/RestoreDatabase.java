package com.gudanggaramtbk.myggsurvey.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.BuildConfig;
import com.gudanggaramtbk.myggsurvey.activity.DownloadMasterActivity;
import com.gudanggaramtbk.myggsurvey.activity.UtilitiesActivity;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

public class RestoreDatabase {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private String fileName;
    private String folder;
    private String strDeviceID;
    private boolean isDownloaded;
    UtilitiesActivity pActivity;

    public RestoreDatabase(){
    }

    public void setAttribute(UtilitiesActivity opActivity, Context context, MySQLiteHelper dbHelper, SharedPreferences config, String StrDeviceID) {
        Log.d("[GudangGaram]", "RestoreDatabase :: setAttribute");
        this.context = context;
        this.dbHelper = dbHelper;
        this.config = config;
        this.strDeviceID = StrDeviceID;
        this.pActivity = opActivity;
    }

    private String get_path_restoredb_url(String strSoapAddress) {
        String result = "";
        String root_path_image;
        // strSoapAddress = http://10.50.131.18/WSGGSurvey/ggsurvey.asmx
        // imageURL = "http://10.50.131.18/WSGGSurvey/image/" + strImageName;
        Log.d("[GudangGaram]", "RestoreDatabase  :: get_path_restoredb_url");
        try {
            if (strSoapAddress.contains(".asmx")) {
                root_path_image = strSoapAddress.substring(0, strSoapAddress.lastIndexOf("/"));
                result = root_path_image + "/restore/" + strDeviceID + "/";
            } else {
                result = "";
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]", "RestoreDatabase  :: get_path_restoredb_url Exception : " + e.getMessage().toString());
            result = "";
        }
        Log.d("[GudangGaram]", "RestoreDatabase  :: get_path_restoredb_url >> " + result);
        return result;
    }

    private boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public void do_restore_db(){
        String dbName;
        Log.d("[GudangGaram]", "RestoreDatabase  :: do_restore_db");
        try{
            dbName = download_restoredb();
            if(!(dbName == null)){
                pushDatabase(dbName);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "RestoreDatabase  :: do_restore_db Exception : " + e.getMessage());
        }
    }

    private String download_restoredb() {
        Log.d("[GudangGaram]", "RestoreDatabase  :: download_restoredb");
        String strdbName;
        String restoredbURL;
        strdbName = dbHelper.Get_DatabaseName();
        Bitmap bitmap = null;
        int count;
        //imageURL = "http://10.50.131.18/WSGGSurvey/image/" + strImageName;
        restoredbURL = get_path_restoredb_url(config.getString("SoapAddress", "")) + strdbName;
        if (restoredbURL.length() > 0) {
            try {
                if (isSDCardPresent()) {
                    URL url = new URL(restoredbURL);
                    URLConnection connection = url.openConnection();
                    connection.connect();
                    // getting file length
                    int lengthOfFile = connection.getContentLength();
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);
                    //save restore deb to sdcard
                    folder = Environment.getExternalStorageDirectory() + File.separator + "ggsurvey" + File.separator;
                    //Create ggsurvey folder if it does not exist
                    File directory = new File(folder);
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }
                    // Output stream to write file
                    OutputStream output = new FileOutputStream(folder + strdbName);

                    byte data[] = new byte[1024];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // writing data to file
                        output.write(data, 0, count);
                        Log.d("[GudangGaram]", "RestoreDatabase  :: download_restoredb > " + total);
                    }
                    // flushing output
                    output.flush();
                    // closing streams
                    output.close();
                    input.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } // end if
        return strdbName;
    }

    private void deleteRestoreFileDB(String StrRestoreDB){
        try{
            File file = new File(StrRestoreDB);
            file.delete();
            if(file.exists()){
                file.getCanonicalFile().delete();
                if(file.exists()){
                    context.deleteFile(file.getName());
                }
            }
        }
        catch(Exception e){
        }
    }

    private void copyFile(String inputPath, String outputPath) {
        InputStream in = null;
        OutputStream out = null;

        Log.d("[GudangGaram]", "copyFile (" + inputPath + ") > " + outputPath);

        try {
            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        } catch (FileNotFoundException fnfe1) {
        } catch (Exception e) {
        }
    }

    private void pushDatabase(String dbName) {
        // this will push downloaded restoredb to database existing
        String restoreDBPath = Environment.getExternalStorageDirectory() + File.separator + "ggsurvey" + File.separator  + dbName;
        String currentDBPath = "/data/data/" + BuildConfig.APPLICATION_ID + "/databases/" + dbName;
        Messages msg = new Messages(context);

        try {
            File fdbrestore = new File(restoreDBPath);
            if (fdbrestore.exists()) {
                Log.d("[GudangGaram]", "RestoreDatabase :: pushDatabase : restore database exists");
                copyFile(restoreDBPath, currentDBPath);
                deleteRestoreFileDB(restoreDBPath);
                // continue to force exit to reload with new database
                pActivity.EH_CMD_EXIT_TO_APPLY_RESTOREDB();
            }
            else{
                msg.Show("Restore Failed","restore database not exists");
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]", "RestoreDatabase :: pushDatabase Exception : " + e.getMessage());
        }
    }
}
