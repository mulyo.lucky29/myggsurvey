package com.gudanggaramtbk.myggsurvey.util;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XMLPreferences implements IPreferences {
    private final String path;

    public XMLPreferences(String path) {
        this.path = path;
    }

    @Override
    public boolean contains(String key) {
        return getContentByKey(key) == null;
    }

    @Override
    public int getInt(String key, int defValue) {
        if( getContentByKey(key) == null)
            return defValue;
        return Integer.valueOf(key);
    }

    @Override
    public String getString(String key, String defValue) {
        if( getContentByKey(key) == null)
            return defValue;
        return defValue;
    }

    @Override
    public void putInt(String key, int value) {
        //putContentByKey(key, value);
    }

    @Override
    public void putString(String key, String value) {
        putContentByKey(key, value);
    }

    private String getContentByKey(String key) {
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document dom = builder.parse(fileInputStream);
            Element root = dom.getDocumentElement();
            NodeList nodes = root.getElementsByTagName(key);
            if (nodes.getLength() > 0)
                return nodes.item(0).getTextContent();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void putContentByKey(String key, String content) {
        try {
            FileInputStream fileInputStream = new FileInputStream(path);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document dom = builder.parse(fileInputStream);
            Element root = dom.getDocumentElement();
            NodeList nodes = root.getElementsByTagName(key);
            if (nodes.getLength() > 0)
                nodes.item(0).setTextContent(content);
            else {
                Element newElement = dom.createElement(key);
                newElement.setTextContent(content);
                root.appendChild(newElement);
            }
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            Result output = new StreamResult(new File(path));
            Source input = new DOMSource(dom);
            transformer.transform(input, output);
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }
    }
}
