package com.gudanggaramtbk.myggsurvey.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import androidx.core.content.FileProvider;
import com.gudanggaramtbk.myggsurvey.BuildConfig;
import com.gudanggaramtbk.myggsurvey.service.DownloadTaskAsync;
import java.io.File;

/**
 * Created by luckym on 1/31/2019.
 */

public class DownloadUpdate {
    private SharedPreferences config;
    private Context context;
    private String apkbaseurl;
    private ProgressDialog pdLoading;

    // override constructor
    public DownloadUpdate(SharedPreferences PConfig){
        this.config = PConfig;
    }
    public void setContext(Context context){
        this.context = context;
    }

    public void InstalApk(){
        Intent intent;
        Uri myuri;
        File fileapk;
        String PathName;
        Log.d("[GudangGaram]: ", "APK Install");

        try{
            PathName = Environment.getExternalStorageDirectory().toString() + File.separator + "ggsurvey" + File.separator + "app-release.apk";
            Log.d("[GudangGaram]: ", "DownloadUpdate : PathName " + PathName);

            fileapk  = new File(PathName);
            if(fileapk.exists()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Log.d("[GudangGaram]: ", "APK Install Higher Version ( >= 7.0) > " + BuildConfig.APPLICATION_ID);
                    try{
                        myuri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".fileprovider", fileapk);
                        Uri packageURI = Uri.parse(context.getApplicationContext().getPackageName());
                        intent = new Intent(android.content.Intent.ACTION_VIEW, packageURI);
                        intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setDataAndType(myuri, "application/vnd.android.package-archive");
                        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        context.startActivity(intent);
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]: ", "APK Install Higher Version ( >= 7.0) Exception : " + e.getMessage());
                    }
                } else {
                    Log.d("[GudangGaram]: ", "APK Install Lower Version");
                    myuri = Uri.fromFile(fileapk);
                    intent = new Intent(Intent.ACTION_VIEW);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setDataAndType(myuri, "application/vnd.android.package-archive");
                    context.startActivity(intent);

                }
                Log.d("[GudangGaram]", "APK Install Done");
            }
            else{
                Log.d("[GudangGaram]: ", "Apk File Not Exist");
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "APK Install Exception : " + e.getMessage());

        }
    }

    public void DownloadApk()
    {
        Log.d("[GudangGaram]", "DownloadUpdate :: DownloadAPK");
        this.apkbaseurl = config.getString("URLUpdater","");
        DownloadTaskAsync d = new DownloadTaskAsync(new DownloadTaskAsync.DownloadTaskAsyncResponse() {
            @Override
            public void PostDownloadAction(String output) {
                Log.d("[GudangGaram]", "APK Download Status " + output);
                if(output == "Finish"){
                    InstalApk();
                }
                //pdLoading.dismiss();
            }
        });
        d.setContext(context);
        d.execute(apkbaseurl);
    }
}