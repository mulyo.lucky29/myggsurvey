package com.gudanggaramtbk.myggsurvey.util;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Adapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.appcompat.view.menu.MenuView;
import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterCheckBoxGroup;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.model.CMTag;
import com.gudanggaramtbk.myggsurvey.model.CSelectedAnswer;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderCheckBoxGroup;

import java.util.List;

public class ConstructAnswer {
    private MySQLiteHelper  odbHelper;
    private String          oSurveyTag;
    private CMTag           ctag;

    public ConstructAnswer(MySQLiteHelper dbHelper, String strSurveyTag){
        Log.d("[GudangGaram]", "ConstructAnswer(" + strSurveyTag + ")");
        odbHelper  = dbHelper;
        oSurveyTag = strSurveyTag;
        ctag       = new CMTag();
    }

    public CSelectedAnswer CollectAnswer(RecyclerView rv, String type, List<CAnswerOption> cao) {
        CSelectedAnswer result = null;
        Log.d("[GudangGaram]", "ConstructAnswer :: CollectAnswer");

        try{
            switch (type) {
                case "TextBox":
                    Log.d("[GudangGaram]", "ConstructAnswer :: CollectAnswer -> TextBox");
                    result = Collect_Answer_TextBox(rv,odbHelper, oSurveyTag, cao);
                    break;
                case "RadioGroup":
                    Log.d("[GudangGaram]", "ConstructAnswer :: CollectAnswer -> RadioGroup");
                    result = Collect_Answer_RadioGroup(rv,odbHelper, oSurveyTag, cao);
                    break;
                case "HRadioGroup":
                    Log.d("[GudangGaram]", "ConstructAnswer :: CollectAnswer -> HRadioGroup");
                    result = Collect_Answer_HRadioGroup(rv,odbHelper, oSurveyTag, cao);
                    break;
                case "CheckBoxGroup":
                    Log.d("[GudangGaram]", "ConstructAnswer :: CollectAnswer -> CheckBoxGroup");
                    result = Collect_Answer_CheckBoxGroup(rv,odbHelper, oSurveyTag, cao);
                    break;
                case "MRadioGroup" :
                    Log.d("[GudangGaram]", "ConstructAnswer :: CollectAnswer -> MRadioGroup");
                    result = Collect_Answer_MRadioGroup(rv,odbHelper, oSurveyTag, cao);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ConstructAnswer :: CollectAnswer Exception :" + e.getMessage().toString());
        }
        return result;
    }

    public static String padLeft(String s, int n) {
        return String.format("%1$#" + n + "s", s);
    }

    public CSelectedAnswer Collect_Answer_TextBox(RecyclerView rv, MySQLiteHelper sdbHelper, String strSurveyTag, List<CAnswerOption> cao){
        Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_TextBox");
        String tag, value;
        String next_override;

        CSelectedAnswer result = null;
        View v = rv.findViewHolderForAdapterPosition(0).itemView;
        EditText et   = v.findViewById(R.id.txt_textbox);


        try{
            tag   = et.getTag().toString();
            value = et.getText().toString().trim();

            Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_TextBox : OptionTagSelected : " + tag);
            result = new CSelectedAnswer();
            result.setSelectedValue(value);
            result.setSelectedTag(tag);

            // find value next_route_override from selected option tag
            PreLoadData pd  = new PreLoadData(sdbHelper, strSurveyTag);
            next_override = pd.GetOverrideNextByOptionATag(tag);
            result.setOverrideNextQuestion(next_override);
            result.setSelectedAttribute("");
            result.setOtherFlag("");
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_TextBox Exception :" + e.getMessage().toString());
        }

        return result;
    }

    public CSelectedAnswer Collect_Answer_MRadioGroup(RecyclerView rv, MySQLiteHelper sdbHelper, String strSurveyTag, List<CAnswerOption> cao){
        Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_MRadioGroup");
        String tag, value;
        String rg_tag, rg_value;
        String next_override;
        int i;
        int count = 0;
        String AnswerOptionTag;
        CSelectedAnswer result = null;

        tag   = "";
        value = "";

        result = new CSelectedAnswer();
        try{
            if (rv.getAdapter() != null) {
                count = rv.getAdapter().getItemCount();
                for(i=0; i<count; i++){
                    View v = rv.findViewHolderForAdapterPosition(i).itemView;
                    RadioGroup rg = v.findViewById(R.id.mrg_answer_set);
                    RadioButton orb  = v.findViewById(rg.getCheckedRadioButtonId());

                    ctag = (CMTag) orb.getTag();
                    rg_tag   = ctag.getStrTag().toString();
                    rg_value = ctag.getStrVal().toString();
                    Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_MRadioGroup : OptionTagSelected : " + rg_tag);

                    tag   = tag   + rg_tag + ";";
                    value = value + rg_value + ";";
                }
            }
            result.setSelectedValue(value);
            result.setSelectedTag(tag);
            result.setSelectedAttribute("");
            result.setOtherFlag("");

            // override should be taken from CQuestionSet as default if there is no
            PreLoadData pd  = new PreLoadData(sdbHelper, strSurveyTag);
            next_override = pd.GetDefaultNextQuestionByATag(tag);
            result.setOverrideNextQuestion(next_override);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_MRadioGroup Exception :" + e.getMessage().toString());
        }

        return result;
    }

    public CSelectedAnswer Collect_Answer_HRadioGroup(RecyclerView rv, MySQLiteHelper sdbHelper, String strSurveyTag, List<CAnswerOption> cao){
        Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_HRadioGroup");
        String tag, value;
        String next_override;

        CSelectedAnswer result = null;
        int count = 0;

        tag   = "";
        value = "";

        View v = rv.findViewHolderForAdapterPosition(0).itemView;
        RadioGroup rg = v.findViewById(R.id.hrg_answer_set);
        RadioButton orb  = v.findViewById(rg.getCheckedRadioButtonId());

        try{
            if (rv.getAdapter() != null) {
                count = rv.getAdapter().getItemCount();
                if(count > 0){
                    tag   = orb.getTag().toString();
                    value = orb.getText().toString();
                    Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_HRadioGroup : OptionTagSelected : " + tag);

                    result = new CSelectedAnswer();
                    result.setSelectedValue(value);
                    result.setSelectedTag(tag);
                    // find value next_route_override from selected option tag
                    PreLoadData pd  = new PreLoadData(sdbHelper, strSurveyTag);
                    next_override = pd.GetOverrideNextByOptionATag(tag);
                    result.setOverrideNextQuestion(next_override);
                    result.setSelectedAttribute("");
                    result.setOtherFlag("");
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_HRadioGroup Exception :" + e.getMessage().toString());
        }

        return result;
    }

    public CSelectedAnswer Collect_Answer_RadioGroup(RecyclerView rv, MySQLiteHelper sdbHelper, String strSurveyTag, List<CAnswerOption> cao){
        Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_RadioGroup");
        String rg_tag, rg_value, rg_is_other, rg_val_other;
        String tag, value;
        String next_override;

        CSelectedAnswer result = null;
        int count = 0;

        tag   = "";
        value = "";

        View v = rv.findViewHolderForAdapterPosition(0).itemView;
        RadioGroup rg = v.findViewById(R.id.rg_answer_set);
        RadioButton orb  = v.findViewById(rg.getCheckedRadioButtonId());

        try{
            if (rv.getAdapter() != null) {
                count = rv.getAdapter().getItemCount();
                if(count > 0){

                    ctag = (CMTag) orb.getTag();
                    rg_tag       = ctag.getStrTag();
                    rg_value     = ctag.getStrVal();
                    rg_is_other  = ctag.getIsOther();
                    rg_val_other = ctag.getOtherVal();

                    Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_RadioGroup : rg_tag         : " + rg_tag);
                    Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_RadioGroup : rg_value       : " + rg_value);
                    Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_RadioGroup : rg_is_other    : " + rg_is_other);
                    Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_RadioGroup : rg_val_other   : " + rg_val_other);

                    tag = rg_tag;
                    value = rg_value;

                    result = new CSelectedAnswer();
                    result.setSelectedValue(value);
                    result.setSelectedTag(tag);

                    if(rg_is_other.equals("Y")){
                        result.setOtherFlag("Y");
                        result.setSelectedAttribute(rg_val_other);
                    }
                    else{
                        result.setOtherFlag("");
                        result.setSelectedAttribute("");
                    }

                    // find value next_route_override from selected option tag
                    PreLoadData pd  = new PreLoadData(sdbHelper, strSurveyTag);
                    next_override = pd.GetOverrideNextByOptionATag(tag);
                    result.setOverrideNextQuestion(next_override);
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_RadioGroup Exception :" + e.getMessage().toString());
        }

        return result;
    }

    public CSelectedAnswer Collect_Answer_CheckBoxGroup(RecyclerView rv, MySQLiteHelper sdbHelper, String strSurveyTag, List<CAnswerOption> cao){
        Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_CheckBoxGroup");
        String tag, value, other_value;
        String next_override;
        boolean found_other_flag = false;
        int i;

        CSelectedAnswer result = null;
        result = new CSelectedAnswer();
        other_value = "";

        try{
            if (rv.getAdapter() != null) {
                tag   = "";
                value = "";

                for(CAnswerOption x : cao){
                    if(x.isSelected()){
                        tag   = tag   + x.getAnswerOptionTag().toString() + ";";
                        value = value + x.getAnswerOptionValue().toString() + ";";

                        try{
                            if(x.getAnswerOtherFlag().trim().equals("Y")){
                                other_value = x.getOtherValue();
                                found_other_flag = true;
                            }
                        }
                        catch(Exception e){
                            other_value = "";
                        }

                        Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_CheckBoxGroup : Selected Tag : " + tag);
                        Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_CheckBoxGroup : Selected Value : " + value);

                        result.setSelectedValue(value);
                        result.setSelectedTag(tag);
                        // denial flag
                        try{
                            if(x.getDenialFlag().trim().length() > 0){
                                result.setDenialFlag(x.getDenialFlag().trim());
                            }
                            else{
                                result.setDenialFlag(null);
                            }
                        }
                        catch(Exception e){
                            result.setDenialFlag(null);
                        }
                        // denial next route
                        try{
                            if(x.getDenialNextDef().trim().length() > 0){
                                result.setDenialNextQuestion(x.getDenialNextDef().trim());
                            }
                            else{
                                result.setDenialNextQuestion(null);
                            }
                        }
                        catch(Exception e){
                            result.setDenialNextQuestion(null);
                        }
                    }
                }

                Log.d("[GudangGaram]", "ConstructAnswer :: Others  : " + other_value);
                // other flag
                if(found_other_flag == true){
                    result.setOtherFlag("Y");
                }
                else{
                    result.setOtherFlag("");
                }
                // value if other selected
                result.setSelectedAttribute(other_value);

                // override should be taken from CQuestionSet as default
                PreLoadData pd  = new PreLoadData(sdbHelper, strSurveyTag);
                next_override = pd.GetDefaultNextQuestionByATag(tag);
                Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_CheckBoxGroup : NextOverride -> " + next_override + "");
                result.setOverrideNextQuestion(next_override);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "ConstructAnswer :: Collect_Answer_CheckBoxGroup Exception :" + e.getMessage().toString());
        }

        return result;
    }
}
