package com.gudanggaramtbk.myggsurvey.util;

import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.model.CQuestionSet;
import java.util.ArrayList;
import java.util.List;

public class MyPreDefData {

    public MyPreDefData(){

    }

    // ----------------------- load data option question manually or from database perhaps do here ------------------------------------
    public List<List<CAnswerOption>> GenerateAnswerOption(){
        List<List<CAnswerOption>> AS = new ArrayList<List<CAnswerOption>>();

        // -------- define option for each question set ---------
        List<CAnswerOption> oAS1 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS2 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS3 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS4 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS5 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS6 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS7 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS8 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS9 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS10 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS11 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS12a = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS12b = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS13 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS14 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS15 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS16 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS17a = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS17b = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS17c = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS18 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS19a = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS19b = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS19c = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS20 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS21 = new ArrayList<CAnswerOption>();
        List<CAnswerOption> oAS22 = new ArrayList<CAnswerOption>();


        oAS1.add(new CAnswerOption("Q1","Q1_1","","Q2"));
        oAS2.add(new CAnswerOption("Q2","Q2_1","","Q3"));

        oAS3.add(new CAnswerOption("Q3","Q3_1","Kurang dari 18 tahun","QE"));
        oAS3.add(new CAnswerOption("Q3","Q3_2","18 – 20 tahun","Q4"));
        oAS3.add(new CAnswerOption("Q3","Q3_3","21 – 24 tahun","Q4"));
        oAS3.add(new CAnswerOption("Q3","Q3_4","25 – 30 tahun","Q4"));
        oAS3.add(new CAnswerOption("Q3","Q3_5","31 – 34 tahun","Q4"));
        oAS3.add(new CAnswerOption("Q3","Q3_6","35 – 44 tahun","Q4"));
        oAS3.add(new CAnswerOption("Q3","Q3_7","45 – 54 tahun","Q4"));
        oAS3.add(new CAnswerOption("Q3","Q3_8","55 – 64 tahun","Q4"));
        oAS3.add(new CAnswerOption("Q3","Q3_9","Diatas 64 tahun","QE"));

        oAS4.add(new CAnswerOption("Q4","Q4_1","Ya","Q5"));
        oAS4.add(new CAnswerOption("Q4","Q4_2","Tidak","QE"));

        oAS5.add(new CAnswerOption("Q5","Q5_1","Lebih dari 6 batang sehari","Q6"));
        oAS5.add(new CAnswerOption("Q5","Q5_2","Kurang dari 6 batang sehari","QE"));

        oAS6.add(new CAnswerOption("Q6","Q6_1", "Apache Filter","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_2", "Apache Kretek 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_3", "Avolution Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_4", "Avolution Red","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_5", "Bentoel Sensasi Sejati 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_6", "Clas Mild","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_7", "Clas Mild Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_8", "Clas Mild Silver","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_9", "Djarum 76","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_10","Djarum 76 Filter Gold 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_11","Djarum Black","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_12","Djarum Black Cappucino 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_13","Djarum Black Menthol 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_14","Djarum Black Mild 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_15","Djarum Clavo Non Filter 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_16","Djarum Clavo Premio 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_17","Djarum Coklat 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_18","Djarum Istimewa 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_19","Djarum Super","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_20","Djarum Super MLD","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_21","Dji Sam Soe Filter","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_22","Dji Sam Soe Non Filter","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_23","Dji Sam Soe Super Premium 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_24","Dunhill Filter 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_25","Dunhill Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_26","Dunhill Mild","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_27","Dunhill Red","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_28","Dunhill Ultra 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_29","Forte","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_30","GG Mild 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_31","GG Mild Shiver 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_32","GG Move 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_33","Gudang Garam Djaja","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_34","Gudang Garam Filter International 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_35","Gudang Garam Halim","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_36","Gudang Garam Merah","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_37","Gudang Garam Patra","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_38","Gudang Garam Signature 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_39","Gudang Garam Signature Mild 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_40","LA Bold","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_41","LA Lights","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_42","LA Lights Ice 16 - Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_43","LA Lights Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_44","LA Lights Red","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_45","LA Red Filter 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_46","Lucky Strike Bold 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_47","Lucky Strike Lights","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_48","Lucky Strike Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_49","Lucky Strike Mild","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_50","Lucky Strike Regular","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_51","Magnum Filter 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_52","Magnum Mild 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_53","Marlboro Black Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_54","Marlboro Filter Black","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_55","Marlboro Filter Black 20","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_56","Marlboro Ice Burst","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_57","Marlboro Light Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_58","Marlboro Lights","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_59","Marlboro Mild Black","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_60","Maxus 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_61","MLD Black Series 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_62","Philip Morris Bold 12","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_63","Sampoerna A Hijau","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_64","Sampoerna A Mild","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_65","Sampoerna A Mild Platinum 16","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_66","Sriwedari","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_67","Star Mild","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_68","Star Mild Menthol","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_69","Surya 12 Coklat","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_70","Surya 12 Merah","Q7"));
        oAS6.add(new CAnswerOption("Q6","Q6_71","Surya 16","Q7"));


        oAS7.add(new CAnswerOption("Q7","Q7_1","Kurang dari 1 jam","QE"));
        oAS7.add(new CAnswerOption("Q7","Q7_2","1 – 2 jam","Q8"));
        oAS7.add(new CAnswerOption("Q7","Q7_3","Lebih dari 2 jam","Q8"));

        oAS8.add(new CAnswerOption("Q8","Q8_1","Spanduk","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_2","Videotron","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_3","Flyer","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_4","Social Media","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_5","Website","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_6","Teman / kerabat / komunitas ( word of mouth )","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_7","Iklan dalam televisi","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_8","Radio","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_9","Koran / majalah","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_10","SMS","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_11","Billboard / baliho","Q9"));
        oAS8.add(new CAnswerOption("Q8","Q8_12","Lainnya","Q9"));


        oAS9.add(new CAnswerOption("Q9","Q9_1","Saya tertarik dengan music / pengisi acara","Q10"));
        oAS9.add(new CAnswerOption("Q9","Q9_2","Saya suka dengan konten/isi acara  (kompetisi,talkshow,games,workshop)","Q10"));
        oAS9.add(new CAnswerOption("Q9","Q9_3","Acara ini dibuat oleh merek rokok yang saya hisap","Q10"));
        oAS9.add(new CAnswerOption("Q9","Q9_4","Lokasi ( dekat rumah , sedang di area sekitar )","Q10"));
        oAS9.add(new CAnswerOption("Q9","Q9_5","Tempat event sedang hits / kekinian","Q10"));
        oAS9.add(new CAnswerOption("Q9","Q9_6","Diajak teman/mengisi waktu  luang","Q10"));

        oAS10.add(new CAnswerOption("Q10","Q10_1","Mengunjungi booth bazaar makanan dan minuman","Q11"));
        oAS10.add(new CAnswerOption("Q10","Q10_2","Mengunjungi booth permainan dan aktivasi lainnya","Q11"));
        oAS10.add(new CAnswerOption("Q10","Q10_3","Mengujungi booth bazaar barang – barang lainnya","Q11"));
        oAS10.add(new CAnswerOption("Q10","Q10_4","Membeli di booth bazzar makanan dan minuman","Q11"));
        oAS10.add(new CAnswerOption("Q10","Q10_5","Mengikuti permainan di booth aktivasi","Q11"));
        oAS10.add(new CAnswerOption("Q10","Q10_6","Membeli barang di booth bazaar","Q11"));
        oAS10.add(new CAnswerOption("Q10","Q10_7","Mengikuti kompetisi di event","Q11"));
        oAS10.add(new CAnswerOption("Q10","Q10_8","Menyaksikan / menonton kompetisi","Q11"));
        oAS10.add(new CAnswerOption("Q10","Q10_9","Menyaksikan / menonton music / artis pengisi acara","Q11"));

        oAS11.add(new CAnswerOption("Q11","Q11_1","Ya","Q12a"));
        oAS11.add(new CAnswerOption("Q11","Q11_2","Tidak","Q13"));

        oAS12a.add(new CAnswerOption("Q12a","Q12a_1","Gudang Garam International","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_2","Gudang Garam Signature Coklat","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_3","Gudang Garam Signature Mild","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_4","Gudang Garam Merah","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_5","Gudang Garam Djaja","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_6","Gudang Garam Patra","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_7","Sriwedari","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_8","Surya","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_9","Surya Pro","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_10","Surya Pro Mild","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_11","GG Mild","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_12","GG Move","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_13","GG Shiver","Q12b"));
        oAS12a.add(new CAnswerOption("Q12a","Q12a_13","Lainnya","Q12b"));

        oAS12b.add(new CAnswerOption("Q12b","Q12b_1","Gudang Garam","Q13"));
        oAS12b.add(new CAnswerOption("Q12b","Q12b_2","PMI / Sampoerna","Q13"));
        oAS12b.add(new CAnswerOption("Q12b","Q12b_3","Djarum","Q13"));
        oAS12b.add(new CAnswerOption("Q12b","Q12b_4","Nojorono","Q13"));
        oAS12b.add(new CAnswerOption("Q12b","Q12b_5","BAT/Bentoel","Q13"));
        oAS12b.add(new CAnswerOption("Q12b","Q12b_6","JTI(Japanese Tobacco International)","Q13"));
        oAS12b.add(new CAnswerOption("Q12b","Q12b_7","Lainnya","Q13"));

        /*
        oAS13.add(new CAnswerOption("Q13","Q13_1","Sangat tidak suka","Q14"));
        oAS13.add(new CAnswerOption("Q13","Q13_2","Tidak suka","Q14"));
        oAS13.add(new CAnswerOption("Q13","Q13_3","Antara suka dan tidak suka","Q14"));
        oAS13.add(new CAnswerOption("Q13","Q13_4","Suka","Q14"));
        oAS13.add(new CAnswerOption("Q13","Q13_5","Sangat Suka","Q14"));
        */

        oAS13.add(new CAnswerOption("Q13", "Q13_a","Pertanyaan 1","{Q13_a_1 | Q13_a_2 | Q13_a_3 | Q13_a_4 | Q13_a_5}","{Sangat Tidak suka | Tidak Suka | Antara suka dan tidak suka | Suka | Sangat Suka}"," {Q14 | Q14 | Q14 | Q14 | Q14}"));
        oAS13.add(new CAnswerOption("Q13", "Q13_b","Pertanyaan 2","{Q13_b_1 | Q13_b_2 | Q13_b_3 | Q13_b_4 | Q13_b_5}","{Sangat Tidak suka | Tidak Suka | Antara suka dan tidak suka | Suka | Sangat Suka}"," {Q14 | Q14 | Q14 | Q14 | Q14}"));


        oAS14.add(new CAnswerOption("Q14","Q14_1","Ya","Q15"));
        oAS14.add(new CAnswerOption("Q14","Q14_2","Tidak","Q15"));

        oAS15.add(new CAnswerOption("Q15","Q15_1","Ya","Q16"));
        oAS15.add(new CAnswerOption("Q15","Q15_2","Tidak","Q16"));

        oAS16.add(new CAnswerOption("Q16","Q16_1","Ya","Q17a"));
        oAS16.add(new CAnswerOption("Q16","Q16_2","Tidak","Q17a"));

        oAS17a.add(new CAnswerOption("Q17a","Q17a_1","Ya","Q17b"));
        oAS17a.add(new CAnswerOption("Q17a","Q17a_2","Tidak","Q18"));

        oAS17b.add(new CAnswerOption("Q17b","Q17b_1","Ya","Q17c"));
        oAS17b.add(new CAnswerOption("Q17b","Q17b_2","Tidak","Q17c"));

        oAS17c.add(new CAnswerOption("Q17c","Q17c_1","Ya","Q18"));
        oAS17c.add(new CAnswerOption("Q17c","Q17c_2","Tidak","Q18"));

        oAS18.add(new CAnswerOption("Q18","Q18_1","Ya","Q19a"));
        oAS18.add(new CAnswerOption("Q18","Q18_2","Tidak","Q19a"));

        oAS19a.add(new CAnswerOption("Q19a","Q19a_1","Ya","Q19b"));
        oAS19a.add(new CAnswerOption("Q19a","Q19a_2","Tidak","Q20"));

        oAS19b.add(new CAnswerOption("Q19b","Q19b_1","Ya","Q19c"));
        oAS19b.add(new CAnswerOption("Q19b","Q19b_2","Tidak","Q20"));

        oAS19c.add(new CAnswerOption("Q19c","Q19c_1","Ya","Q20"));
        oAS19c.add(new CAnswerOption("Q19c","Q19c_2","Tidak","Q20"));
        oAS19c.add(new CAnswerOption("Q19c","Q19c_3","Belum memutuskan","Q20"));

        oAS20.add(new CAnswerOption("Q20","Q20_1","Ya","Q21"));
        oAS20.add(new CAnswerOption("Q20","Q20_2","Sama saja","Q21"));

        oAS21.add(new CAnswerOption("Q21","Q21_1","Sangat tidak ingin datang","Q22"));
        oAS21.add(new CAnswerOption("Q21","Q21_2","Kurang ingin datang","Q22"));
        oAS21.add(new CAnswerOption("Q21","Q21_3","Belum bisa memastikan","Q22"));
        oAS21.add(new CAnswerOption("Q21","Q21_4","Cukup inigin datang","Q22"));
        oAS21.add(new CAnswerOption("Q21","Q21_5","Sangat ingin datang","Q22"));

        oAS22.add(new CAnswerOption("Q22","Q22_1","Sangat tidak ingin","QE"));
        oAS22.add(new CAnswerOption("Q22","Q22_2","Kurang ingin","QE"));
        oAS22.add(new CAnswerOption("Q22","Q22_3","Biasa saja","QE"));
        oAS22.add(new CAnswerOption("Q22","Q22_4","Cukup ingin","QE"));
        oAS22.add(new CAnswerOption("Q22","Q22_5","Sangat ingin","QE"));


        AS.add(oAS1);
        AS.add(oAS2);
        AS.add(oAS3);
        AS.add(oAS4);
        AS.add(oAS5);
        AS.add(oAS6);
        AS.add(oAS7);
        AS.add(oAS8);
        AS.add(oAS9);
        AS.add(oAS10);
        AS.add(oAS11);
        AS.add(oAS12a);
        AS.add(oAS12b);
        AS.add(oAS13);
        AS.add(oAS14);
        AS.add(oAS15);
        AS.add(oAS16);
        AS.add(oAS17a);
        AS.add(oAS17b);
        AS.add(oAS17c);
        AS.add(oAS18);
        AS.add(oAS19a);
        AS.add(oAS19b);
        AS.add(oAS19c);
        AS.add(oAS20);
        AS.add(oAS21);
        AS.add(oAS22);


        return AS;
    }

    // ----------------------- load data question manually or from database perhaps do here ------------------------------------
    public List<CQuestionSet> GenerateQuestion() {
        List<CQuestionSet> QS = new ArrayList<CQuestionSet>();
        QS.add(new CQuestionSet("SV-SET2", "Q1", "Q2","Q1. Nama respondent", "", "TextBox"));
        QS.add(new CQuestionSet("SV-SET2", "Q2", "Q3","Q2. No Handphone", "", "TextBox"));
        QS.add(new CQuestionSet("SV-SET2", "Q3", "Q4","Q3. Dapatkah anda memberitahu saya usia anda saat ini", "<p><b>Notes untuk FP [Jangan dibacakan]</b><br><ul><li>Pilihan jawaban tidak di tunjukkan kepada respondent</li><li>FP menanyakan usia kemudian melingkari/memilih pilihan usia</li></ul></p>", "RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2", "Q4", "Q5","Q4. Apakah anda merokok secara rutin setiap hari", "", "RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2", "Q5", "Q6","Q5. Berapa batangkah rokok yang anda hisap setiap hari", "", "RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2", "Q6", "Q7","Q6. Merek rokok apakah yang rutin anda hisap setiap hari", "", "RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2", "Q7", "Q8","Q7. Sudah berapa lamakah anda berada di event ini?", "", "RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2", "Q8", "Q9","Q8. Dari pilihan dibawah ini, dapatkah anda memberitahu saya, darimanakah anda mengetahui event ini?", "", "CheckBoxGroup"));
        QS.add(new CQuestionSet("SV-SET2", "Q9", "Q10","Q9. Dari pilihan dibawah ini, apa alasan anda datang ke acara ini?","","CheckBoxGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q10", "Q11","Q10. Dari pilihan dibawah ini , kegiatan apa saja yang anda ikuti  selama event ini","","CheckBoxGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q11", "Q12a","Q11. Apakah anda mengetahui sponsor dari event ini?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q12a","Q12b","Q12a. Anda mengatakan anda mengetahui merk yang mensponsori acara ini, dapatkah anda memberitahu saya","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q12b","Q13","Q12b. Menurut anda dari perusahaan rokok / manufacture apakah merek tersebut?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q13", "Q14","Q13. Dari skala 1 – 5 dimana 1 tidak suka dan 5 sangat suka, seberapa suka anda akan acara ini secara keseluruhan ?","","MRadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q14", "Q15","Q14. Menurut anda, Apakah acara ini membuat anda memiliki pandangan lebih baik dari merek yang mensponsori acara ini?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q15", "Q16","Q15. Apakah acara ini sesuai / relevan dengan minat anda?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q16", "Q17a","Q16. Menurut anda ,apakah acara ini unik/berbeda dari acara acara lain pada umumnya?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q17a","Q17b","Q17a. Sebelum datang ke event ini, apakah anda mengetahui merk [merk rokok] ini ?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q17b","Q17c","Q17b. Sebelumnya, apakah anda pernah mencoba rokok merk $merk_rokok ini ?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q17c","Q18","Q17c. Sebelumnya, apakah anda pernah membeli rokok merk $merk_rokok ini ?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q18", "Q19a","Q18. Menurut anda, Apakah acara ini membuat anda memiliki pandangan lebih baik dari terhadap merk $merk_rokok yang mensponsori acara ini?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q19a","Q19b","Q19a. Apakah anda membeli rokok merk $merk_rokok ketika di event ini?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q19b","Q19c","Q19b. Apakah anda suka dengan rokoknya?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q19c","Q20","Q19c. Apakah anda mempertimbangkan merk $merk_rokok  tersebut untuk dibeli selanjutnya","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q20", "Q21","Q20. Menurut anda, Apakah acara ini membuat anda memiliki pandangan yang lebih baik terhadap perusahaan Gudang Garam","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q21", "Q22","Q21. Apabila event ini akan diselenggarakan lagi, bagaimana keinginan anda untuk datang kembali ke event ini?","","RadioGroup"));
        QS.add(new CQuestionSet("SV-SET2","Q22", "QE", "Q22. Seberapa besar keinginan Anda untuk merekomendsikan kepada teman-teman Anda untuk datang ke event ini?","","RadioGroup"));

        return QS;
    }

}
