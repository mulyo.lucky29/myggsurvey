package com.gudanggaramtbk.myggsurvey.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.BuildConfig;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.activity.UtilitiesActivity;
import com.gudanggaramtbk.myggsurvey.util.Messages;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DumpDatabase {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private String fileName;
    private String folder;
    private String strDeviceID;
    private boolean isDownloaded;

    public DumpDatabase(){
    }

    public void setAttribute(String ostrDeviceID){
        strDeviceID = ostrDeviceID;
    }

    private boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }
    public void CreateDumpDatabase() {
        String dbdumpName;
        String dbName;
        String sysdate = new SimpleDateFormat("yyyyMMddhhmmssSSS").format(new Date());
        dbName = "MyGGSurvey.db";
        dbdumpName = strDeviceID + "_" + sysdate + ".db";
        Messages msg = new Messages(context);

        // this will push downloaded restoredb to database existing
        String dumpDBPath = Environment.getExternalStorageDirectory() + File.separator + "ggsurvey" + File.separator  + "dump";
        String currentDBPath = "/data/data/" + BuildConfig.APPLICATION_ID + "/databases/" + dbName;
        String dumpDBFilename = dumpDBPath + File.separator + dbdumpName;

        // check directory first
        File directory = new File(dumpDBPath);
        if (! directory.exists()){
            directory.mkdir();
        }
        try {
            File fdbsource = new File(currentDBPath);
            if (fdbsource.exists()) {
                copyFile(currentDBPath,dumpDBFilename);
            }
            else{
                msg.Show("Dump Database","Dump Database Failed");
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]", "RestoreDatabase :: pushDatabase Exception : " + e.getMessage());
        }
    }

    public void deleteDumpDB() {
        String dumpDBPath = Environment.getExternalStorageDirectory() + File.separator + "ggsurvey" + File.separator + "dump";
        boolean success = true;
        File dir = new File(dumpDBPath);
        try{
            if (dir.isDirectory()) {
                String[] children = dir.list();
                for (int i = 0; i < children.length; i++) {
                    new File(dir, children[i]).delete();
                }
            }
        }
        catch(Exception e){
        }
    }

    private void copyFile(String inputPath, String outputPath) {
        InputStream in = null;
        OutputStream out = null;

        Log.d("[GudangGaram]", "copyFile (" + inputPath + ") > " + outputPath);

        try {
            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        } catch (FileNotFoundException fnfe1) {
        } catch (Exception e) {
        }
    }


}
