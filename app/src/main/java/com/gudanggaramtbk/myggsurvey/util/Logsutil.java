package com.gudanggaramtbk.myggsurvey.util;

import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Logsutil {
    public Logsutil(){
        Log.d("[GudangGaram]", "Logsutil");
    }

    public StringBuilder readLogs() {
        Log.d("[GudangGaram]", "Logsutil :: readLogs");
        StringBuilder logBuilder = new StringBuilder();
        try {
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                logBuilder.append(line + "\n");
            }
        } catch (IOException e) {
            Log.d("[GudangGaram]", "Logsutil :: readLogs Exception : " + e.getMessage().toString());
        }
        return logBuilder;
    }

    public void clearLogs(){
        Log.d("[GudangGaram]", "Logsutil :: clearLogs");
        //clear the log
        try {
            Runtime.getRuntime().exec("logcat -c");
        } catch (IOException e) {
            Log.d("[GudangGaram]", "Logsutil :: clearLogs Exception : " + e.getMessage().toString());
        }
    }
}
