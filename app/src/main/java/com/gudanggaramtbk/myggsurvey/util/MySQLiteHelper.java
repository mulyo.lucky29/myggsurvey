package com.gudanggaramtbk.myggsurvey.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.model.CDynVariable;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.model.CImage;
import com.gudanggaramtbk.myggsurvey.model.CLookup;
import com.gudanggaramtbk.myggsurvey.model.CMainSummary;
import com.gudanggaramtbk.myggsurvey.model.CQuestionSet;
import com.gudanggaramtbk.myggsurvey.model.CSurveyD;
import com.gudanggaramtbk.myggsurvey.model.CSurveyH;
import com.gudanggaramtbk.myggsurvey.model.CSurveySet;
import com.gudanggaramtbk.myggsurvey.model.StringWithTag;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class MySQLiteHelper extends SQLiteOpenHelper {

private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "MyGGSurvey.db";
    private static Cursor XCursor;
    private static Cursor XMICursor;

    // ====================== table define ============================
    private static final String TABLE_M_EVENT           = "GGGG_EVENT";
    private static final String TABLE_M_SURVEY_SET      = "GGGG_SURVEY_SET";
    private static final String TABLE_M_QUESTION_SET    = "GGGG_SURVEY_QUESTION_SET";
    private static final String TABLE_M_ANSWER_OPTION   = "GGGG_SURVEY_ANSWER_OPTION";
    private static final String TABLE_M_PICTURE         = "GGGG_SURVEY_PICTURE";
    private static final String TABLE_T_SURVEY_H        = "GGGG_SURVEY_H";
    private static final String TABLE_T_SURVEY_D        = "GGGG_SURVEY_D";
    private static final String TABLE_M_LOOKUP          = "GGGG_LOOKUP";


    // ========================= Master Lookup =======================================
    private static final String KEY_M_LOOKUP_CONTEXT   = "LContext";
    private static final String KEY_M_LOOKUP_VALUE     = "LValue";
    private static final String KEY_M_LOOKUP_MEANNG    = "LMeaning";

    // ========================= Master Event =======================================
    private static final String KEY_M_EVT_EVENT_ID                = "Event_ID";
    private static final String KEY_M_EVT_EVENT_NAME              = "Event_Name";
    private static final String KEY_M_EVT_EVENT_DESCRIPTION       = "Event_Description";
    private static final String KEY_M_EVT_START_DATE              = "Start_Date";
    private static final String KEY_M_EVT_END_DATE                = "End_Date";
    private static final String KEY_M_EVT_SURVEY_TAG              = "Survey_Tag";
    private static final String KEY_M_EVT_ATTRIBUTE1              = "Attribute1";
    private static final String KEY_M_EVT_ATTRIBUTE2              = "Attribute2";
    private static final String KEY_M_EVT_ATTRIBUTE3              = "Attribute3";
    private static final String KEY_M_EVT_ATTRIBUTE4              = "Attribute4";
    private static final String KEY_M_EVT_ATTRIBUTE5              = "Attribute5";

    // ========================= Master Survey Set ==================================
    private static final String KEY_M_ST_SURVEY_TAG              = "Survey_Tag";
    private static final String KEY_M_ST_SURVEY_DESCRIPTION      = "Survey_Desc";
    private static final String KEY_M_ST_ISACTIVE                = "IsActive";
    private static final String KEY_M_ST_OPENING_NARATION        = "Opening_Naration";
    private static final String KEY_M_ST_CLOSING_NARATION        = "Closing_Naration";
    private static final String KEY_M_ST_NOT_VALID_NARATION      = "Not_Valid_Naration";
    private static final String KEY_M_ST_ATTRIBUTE1              = "Attribute1";
    private static final String KEY_M_ST_ATTRIBUTE2              = "Attribute2";
    private static final String KEY_M_ST_ATTRIBUTE3              = "Attribute3";
    private static final String KEY_M_ST_ATTRIBUTE4              = "Attribute4";

    // ========================= Master Question Set ================================
    private static final String KEY_M_QS_SURVEY_TAG              = "Survey_Tag";
    private static final String KEY_M_QS_QUESTION_TAG            = "Question_TAG";
    private static final String KEY_M_QS_QUESTION_VALUE          = "Question_Value";
    private static final String KEY_M_QS_QUESTION_HINT           = "Question_Hint";
    private static final String KEY_M_QS_ANSWER_TYPE             = "Answer_Type";
    private static final String KEY_M_QS_DEFAULT_NEXT            = "Default_Next";
    private static final String KEY_M_QS_SECTION_LABEL           = "Section_Label";
    private static final String KEY_M_QS_INTERRUPT_INFO          = "Interrupt_Info";
    private static final String KEY_M_QS_INTERRUPT_DURATION      = "Interrupt_Duration";
    private static final String KEY_M_QS_COHERENCE_WITH          = "Coherence_With";
    private static final String KEY_M_QS_INNERLOOP_BLOCK_NEXT    = "InnerLoop_Block_Next";
    private static final String KEY_M_QS_INNERLOOP_DEQUEUE_NODE  = "InnerLoop_Dequeue_Node";

    // ========================= Master Answer Option ===============================
    private static final String KEY_M_AO_SURVEY_TAG              = "Survey_Tag";
    private static final String KEY_M_AO_QUESTION_TAG            = "Question_Tag";
    private static final String KEY_M_AO_MATRIX_QUESTION_TAG     = "Matrix_Question_Tag";
    private static final String KEY_M_AO_MATRIX_QUESTION_LABEL   = "Matrix_Question_Label";
    private static final String KEY_M_AO_TEXTBOX_VALIDATION_TYPE = "TextBox_Validation_Type";
    private static final String KEY_M_AO_ANSWER_OPTION_TAG       = "Answer_Option_Tag";
    private static final String KEY_M_AO_ANSWER_OPTION_VALUE     = "Answer_Option_Value";
    private static final String KEY_M_AO_OVERRIDE_NEXT_DEF       = "Override_Next_Def";
    private static final String KEY_M_AO_ANSWER_OPTION_IMAGE     = "Image_Name";
    private static final String KEY_M_AO_INNERLOOP_NEXT_DEF      = "InnerLoop_Next_Def";
    private static final String KEY_M_AO_OTHER_FLAG              = "Other_Flag";
    private static final String KEY_M_AO_DENIAL_FLAG             = "Denial_Flag";
    private static final String KEY_M_AO_DENIAL_NEXT_DEF         = "Denial_Next_Def";



    // ========================= Master Picture ===============================
    private static final String KEY_M_PIC_CODE                    = "Image_Code";
    private static final String KEY_M_PIC_NAME_FILE               = "Image_FileName";


    // ========================= Survey Header  ===============================
    private static final String KEY_T_SH_HID                      = "SurveyHID";
    private static final String KEY_T_SH_SURVEY_TAG               = "Survey_Tag";
    private static final String KEY_T_SH_INTERVIEWER_NAME         = "Interviewer_Name";
    private static final String KEY_T_SH_EVENT_ID                 = "Event_ID";
    private static final String KEY_T_SH_EVENT_NAME               = "Event_Name";
    private static final String KEY_T_SH_EVENT_ATTRIBUTE1         = "Event_Attribute1";
    private static final String KEY_T_SH_EVENT_ATTRIBUTE2         = "Event_Attribute2";
    private static final String KEY_T_SH_EVENT_ATTRIBUTE3         = "Event_Attribute3";
    private static final String KEY_T_SH_EVENT_ATTRIBUTE4         = "Event_Attribute4";
    private static final String KEY_T_SH_SURVEY_DESC              = "Survey_Desc";
    private static final String KEY_T_SH_DEVICE_ID                = "Device_ID";
    private static final String KEY_T_SH_STATUS                   = "Status";
    private static final String KEY_T_SH_START_DATE               = "Start_Date";
    private static final String KEY_T_SH_END_DATE                 = "End_Date";
    private static final String KEY_T_SH_CANCEL_DATE              = "Cancel_Date";
    private static final String KEY_T_SH_CANCEL_REASON            = "Cancel_Reason";


    // ========================= Survey Detail  ===============================
    private static final String KEY_T_SD_DID                      = "SurveyDID";
    private static final String KEY_T_SD_HID                      = "SurveyHID";
    private static final String KEY_T_SD_SEQ_NO                   = "Seq_No";
    private static final String KEY_T_SD_QUESTION_TAG             = "Question_Tag";
    private static final String KEY_T_SD_QUESTION_VALUE           = "Question_Value";
    private static final String KEY_T_SD_ANSWER_OPTION_TAG        = "Answer_Option_Tag";
    private static final String KEY_T_SD_ANSWER_OPTION_VALUE      = "Answer_Option_Value";
    private static final String KEY_T_SD_START_DATE               = "Start_Date";
    private static final String KEY_T_SD_END_DATE                 = "End_Date";
    private static final String KEY_T_SD_CANCEL_DATE              = "Cancel_Date";
    private static final String KEY_T_SD_CREATED_DATE             = "Created_Date";
    private static final String KEY_T_SD_ATTRIBUTE1               = "Attribute1";
    private static final String KEY_T_SD_ATTRIBUTE2               = "Attribute2";
    private static final String KEY_T_SD_ATTRIBUTE3               = "Attribute3";
    private static final String KEY_T_SD_ATTRIBUTE4               = "Attribute4";
    private static final String KEY_T_SD_ATTRIBUTE5               = "Attribute5";

    public String Get_DatabaseName(){
        return DATABASE_NAME;
    }
    public static String getTableMLookup() {
        return TABLE_M_LOOKUP;
    }

    public long countRecordTable(String pTableName){
        long result;
        result = 0;

        SQLiteDatabase db = this.getWritableDatabase();
        String kueri = "SELECT count(*) FROM " + pTableName + ";";
        Log.d("[GudangGaram]", "MySQLiteHelper :: countRecordTable :: Query : " + kueri);

        XCursor = db.rawQuery(kueri, null);
        try {
            if(XCursor!=null){
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast())
                {
                    result = XCursor.getInt(0);
                    XCursor.moveToNext();
                }
            }
        }
        catch (SQLiteException e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: getCountScannedBarcode :: Exception " + e.getMessage().toString());
            e.printStackTrace();
            result = 0;
        }

        return result;
    }

    private static final String CREATE_LOOKUP_TABLE = " CREATE TABLE IF NOT EXISTS " + TABLE_M_LOOKUP + " ( " +
            KEY_M_LOOKUP_CONTEXT + " TEXT, " +
            KEY_M_LOOKUP_VALUE + " TEXT, " +
            KEY_M_LOOKUP_MEANNG + " TEXT, " +
            "PRIMARY KEY(" + KEY_M_LOOKUP_CONTEXT + "," + KEY_M_LOOKUP_VALUE + ")" + "); ";

    public static final String CREATE_EVENT_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_EVENT + " ( " +
            KEY_M_EVT_EVENT_ID + " TEXT PRIMARY KEY, " +
            KEY_M_EVT_EVENT_NAME + " TEXT, " +
            KEY_M_EVT_EVENT_DESCRIPTION + " TEXT, " +
            KEY_M_EVT_START_DATE + " TEXT, " +
            KEY_M_EVT_END_DATE + " TEXT, " +
            KEY_M_EVT_SURVEY_TAG + " TEXT, " +
            KEY_M_EVT_ATTRIBUTE1 + " TEXT, " +
            KEY_M_EVT_ATTRIBUTE2 + " TEXT, " +
            KEY_M_EVT_ATTRIBUTE3 + " TEXT, " +
            KEY_M_EVT_ATTRIBUTE4 + " TEXT, " +
            KEY_M_EVT_ATTRIBUTE5 + " TEXT " + "); ";

    public static final String CREATE_SURVEY_SET_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_SURVEY_SET + " ( " +
            KEY_M_ST_SURVEY_TAG + " TEXT PRIMARY KEY, " +
            KEY_M_ST_SURVEY_DESCRIPTION  + " TEXT, " +
            KEY_M_ST_ISACTIVE + " TEXT, " +
            KEY_M_ST_OPENING_NARATION + " TEXT, " +
            KEY_M_ST_CLOSING_NARATION + " TEXT, " +
            KEY_M_ST_NOT_VALID_NARATION + " TEXT, " +
            KEY_M_ST_ATTRIBUTE1 + " TEXT, " +
            KEY_M_ST_ATTRIBUTE2 + " TEXT, " +
            KEY_M_ST_ATTRIBUTE3 + " TEXT, " +
            KEY_M_ST_ATTRIBUTE4  + " TEXT " +  "); ";

    public static final String CREATE_QUESTION_SET = "CREATE TABLE IF NOT EXISTS " + TABLE_M_QUESTION_SET + " ( " +
            KEY_M_QS_SURVEY_TAG + " TEXT, " +
            KEY_M_QS_QUESTION_TAG + " TEXT, " +
            KEY_M_QS_QUESTION_VALUE + " TEXT, " +
            KEY_M_QS_QUESTION_HINT + " TEXT, " +
            KEY_M_QS_ANSWER_TYPE + " TEXT, " +
            KEY_M_QS_DEFAULT_NEXT + " TEXT, " +
            KEY_M_QS_SECTION_LABEL + " TEXT, " +
            KEY_M_QS_INTERRUPT_INFO + " TEXT, " +
            KEY_M_QS_INTERRUPT_DURATION + " INTEGER, " +
            KEY_M_QS_COHERENCE_WITH + " TEXT, " +
            KEY_M_QS_INNERLOOP_BLOCK_NEXT  + " TEXT, " +
            KEY_M_QS_INNERLOOP_DEQUEUE_NODE + " TEXT, " +
            "PRIMARY KEY (" + KEY_M_QS_SURVEY_TAG + "," + KEY_M_QS_QUESTION_TAG + ")" + "); ";


    public static final String CREATE_ANSWER_OPTION = "CREATE TABLE IF NOT EXISTS " + TABLE_M_ANSWER_OPTION + " ( " +
            KEY_M_AO_SURVEY_TAG + " TEXT, " +
            KEY_M_AO_QUESTION_TAG + " TEXT, " +
            KEY_M_AO_MATRIX_QUESTION_TAG + " TEXT, " +
            KEY_M_AO_MATRIX_QUESTION_LABEL + " TEXT, " +
            KEY_M_AO_TEXTBOX_VALIDATION_TYPE + " TEXT, " +
            KEY_M_AO_ANSWER_OPTION_TAG  + " TEXT, " +
            KEY_M_AO_ANSWER_OPTION_VALUE + " TEXT, " +
            KEY_M_AO_OVERRIDE_NEXT_DEF  + " TEXT, " +
            KEY_M_QS_DEFAULT_NEXT + " TEXT, " +
            KEY_M_AO_ANSWER_OPTION_IMAGE + " TEXT, " +
            KEY_M_AO_INNERLOOP_NEXT_DEF + " TEXT, " +
            KEY_M_AO_OTHER_FLAG + " TEXT, " +
            KEY_M_AO_DENIAL_FLAG + " TEXT, " +
            KEY_M_AO_DENIAL_NEXT_DEF + " TEXT, " +
            "PRIMARY KEY (" + KEY_M_AO_SURVEY_TAG + "," + KEY_M_AO_ANSWER_OPTION_TAG + ")" + "); ";

    public static final String CREATE_IMAGE = "CREATE TABLE IF NOT EXISTS " + TABLE_M_PICTURE+ " ( " +
            KEY_M_PIC_CODE + " TEXT PRIMARY KEY, " +
            KEY_M_PIC_NAME_FILE + " TEXT); ";

    public static final String CREATE_SURVEY_H  = "CREATE TABLE IF NOT EXISTS " + TABLE_T_SURVEY_H +  " ( " +
            KEY_T_SH_HID + " TEXT PRIMARY KEY, " +
            KEY_T_SH_SURVEY_TAG  + " TEXT, " +
            KEY_T_SH_SURVEY_DESC + " TEXT, " +
            KEY_T_SH_INTERVIEWER_NAME + " TEXT, " +
            KEY_T_SH_EVENT_ID + " TEXT, " +
            KEY_T_SH_EVENT_NAME + " TEXT, " +
            KEY_T_SH_EVENT_ATTRIBUTE1 + " TEXT, " +
            KEY_T_SH_EVENT_ATTRIBUTE2 + " TEXT, " +
            KEY_T_SH_EVENT_ATTRIBUTE3 + " TEXT, " +
            KEY_T_SH_EVENT_ATTRIBUTE4 + " TEXT, " +
            KEY_T_SH_DEVICE_ID + " TEXT, " +
            KEY_T_SH_STATUS + " TEXT," +
            KEY_T_SH_START_DATE + " TEXT," +
            KEY_T_SH_END_DATE + " TEXT, " +
            KEY_T_SH_CANCEL_DATE + " TEXT, " +
            KEY_T_SH_CANCEL_REASON + " TEXT " +
            " ); ";

    public static final String CREATE_SURVEY_D  = "CREATE TABLE IF NOT EXISTS " + TABLE_T_SURVEY_D +  " ( " +
            KEY_T_SD_DID   + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            KEY_T_SD_HID   + " TEXT, " +
            KEY_T_SD_SEQ_NO + " INTEGER, " +
            KEY_T_SD_QUESTION_TAG  + " TEXT, " +
            KEY_T_SD_QUESTION_VALUE + " TEXT, " +
            KEY_T_SD_ANSWER_OPTION_TAG + " TEXT, " +
            KEY_T_SD_ANSWER_OPTION_VALUE + " TEXT," +
            KEY_T_SD_START_DATE + " TEXT," +
            KEY_T_SD_END_DATE + " TEXT, " +
            KEY_T_SD_CANCEL_DATE + " TEXT, " +
            KEY_T_SD_CREATED_DATE + " TEXT, " +
            KEY_T_SD_ATTRIBUTE1 + " TEXT, " +
            KEY_T_SD_ATTRIBUTE2 + " TEXT, " +
            KEY_T_SD_ATTRIBUTE3 + " TEXT, " +
            KEY_T_SD_ATTRIBUTE4 + " TEXT, " +
            KEY_T_SD_ATTRIBUTE5 + " TEXT " + " ); ";


    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("[GudangGaram]", "MySQLiteHelper :: onCreate");
        // Drop table if existed
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_QUESTION_SET);
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_ASSET);
        //onCreate(db);


        db.execSQL(CREATE_LOOKUP_TABLE);
        db.execSQL(CREATE_EVENT_TABLE);
        db.execSQL(CREATE_SURVEY_SET_TABLE);
        db.execSQL(CREATE_QUESTION_SET);
        db.execSQL(CREATE_ANSWER_OPTION);
        db.execSQL(CREATE_IMAGE);
        db.execSQL(CREATE_SURVEY_H);
        db.execSQL(CREATE_SURVEY_D);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop table if existed
        //db.execSQL("DROP TABLE IF EXISTS " + TABLE_M_QUESTION_SET);
        //onCreate(db);
    }

    // ================================= db operation util =========================================

    public Integer deleteTable(String pTableName, String pWhere){
        Integer result;
        Log.d("[GudangGaram]", "MySQLiteHelper :: deleteTable : " + pTableName);
        SQLiteDatabase db= this.getWritableDatabase();
        result = db.delete(pTableName, pWhere, null);
        db.close();
        return result;
    }

    public void flushTable(String pTableName){
        Log.d("[GudangGaram]", "MySQLiteHelper :: flushTable");
        SQLiteDatabase db= this.getWritableDatabase();
        db.delete(pTableName, null, null);
        db.close();
    }

    public void flush_all(){
        Log.d("[GudangGaram]", "MySQLiteHelper :: flush_all");
        flushTable(TABLE_M_LOOKUP);
        flushTable(TABLE_M_EVENT);
        flushTable(TABLE_M_SURVEY_SET);
        flushTable(TABLE_M_QUESTION_SET);
        flushTable(TABLE_M_ANSWER_OPTION);
        flushTable(TABLE_M_PICTURE);
        flushTable(TABLE_T_SURVEY_H);
        flushTable(TABLE_T_SURVEY_D);

        // delete all dump from sdcard
        try{
            DumpDatabase ddb = new DumpDatabase();
            ddb.deleteDumpDB();
        }
        catch(Exception e){
        }
    }

    public long AddLookup(CLookup oCLookup){
        long savestatus;
        String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddLookup");

        try{ values.put(KEY_M_LOOKUP_CONTEXT, oCLookup.getLContext()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOOKUP_VALUE, oCLookup.getLValue()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_LOOKUP_MEANNG, oCLookup.getLMmeaning()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_M_LOOKUP, null, values); // key/value -> keys = column names/ values = column values
        db.close();

        return savestatus;
    }

    public long AddEvent(CEvent oCEvent){
        long savestatus;
        String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddEvent");

        try{ values.put(KEY_M_EVT_EVENT_ID, oCEvent.getEventID()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_EVENT_NAME, oCEvent.getEventName()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_EVENT_DESCRIPTION, oCEvent.getEventDescription()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_START_DATE, oCEvent.getEventStartDate()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_END_DATE, oCEvent.getEventEndDate()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_SURVEY_TAG, oCEvent.getEventSurveyTag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_ATTRIBUTE1, oCEvent.getAttribute1()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_ATTRIBUTE2, oCEvent.getAttribute2()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_ATTRIBUTE3, oCEvent.getAttribute3()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_ATTRIBUTE4, oCEvent.getAttribute4()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_EVT_ATTRIBUTE5, oCEvent.getAttribute5()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_M_EVENT, null, values); // key/value -> keys = column names/ values = column values
        db.close();

        return savestatus;
    }
    public long AddSurveySet(CSurveySet oCSuveySet){
        long savestatus;
        String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddSurveySet");

        try{ values.put(KEY_M_ST_SURVEY_TAG, oCSuveySet.getSurveyTag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_SURVEY_DESCRIPTION, oCSuveySet.getSurveyDescription()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_ISACTIVE, oCSuveySet.getIsActive()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_OPENING_NARATION, oCSuveySet.getOpeningNaration()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_CLOSING_NARATION, oCSuveySet.getClosingNaration()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_NOT_VALID_NARATION, oCSuveySet.getNotValidNaration()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_ATTRIBUTE1, oCSuveySet.getAttribute1()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_ATTRIBUTE1, oCSuveySet.getAttribute2()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_ATTRIBUTE1, oCSuveySet.getAttribute3()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_ST_ATTRIBUTE1, oCSuveySet.getAttribute4()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_M_SURVEY_SET, null, values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }
    public long AddQuestionSet(CQuestionSet oCQuestionSet){
        long savestatus;
        String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddQuestionSet");

        try{ values.put(KEY_M_QS_SURVEY_TAG, oCQuestionSet.getSurveyTag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_QUESTION_TAG, oCQuestionSet.getQuestionTag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_QUESTION_VALUE, oCQuestionSet.getQuestionValue()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_QUESTION_HINT, oCQuestionSet.getQuestionHint()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_ANSWER_TYPE, oCQuestionSet.getAnswerType()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_DEFAULT_NEXT, oCQuestionSet.getDefaultNext()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_SECTION_LABEL, oCQuestionSet.getSectionLabel()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_INTERRUPT_INFO, oCQuestionSet.getInterrupt_Info()); }
        catch(java.lang.NullPointerException exception){ }
        try{
            if(oCQuestionSet.getInterrupt_Duration() > 0){
                values.put(KEY_M_QS_INTERRUPT_DURATION, oCQuestionSet.getInterrupt_Duration());
            }
        }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_COHERENCE_WITH, oCQuestionSet.getCoherenceWith()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_INNERLOOP_BLOCK_NEXT, oCQuestionSet.getInnerLoop_Block_Next()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_QS_INNERLOOP_DEQUEUE_NODE, oCQuestionSet.getInnerLoop_Dequeue_Node()); }
        catch(java.lang.NullPointerException exception){ }
        savestatus = db.insert(TABLE_M_QUESTION_SET, null, values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }
    public long AddAnswerOption(CAnswerOption oCAnswerOption){
        long savestatus;
        String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddAnswerOption");

        try{ values.put(KEY_M_AO_SURVEY_TAG, oCAnswerOption.getSurveyTag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_QUESTION_TAG, oCAnswerOption.getQuestionTag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_MATRIX_QUESTION_TAG, oCAnswerOption.getMatrixQuestionTag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_MATRIX_QUESTION_LABEL, oCAnswerOption.getMatrixQuestionLabel()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_TEXTBOX_VALIDATION_TYPE, oCAnswerOption.getTextBoxValidationType()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_ANSWER_OPTION_TAG, oCAnswerOption.getAnswerOptionTag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_ANSWER_OPTION_VALUE, oCAnswerOption.getAnswerOptionValue()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_OVERRIDE_NEXT_DEF, oCAnswerOption.getOverrideNextDef()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_ANSWER_OPTION_IMAGE, oCAnswerOption.getImageName()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_INNERLOOP_NEXT_DEF, oCAnswerOption.getInnerLoopNextDef()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_OTHER_FLAG, oCAnswerOption.getAnswerOtherFlag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_DENIAL_FLAG, oCAnswerOption.getDenialFlag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_AO_DENIAL_NEXT_DEF, oCAnswerOption.getDenialNextDef()); }
        catch(java.lang.NullPointerException exception){ }


        savestatus = db.insert(TABLE_M_ANSWER_OPTION, null, values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }
    public long AddImage(CImage oCImage){
        long savestatus;
        String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddEvent");

        try{ values.put(KEY_M_PIC_CODE, oCImage.getPict_code()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_M_PIC_NAME_FILE, oCImage.getPict_name()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_M_PICTURE, null, values); // key/value -> keys = column names/ values = column values
        db.close();

        return savestatus;
    }
    public long AddSurveyHeader(CSurveyH oCSurveyH){
        long savestatus;
        String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddSurveyHeader");

        try{ values.put(KEY_T_SH_HID, oCSurveyH.getSurveyHID()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_SURVEY_TAG, oCSurveyH.getSurvey_Tag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_SURVEY_DESC, oCSurveyH.getSurvey_Desc()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_INTERVIEWER_NAME, oCSurveyH.getInterviewerName()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_EVENT_ID, oCSurveyH.getEventID()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_EVENT_NAME, oCSurveyH.getEventName()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_EVENT_ATTRIBUTE1, oCSurveyH.getEventAttribute1()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_EVENT_ATTRIBUTE2, oCSurveyH.getEventAttribute2()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_EVENT_ATTRIBUTE3, oCSurveyH.getEventAttribute3()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_EVENT_ATTRIBUTE4, oCSurveyH.getEventAttribute4()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_DEVICE_ID, oCSurveyH.getDevice_ID()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_STATUS, oCSurveyH.getStatus()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_START_DATE, oCSurveyH.getStart_Date()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_END_DATE, oCSurveyH.getEnd_Date()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_CANCEL_DATE, oCSurveyH.getCancel_Date()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SH_CANCEL_REASON, oCSurveyH.getCancel_Reason()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_T_SURVEY_H, null, values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }
    public long AddSurveyDetail(CSurveyD oCSurveyD){
        long savestatus;
        String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddSurveyHeader");

        try{ values.put(KEY_T_SD_HID, oCSurveyD.getSurveyHID()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_SEQ_NO , oCSurveyD.getSeq_No()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_QUESTION_TAG, oCSurveyD.getQuestion_Tag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_QUESTION_VALUE, oCSurveyD.getQuestion_Value());}
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_ANSWER_OPTION_TAG, oCSurveyD.getAnswer_Option_Tag()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_ANSWER_OPTION_VALUE, oCSurveyD.getAnswer_Option_Value()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_START_DATE, oCSurveyD.getStart_Date()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_END_DATE, oCSurveyD.getEnd_Date()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_CANCEL_DATE, oCSurveyD.getCancel_Date()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_CREATED_DATE, oCSurveyD.getCreated_Date()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_ATTRIBUTE1, oCSurveyD.getAttribute1()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_ATTRIBUTE2, oCSurveyD.getAttribute2()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_ATTRIBUTE3, oCSurveyD.getAttribute3()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_ATTRIBUTE4, oCSurveyD.getAttribute4()); }
        catch(java.lang.NullPointerException exception){ }
        try{ values.put(KEY_T_SD_ATTRIBUTE5, oCSurveyD.getAttribute5()); }
        catch(java.lang.NullPointerException exception){ }

        savestatus = db.insert(TABLE_T_SURVEY_D, null, values); // key/value -> keys = column names/ values = column values
        db.close();
        return savestatus;
    }

    public void AddBulkLookup(List<CLookup> LCLookup){
        Integer n;
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkLookup");

        n = LCLookup.size();
        try{
            if(n > 0){
                for (CLookup clo : LCLookup){
                    AddLookup(clo);
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkLookup Exception : " + e.getMessage().toString());
        }
    }

    public void AddBulkImage(List<CImage> LCImage){
        Integer n;
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkImage");

        n = LCImage.size();
        try{
            if(n > 0){
                for (CImage oci : LCImage){
                    AddImage(oci);
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkImage Exception : " + e.getMessage().toString());
        }
    }
    public void AddBulkSurveySet(List<CSurveySet> LCSurveySet){
        Integer n;
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkSurveySet");

        n = LCSurveySet.size();
        try{
            if(n > 0){
                for (CSurveySet oss : LCSurveySet){
                    AddSurveySet(oss);
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkSurveySet Exception : " + e.getMessage().toString());
        }
    }
    public void AddBulkQuestionSet(List<CQuestionSet> LCQuestionSet){
        Integer n;
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkQuestionSet");

        n = LCQuestionSet.size();
        try{
            if(n > 0){
                for (CQuestionSet ocs : LCQuestionSet){
                    AddQuestionSet(ocs);
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkQuestionSet Exception : " + e.getMessage().toString());
        }
    }
    public void AddBulkAnswerOption(List<CAnswerOption> LCAnswerOption){
        Integer n;
        Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkQuestionSet");

        n = LCAnswerOption.size();
        try{
            if(n > 0){
                for (CAnswerOption cao : LCAnswerOption){
                    AddAnswerOption(cao);
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: AddBulkQuestionSet Exception : " + e.getMessage().toString());
        }
    }

    public Boolean havingImage(String strQuestionTag){
        Boolean result = false;
        Integer query_result = 0;
        Log.d("[GudangGaram]", "MySQLiteHelper :: havingImage");
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " SELECT count(*) " +
                " FROM " + TABLE_M_ANSWER_OPTION + " " +
                " WHERE Question_Tag = '" + strQuestionTag + "' " +
                " AND (length(coalesce(trim(Image_Name),null,'')) > 0); ";

        XCursor = db.rawQuery(kueri, null);
        try {
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    query_result =XCursor.getInt(0);
                    XCursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            result = false;
            Log.d("[GudangGaram]", "MySQLiteHelper :: havingImage Exception : " + e.getMessage());
        }
        finally {
            XCursor.close();

            if(query_result > 0){
                result = true;
            }
            else{
                result = false;
            }
        }
        return result;
    }
    public String getImage(String strImageCode){
        String result = "";
        Log.d("[GudangGaram]", "MySQLiteHelper :: getImage");
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " SELECT Image_FileName" +
                " FROM " + TABLE_M_PICTURE + " " +
                " WHERE Image_Code = '" + strImageCode + "';";

        Log.d("[GudangGaram]", "MySQLiteHelper :: getImage Query : " +  kueri);
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: getListEvent count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    result =XCursor.getString(0);
                    Log.d("[GudangGaram]", "MySQLiteHelper :: getImage Filename : " +  result);
                    XCursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: getImage Exception : " + e.getMessage());
        }
        finally {
            XCursor.close();
        }
        return result;
    }

    public String getReplacedSentenceOverVariable(String strEventID, String strSentences){
        Log.d("[GudangGaram]", "getReplacedSentenceOverVariable");
        String strTemp = strSentences;
        String[] lookup_value = {"#[kota]","#[merk_rokok]","#[nama_outlet]","$[type_survey]"};
        String kueri = " SELECT " +
                " Attribute1, " +
                " Attribute2, " +
                " Attribute3, " +
                " Attribute4, " +
                " Attribute5 " +
                " FROM " + TABLE_M_EVENT + " " +
                " WHERE 1 = 1 " +
                " AND Event_ID = '" + strEventID + "'; ";

        SQLiteDatabase db =  this.getReadableDatabase();
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "getReplacedSentenceOverVariable count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    for(int i = 0; i< lookup_value.length; i++){
                        Log.d("[GudangGaram]", "getReplacedSentenceOverVariable : replace " + lookup_value[i] + " with " + XCursor.getString(i).trim());

                        Log.d("[GudangGaram]", "getReplacedSentenceOverVariable : find tag " + lookup_value[i] + " :" + strTemp.indexOf(lookup_value[i]));

                        strTemp = strTemp.replaceAll(Pattern.quote(lookup_value[i]) ,XCursor.getString(i).trim());
                    }
                    XCursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getReplacedSentenceOverVariable : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
            Log.d("[GudangGaram]", "getReplacedSentenceOverVariable : replace result " + strTemp);
        }
        return strTemp;
    }

    public List<StringWithTag> getListLookup(String strLookupType){
        List<StringWithTag>  result;
        result = new ArrayList<StringWithTag>();

        Log.d("[GudangGaram]", "MySQLiteHelper :: getListLookup");
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " SELECT " +
                " LValue, " +
                " LMeaning  " +
                " FROM " + TABLE_M_LOOKUP + " " +
                " WHERE 1 = 1 " +
                "  AND LContext = '" + strLookupType + "' " +
                " ORDER BY LContext, LValue;";

        Log.d("[GudangGaram]", "getListLookup Query : " +  kueri);

        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "getListLookup count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    String key  = XCursor.getString(0);
                    String value = XCursor.getString(1);
                    result.add(new StringWithTag(value, key));
                    XCursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getListLookup : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
        }
        return result;
    }

    public List<CEvent> getListEvent(String strAreaCode, String strSurveyType){
        List<CEvent> result;
        result = new ArrayList<CEvent>();

        Log.d("[GudangGaram]", "MySQLiteHelper :: getListEvent");
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " SELECT " +
                " Event_ID, " +
                " Event_Name, " +
                " Event_Description, " +
                " Start_Date, " +
                " End_Date, " +
                " Survey_Tag, " +
                " Attribute1, " +
                " Attribute2, " +
                " Attribute3, " +
                " Attribute4, " +
                " Attribute5 " +
                " FROM " + TABLE_M_EVENT + " " +
                " WHERE 1 = 1 " +
                " AND Attribute1 = '" + strAreaCode + "' ";

        if(!(strSurveyType.equals("ALL"))){
            kueri = kueri + " AND Attribute4 = '" +  strSurveyType + "' ";
        }
        kueri = kueri + " ORDER BY Start_Date DESC; ";

        Log.d("[GudangGaram]", "getListEvent Query : " +  kueri);

        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "getListEvent count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {

                    CEvent o = new CEvent();
                    o.setEventID(XCursor.getString(0));
                    o.setEventName(XCursor.getString(1));
                    o.setEventDescription(XCursor.getString(2));
                    o.setEventStartDate(XCursor.getString(3));
                    o.setEventEndDate(XCursor.getString(4));
                    o.setEventSurveyTag(XCursor.getString(5));
                    o.setAttribute1(XCursor.getString(6));
                    o.setAttribute2(XCursor.getString(7));
                    o.setAttribute3(XCursor.getString(8));
                    o.setAttribute4(XCursor.getString(9));
                    o.setAttribute5(XCursor.getString(10));

                    result.add(o);
                    XCursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getListEvent : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
        }
        return result;
    }

    public List<String> getListArea(){
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: getListArea");
        List<String> lvi = new ArrayList<String>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " SELECT LValue " +
                "FROM " + TABLE_M_LOOKUP + " " +
                "WHERE LContext = 'GS_MS_AREA' " +
                "ORDER BY LContext, LValue; ";

        Log.d("[GudangGaram]", "getListArea Query : " +  kueri);

        ctr = 0;
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "getListArea count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "getListArea : " +  ctr);
                    lvi.add(XCursor.getString(0));
                    XCursor.moveToNext();
                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getListArea : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
        }
        return lvi;
    }

    public List<CSurveyH> getListMain(String strAreaCode) {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: getListMain");
        List<CSurveyH> lvi = new ArrayList<CSurveyH>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " SELECT SurveyHID," +
                " Survey_Tag, " +
                " Survey_Desc, " +
                " Device_ID, " +
                " Status, " +
                " Start_Date, " +
                " End_Date, " +
                " Cancel_Date, " +
                " Cancel_Reason, " +
                " Event_ID, " +
                " Event_Name " +
                " FROM " + TABLE_T_SURVEY_H + " " +
                " WHERE 1 = 1 " +
                " AND TRIM(Event_Attribute1) = '" + strAreaCode.trim() + "' " +
                " ORDER BY SurveyHID DESC; ";

        Log.d("[GudangGaram]", "getListMain Query : " +  kueri);

        ctr = 0;
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "getListMain count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "getListMain : " +  ctr);
                    CSurveyH o = new CSurveyH();
                    o.setSurveyHID(XCursor.getString(0));
                    o.setSurvey_Tag(XCursor.getString(1));
                    o.setSurvey_Desc(XCursor.getString(2));
                    o.setDevice_ID(XCursor.getString(3));
                    o.setStatus(XCursor.getString(4));
                    o.setStart_Date(XCursor.getString(5));
                    o.setEnd_Date(XCursor.getString(6));
                    o.setCancel_Date(XCursor.getString(7));
                    o.setCancel_Reason(XCursor.getString(8));
                    o.setEventID(XCursor.getString(9));
                    o.setEventName(XCursor.getString(10));

                    lvi.add(o);
                    XCursor.moveToNext();

                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getListMain : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
        }
        return lvi;
    }
    public List<CMainSummary> getListMainSummary(String strAreaCode) {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: getListMainSummary");
        List<CMainSummary> lvi = new ArrayList<CMainSummary>();
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " SELECT Event_ID, " +
                              " Event_Name, " +
                              " Start_Date, " +
                              " sum(Rec_Valid) as Rec_Valid, " +
                              " sum(Rec_Not_Valid) as Rec_Not_Valid, " +
                              " sum(Rec_Cancelled) as Rec_Cancelled, " +
                              " sum(Rec_Total) as Rec_Total " +
                        " FROM ( " +
                           " SELECT Event_ID, Event_Name, " +
                           " substr(Start_Date,0,10) Start_Date, " +
                           " (case when Status = 'Rec'  then 1 else 0 end) as Rec_Valid, " +
                           " (case when Status = 'Not Valid' then 1 else 0 end) as Rec_Not_Valid, " +
                           " (case when Status = 'Cancelled' then 1 else 0 end) as Rec_Cancelled, " +
                           "  1 Rec_Total " +
                           " from GGGG_SURVEY_H " +
                           " where TRIM(Event_Attribute1) = '" + strAreaCode.trim() + "' ) x " +
                        " GROUP BY  Event_ID, Event_Name, Start_Date " +
                        " ORDER BY Event_ID ";

        Log.d("[GudangGaram]", "getListMainSummary Query : " +  kueri);

        ctr = 0;
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "getListMainSummary count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "getListMainSummary : " +  ctr);
                    CMainSummary o = new CMainSummary();
                    o.setEventID(XCursor.getString(0));
                    o.setEventName(XCursor.getString(1));
                    o.setDateExec(XCursor.getString(2));
                    o.setRecValid(XCursor.getInt(3));
                    o.setRecInvalid(XCursor.getInt(4));
                    o.setRecCancelled(XCursor.getInt(5));
                    o.setRecTotal(XCursor.getInt(6));

                    Log.d("[GudangGaram]", "getListMainSummary : EventID   -> " +  XCursor.getString(0));
                    Log.d("[GudangGaram]", "getListMainSummary : EventName -> " +  XCursor.getString(1));

                    lvi.add(o);
                    XCursor.moveToNext();

                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getListMainSummary : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
        }
        return lvi;
    }
    public CSurveyH getSurveyHByID(String strSurveyHID) {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: getSurveyHByID(" + strSurveyHID + ")");
        CSurveyH result;
        SQLiteDatabase db =  this.getReadableDatabase();

        String kueri = " SELECT SurveyHID," +
                " Survey_Tag, " +
                " Survey_Desc, " +
                " Event_ID, " +
                " Event_Name, " +
                " Interviewer_Name, " +
                " Event_Attribute1, " +
                " Event_Attribute2, " +
                " Event_Attribute3, " +
                " Event_Attribute4, " +
                " Device_ID, " +
                " Status, " +
                " Start_Date, " +
                " End_Date, " +
                " Cancel_Date, " +
                " Cancel_Reason " +
                " FROM " + TABLE_T_SURVEY_H + " " +
                " WHERE 1 = 1 " +
                " AND SurveyHID = '" +  strSurveyHID + "' " +
                " ORDER BY SurveyHID DESC; ";

        Log.d("[GudangGaram]", "getSurveyHByID (" + strSurveyHID + ") Query : " +  kueri);

        ctr = 0;
        XCursor = db.rawQuery(kueri, null);
        result = new CSurveyH();
        try {
            Log.d("[GudangGaram]", "getSurveyHByID (" + strSurveyHID + ") count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "getSurveyHByID : " +  ctr);
                    result.setSurveyHID(XCursor.getString(0));
                    result.setSurvey_Tag(XCursor.getString(1));
                    result.setSurvey_Desc(XCursor.getString(2));
                    result.setEventID(XCursor.getString(3));
                    result.setEventName(XCursor.getString(4));
                    result.setInterviewerName(XCursor.getString(5));
                    result.setEventAttribute1(XCursor.getString(6));
                    result.setEventAttribute2(XCursor.getString(7));
                    result.setEventAttribute3(XCursor.getString(8));
                    result.setEventAttribute4(XCursor.getString(9));
                    result.setDevice_ID(XCursor.getString(10));
                    result.setStatus(XCursor.getString(11));
                    result.setStart_Date(XCursor.getString(12));
                    result.setEnd_Date(XCursor.getString(13));
                    result.setCancel_Date(XCursor.getString(14));
                    result.setCancel_Reason(XCursor.getString(15));
                    XCursor.moveToNext();

                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "getSurveyHByID (" + strSurveyHID + ") : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
        }
        return result;
    }
    public List<CSurveyD> loadSurveyD(String strSurveyHID) {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: loadSurveyD(" + strSurveyHID+ ")");

        String kueri = " SELECT SurveyDID, " +
                " SurveyHID, " +
                " Seq_No, " +
                " Question_Tag, " +
                " Question_Value, " +
                " Answer_Option_Tag, " +
                " Answer_Option_Value, " +
                " Start_Date, " +
                " End_Date, " +
                " Cancel_Date, " +
                " Created_Date, " +
                " Attribute1 " +
                " FROM " + TABLE_T_SURVEY_D  + " " +
                " WHERE 1 = 1 " +
                " AND SurveyHID = '" + strSurveyHID + "' " +
                " ORDER BY Seq_No; ";

        Log.d("[GudangGaram]", "MySQLiteHelper :: loadSurveyD Query : " +  kueri);

        List<CSurveyD> lvi  = new ArrayList<CSurveyD>();
        SQLiteDatabase db =  this.getReadableDatabase();

        ctr = 1;
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: loadSurveyD Query count : " +  XCursor.getCount());
            lvi.clear();
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "MySQLiteHelper :: loadSurveyD : XCursor [" +  ctr + "]");
                    CSurveyD csd = new CSurveyD();
                    csd.setSurveyDID(XCursor.getString(0));
                    csd.setSurveyHID(XCursor.getString(1));
                    csd.setSeq_No(XCursor.getString(2));
                    csd.setQuestion_Tag(XCursor.getString(3));
                    csd.setQuestion_Value(XCursor.getString(4));
                    csd.setAnswer_Option_Tag(XCursor.getString(5));
                    csd.setAnswer_Option_Value(XCursor.getString(6));
                    csd.setStart_Date(XCursor.getString(7));
                    csd.setEnd_Date(XCursor.getString(8));
                    csd.setCancel_Date(XCursor.getString(9));
                    csd.setCreated_Date(XCursor.getString(10));
                    csd.setAttribute1(XCursor.getString(11));

                    lvi.add(csd);
                    XCursor.moveToNext();
                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: loadSurveyD Exception : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
            Log.d("[GudangGaram]", "MySQLiteHelper :: loadSurveyD : lvi size = " +  lvi.size());
        }
        return lvi;
    }
    public List<CSurveySet> GetSurveySet(String strSurveyTag) {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: GetSurveySet(" + strSurveyTag+ ")");

        String kueri = " SELECT Survey_Tag, " +
                " Survey_Desc, " +
                " IsActive, " +
                " Opening_Naration, " +
                " Closing_Naration, " +
                " Not_Valid_Naration, " +
                " Attribute1, " +
                " Attribute2, " +
                " Attribute3, " +
                " Attribute4 " +
                " FROM " + TABLE_M_SURVEY_SET +
                " WHERE 1 = 1 " +
                " AND Survey_Tag = '" + strSurveyTag + "'; ";

        Log.d("[GudangGaram]", "MySQLiteHelper :: GetSurveySet Query : " +  kueri);

        List<CSurveySet> lvi  = new ArrayList<CSurveySet>();
        SQLiteDatabase db =  this.getReadableDatabase();

        ctr = 1;
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GetSurveySet Query count : " +  XCursor.getCount());
            lvi.clear();
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    Log.d("[GudangGaram]", "MySQLiteHelper :: GetSurveySet : XMICursor [" +  ctr + "]");

                    CSurveySet o = new CSurveySet();
                    o.setSurveyTag(XCursor.getString(0));
                    o.setSurveyDescription(XCursor.getString(1));
                    o.setIsActive(XCursor.getString(2));
                    o.setOpeningNaration(XCursor.getString(3));
                    o.setClosingNaration(XCursor.getString(4));
                    o.setNotValidNaration(XCursor.getString(5));
                    o.setAttribute1(XCursor.getString(6));
                    o.setAttribute2(XCursor.getString(7));
                    o.setAttribute3(XCursor.getString(8));
                    o.setAttribute4(XCursor.getString(9));

                    lvi.add(o);
                    XCursor.moveToNext();

                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GetSurveySet Exception : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
            Log.d("[GudangGaram]", "MySQLiteHelper :: GetSurveySet : lvi size = " +  lvi.size());
        }
        return lvi;
    }

    public String GetInnerLoopNextDefByOptionATag(String strSurveySet, String aTag){
        Log.d("[GudangGaram]", "MySQLiteHelper :: GetInnerLoopNextDefByOptionATag");
        String result = "";

        String kueri = " SELECT TRIM(InnerLoop_Next_Def) " +
                " FROM " + TABLE_M_ANSWER_OPTION +
                " WHERE 1 = 1 " +
                " AND Survey_Tag = '" + strSurveySet + "' " +
                " AND Answer_Option_Tag = '" + aTag + "'; ";

        Log.d("[GudangGaram]", "MySQLiteHelper :: GetInnerLoopNextDefByOptionATag Query : " +  kueri);

        SQLiteDatabase db =  this.getReadableDatabase();
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GetInnerLoopNextDefByOptionATag Query count : " +  XCursor.getCount());
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    result = XCursor.getString(0);
                    XCursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GetInnerLoopNextDefByOptionATag Exception : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
        }
        return result;
    }

    public List<CAnswerOption> GenerateAnswerOptionByATag(String strSurveyTag, String qTag, String qsTag,String strdelimitedAnswerOptionTag, String strAnswerType){
        String [] delim;
        String tmpTag;
        String replacetmpTag;
        String condition;

        List<CAnswerOption> result;
        result = new ArrayList<CAnswerOption>();
        Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOptionByATag (" + strSurveyTag + ") Parameters ");
        Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOptionByATag (" + strSurveyTag + ") strdelimitedAnswerOptionTag > " + strdelimitedAnswerOptionTag);

        delim = strdelimitedAnswerOptionTag.split("\\;");
        condition = "";
        if(delim.length > 0){
            if(strAnswerType.equals("CheckBoxGroup")){
                condition = condition + " AND Answer_option_Tag in (";
            }
            else if(strAnswerType.equals("MRadioGroup")){
                condition = condition + " AND Matrix_Question_Tag in (";
            }

            for (int i = 0; i< delim.length; i++){
                tmpTag         = delim[i].trim();
                replacetmpTag  = tmpTag.replace(qsTag,qTag);

                condition = condition + "'" + replacetmpTag + "'";
                if(i == delim.length-1){
                    condition = condition + ")";
                }
                else{
                    condition = condition + ",";
                }

                Log.d("[GudangGaram]", "PreLoadData :: GenerateAnswerOptionByATag : tmpTag        : " + tmpTag);
                Log.d("[GudangGaram]", "PreLoadData :: GenerateAnswerOptionByATag : replacetmpTag : " + replacetmpTag);
            }
        }
        else{
            condition = condition + " AND 1 = 1 ";
        }

        String kueri = " SELECT Survey_Tag," +
                "Question_Tag," +
                "Matrix_Question_Tag," +
                "Matrix_Question_Label," +
                "TextBox_Validation_Type," +
                "Answer_option_Tag," +
                "Answer_Option_Value," +
                "Override_Next_Def," +
                "Image_Name, " +
                "InnerLoop_Next_Def,  " +
                "Other_Flag, " +
                "Denial_Flag," +
                "Denial_Next_Def " +
                " FROM  " + TABLE_M_ANSWER_OPTION + " " +
                " WHERE Survey_Tag = '" + strSurveyTag + "' " +
                "   AND Question_Tag = '" + qTag + "' " + condition +
                " ORDER BY Survey_Tag, Question_Tag, cast(replace(replace(Answer_option_Tag,Question_Tag,''),'_','') as integer) ";

        Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOptionByATag (" + strSurveyTag + ") Query : " + kueri);

        SQLiteDatabase db =  this.getReadableDatabase();
        XCursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOptionByATag (" + strSurveyTag + ") Query count : " + XCursor.getCount());

            result.clear();
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    CAnswerOption ao= new CAnswerOption();
                    ao.setSurveyTag(XCursor.getString(0));
                    ao.setQuestionTag(XCursor.getString(1));
                    ao.setMatrixQuestionTag(XCursor.getString(2));
                    ao.setMatrixQuestionLabel(XCursor.getString(3));
                    ao.setTextBoxValidationType(XCursor.getString(4));
                    ao.setAnswerOptionTag(XCursor.getString(5));
                    ao.setAnswerOptionValue(XCursor.getString(6));
                    ao.setOverrideNextDef(XCursor.getString(7));
                    ao.setImageName(XCursor.getString(8));
                    ao.setInnerLoopNextDef(XCursor.getString(9));
                    ao.setAnswerOtherFlag(XCursor.getString(10));
                    ao.setDenialFlag(XCursor.getString(11));
                    ao.setDenialNextDef(XCursor.getString(12));

                    ao.setSelected(false);
                    result.add(ao);
                    XCursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOptionByATag (" + strSurveyTag + ") Query count : " + XCursor.getCount());
        }
        finally {
            XCursor.close();
        }
        return result;
    }


    public List<CAnswerOption> GenerateAnswerOptionByQTag(String strSurveyTag, String strQuestionTag){
        List<CAnswerOption> result;
        result = new ArrayList<CAnswerOption>();
        Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOptionByQTag (" + strSurveyTag + "," + strQuestionTag + ")");

        String kueri = " SELECT Survey_Tag," +
                "Question_Tag," +
                "Matrix_Question_Tag," +
                "Matrix_Question_Label," +
                "TextBox_Validation_Type," +
                "Answer_option_Tag," +
                "Answer_Option_Value," +
                "Override_Next_Def," +
                "Image_Name, " +
                "InnerLoop_Next_Def,  " +
                "Other_Flag, " +
                "Denial_Flag, " +
                "Denial_Next_Def " +
                " FROM  " + TABLE_M_ANSWER_OPTION + " " +
                " WHERE Survey_Tag = '" + strSurveyTag + "' " +
                "   AND Question_Tag = '" + strQuestionTag + "' " +
                " ORDER BY Survey_Tag, Question_Tag, cast(replace(replace(Answer_option_Tag,Question_Tag,''),'_','') as integer) ";

        SQLiteDatabase db =  this.getReadableDatabase();
        XCursor = db.rawQuery(kueri, null);
        try {
            result.clear();
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                while (!XCursor.isAfterLast()) {
                    CAnswerOption ao= new CAnswerOption();
                    ao.setSurveyTag(XCursor.getString(0));
                    ao.setQuestionTag(XCursor.getString(1));
                    ao.setMatrixQuestionTag(XCursor.getString(2));
                    ao.setMatrixQuestionLabel(XCursor.getString(3));
                    ao.setTextBoxValidationType(XCursor.getString(4));
                    ao.setAnswerOptionTag(XCursor.getString(5));
                    ao.setAnswerOptionValue(XCursor.getString(6));
                    ao.setOverrideNextDef(XCursor.getString(7));
                    ao.setImageName(XCursor.getString(8));
                    ao.setInnerLoopNextDef(XCursor.getString(9));
                    ao.setAnswerOtherFlag(XCursor.getString(10));
                    ao.setDenialFlag(XCursor.getString(11));
                    ao.setDenialNextDef(XCursor.getString(12));

                    ao.setSelected(false);
                    result.add(ao);
                    XCursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOptionByQTag (" + strSurveyTag + "," + strQuestionTag + ") Exception : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
        }
        return result;
    }
    public List<List<CAnswerOption>> GenerateAnswerOption(String strSurveyTag){
        List<List<CAnswerOption>> result = new ArrayList<List<CAnswerOption>>();

        String kueri = " SELECT Survey_Tag, " +
                " Question_TAG " +
                " FROM " + TABLE_M_QUESTION_SET +
                " WHERE 1 = 1 " +
                " AND Survey_Tag = '" + strSurveyTag + "' " +
                " ORDER BY Question_TAG; ";

        SQLiteDatabase db =  this.getReadableDatabase();
        XMICursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOption Query count : " +  XMICursor.getCount());
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    List<CAnswerOption> tmpList = new ArrayList<CAnswerOption>();
                    tmpList = GenerateAnswerOptionByQTag(strSurveyTag, XMICursor.getString(1).toString());
                    result.add(tmpList);
                    XMICursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOption Exception : " +  e.getMessage().toString());
        }
        finally {
            XMICursor.close();
        }
        return result;
    }


    public List<List<CAnswerOption>> GenerateAnswerOptionOverride(String strSurveyTag, String strQuestionTag, String qsTag, String strSelectedTag, String strAnswerType){
        List<List<CAnswerOption>> result = new ArrayList<List<CAnswerOption>>();

        String kueri = " SELECT Survey_Tag, " +
                " Question_TAG " +
                " FROM " + TABLE_M_QUESTION_SET +
                " WHERE 1 = 1 " +
                " AND Survey_Tag = '" + strSurveyTag + "' " +
                " AND Question_TAG = '" + strQuestionTag + "' " +
                " ORDER BY Question_TAG; ";

        SQLiteDatabase db =  this.getReadableDatabase();
        XMICursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOption Query count : " +  XMICursor.getCount());
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    List<CAnswerOption> tmpList = new ArrayList<CAnswerOption>();
                    //tmpList = GenerateAnswerOptionByQTag(strSurveyTag, XMICursor.getString(1).toString());

                    // GenerateAnswerOptionByATag(String strSurveyTag, String qTag, String qsTag,String strdelimitedAnswerOptionTag, String strAnswerType)
                    tmpList = GenerateAnswerOptionByATag(strSurveyTag,strQuestionTag, qsTag, strSelectedTag, strAnswerType);
                    result.add(tmpList);
                    XMICursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateAnswerOption Exception : " +  e.getMessage().toString());
        }
        finally {
            XMICursor.close();
        }
        return result;
    }

    public List<CQuestionSet> GenerateQuestion(String strSurveyTag) {
        Integer ctr;
        Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateQuestion(" + strSurveyTag+ ")");

        String kueri = " SELECT Survey_Tag, " +
                " Question_TAG, " +
                " Question_Value, " +
                " Question_Hint, " +
                " Answer_Type, " +
                " Default_Next, " +
                " Section_Label, " +
                " Interrupt_Info, " +
                " Interrupt_Duration, " +
                " Coherence_With, " +
                " InnerLoop_Block_Next, " +
                " InnerLoop_Dequeue_Node " +
                " FROM " + TABLE_M_QUESTION_SET +
                " WHERE 1 = 1 " +
                " AND Survey_Tag = '" + strSurveyTag + "' " +
                " ORDER BY Question_TAG; ";

        List<CQuestionSet> lvi  = new ArrayList<CQuestionSet>();
        SQLiteDatabase db =  this.getReadableDatabase();

        ctr = 1;
        XCursor = db.rawQuery(kueri, null);
        try {
            if (XCursor.getCount() > 0) {
                XCursor.moveToFirst();
                lvi.clear();
                while (!XCursor.isAfterLast()) {
                    CQuestionSet o = new CQuestionSet();
                    o.setSurveyTag(XCursor.getString(0).trim());
                    o.setQuestionTag(XCursor.getString(1).trim());
                    o.setQuestionValue(XCursor.getString(2).trim());
                    o.setQuestionHint(XCursor.getString(3).trim());
                    o.setAnswerType(XCursor.getString(4).trim());
                    o.setDefaultNext(XCursor.getString(5).trim());
                    o.setSectionLabel(XCursor.getString(6).trim());
                    o.setInterrupt_Info(XCursor.getString(7).trim());
                    o.setInterrupt_Duration(XCursor.getInt(8));
                    o.setCoherenceWith(XCursor.getString(9));
                    o.setInnerLoop_Block_Next(XCursor.getString(10));
                    o.setInnerLoop_Dequeue_Node(XCursor.getString(11));

                    lvi.add(o);
                    XCursor.moveToNext();

                    ctr +=1;
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateQuestion Exception : " +  e.getMessage().toString());
        }
        finally {
            XCursor.close();
            Log.d("[GudangGaram]", "MySQLiteHelper :: GenerateQuestion : lvi size = " +  lvi.size());
        }
        return lvi;
    }


    public Boolean IsSurveySetUsedByThisEventUsedTooByAnotherEvent(String strEventID){
        Boolean result = false;
        Integer ncount = 0;

        String kueri = " SELECT count(*) as nrow" +
                       " FROM " + TABLE_M_EVENT + " " +
                       " WHERE Survey_Tag IN (" +
                       "  SELECT Survey_Tag " +
                       "  FROM " + TABLE_M_EVENT + " " +
                       "  WHERE Event_ID = '" + strEventID + "') " +
                       " AND Event_ID <> '" + strEventID + "';";

        SQLiteDatabase db =  this.getReadableDatabase();
        XMICursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: IsSurveySetUsedByThisEventUsedTooByAnotherEvent Query count : " +  XMICursor.getCount());
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    ncount = XMICursor.getInt(0);
                    XMICursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: IsSurveySetUsedByThisEventUsedTooByAnotherEvent Exception : " +  e.getMessage().toString());
        }
        finally {
            XMICursor.close();
            if(ncount > 0){
                result = true;
            }
            else{
                result = false;
            }
        }

        return result;
    }

    public Boolean IsMatchByCoherenceCriteria(String cTag) {
        Boolean result = false;
        Integer ncount = 0;

        String kueri = " SELECT count(*) as nrow" +
                " FROM " + TABLE_M_ANSWER_OPTION + " " +
                " WHERE Coherence_Criteria_Tag = '" + cTag + "'";

        SQLiteDatabase db =  this.getReadableDatabase();
        XMICursor = db.rawQuery(kueri, null);
        try {
            Log.d("[GudangGaram]", "MySQLiteHelper :: IsMatchByCoherenceCriteria(" + cTag + ") Query count : " +  XMICursor.getCount());
            if (XMICursor.getCount() > 0) {
                XMICursor.moveToFirst();
                while (!XMICursor.isAfterLast()) {
                    ncount = XMICursor.getInt(0);
                    XMICursor.moveToNext();
                }
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "MySQLiteHelper :: IsMatchByCoherenceCriteria(" + cTag + ") Exception : " +  e.getMessage().toString());
        }
        finally {
            XMICursor.close();
            if(ncount > 0){
                result = true;
            }
            else{
                result = false;
            }
            Log.d("[GudangGaram]", "MySQLiteHelper :: IsMatchByCoherenceCriteria(" + cTag + ") Result : " +  result);
        }

        return result;
    }

    public void DeleteEvent(String strEventID){
        Integer rec_del_question_set = 0;
        Integer rec_del_answer_option = 0;
        Integer rec_del_survey_set = 0;
        Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent EventID = " + strEventID);
        SQLiteDatabase db  = this.getWritableDatabase();
        String pWhereDeleteSurveySet    = KEY_M_ST_SURVEY_TAG  +  " IN (SELECT " + KEY_M_EVT_SURVEY_TAG + " FROM " + TABLE_M_EVENT + " WHERE " + KEY_M_EVT_EVENT_ID + " = '" + strEventID + "'); ";
        String pWhereDeleteQuestionSet  = KEY_M_QS_SURVEY_TAG  +  " IN (SELECT " + KEY_M_EVT_SURVEY_TAG + " FROM " + TABLE_M_EVENT + " WHERE " + KEY_M_EVT_EVENT_ID + " = '" + strEventID + "'); ";
        String pWhereDeleteAnswerOption = KEY_M_AO_SURVEY_TAG  +  " IN (SELECT " + KEY_M_EVT_SURVEY_TAG + " FROM " + TABLE_M_EVENT + " WHERE " + KEY_M_EVT_EVENT_ID + " = '" + strEventID + "'); ";
        String pWhereDeleteEvent        = KEY_M_EVT_EVENT_ID + " = '" + strEventID + "' ";

        try{
            // check first if the event has survey set used by another event
            if(!IsSurveySetUsedByThisEventUsedTooByAnotherEvent(strEventID)){
                try{
                    // delete answer option by survey tag look up by event id
                    rec_del_answer_option = deleteTable(TABLE_M_ANSWER_OPTION, pWhereDeleteAnswerOption);
                    Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent -> DeleteAnswerOption Query = " + pWhereDeleteAnswerOption);
                    Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent -> DeleteAnswerOptionCount = " + rec_del_answer_option );
                    if(rec_del_answer_option > 0){
                        // delete question set by survey tag lookup by event id
                        rec_del_question_set = deleteTable(TABLE_M_QUESTION_SET, pWhereDeleteQuestionSet);
                        Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent -> DeleteQuestionSet Query = " +  pWhereDeleteQuestionSet);
                        Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent -> DeleteQuestionSetCount = " + rec_del_question_set);
                        if(rec_del_question_set > 0){
                            // delete survey set by survey tag look up by event id
                            rec_del_survey_set = deleteTable(TABLE_M_SURVEY_SET, pWhereDeleteSurveySet);
                            Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent -> DeleteSurveySet Query = " +  pWhereDeleteSurveySet);
                            Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent -> DeleteSurveySetCount = " + rec_del_survey_set);
                        }
                    }
                    if((rec_del_answer_option > 0)  && (rec_del_question_set > 0) && (rec_del_survey_set > 0)){
                        deleteTable(TABLE_M_EVENT, pWhereDeleteEvent);
                        Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent -> DeleteEvent Query = " + pWhereDeleteEvent);
                    }
                }
                catch(Exception e){
                    Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent Exception : " + e.getMessage().toString());
                }
            }
            else{
                deleteTable(TABLE_M_EVENT, pWhereDeleteEvent);
                Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent -> DeleteEvent Query = " + pWhereDeleteEvent);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteEvent Exception :" + e.getMessage().toString());
        }
    }

    public void DeleteSurveyH(String strSurveyHID){
        Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteSurveyH");
        SQLiteDatabase db  = this.getWritableDatabase();
        String pWehere = KEY_T_SH_HID + " = '" + strSurveyHID + "' ";
        try{
            db.delete(TABLE_T_SURVEY_H,pWehere, null);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteSurveyH Exception :" + e.getMessage().toString());
        }
    }
    public void DeleteSurveyD(String strSurveyHID){
        Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteSurveyH");
        SQLiteDatabase db  = this.getWritableDatabase();

        String pWehere = KEY_T_SD_HID + " = '" + strSurveyHID + "' ";
        try{
            db.delete(TABLE_T_SURVEY_D,pWehere, null);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MySQLiteHelper :: DeleteSurveyH Exception :" + e.getMessage().toString());
        }
    }

}
