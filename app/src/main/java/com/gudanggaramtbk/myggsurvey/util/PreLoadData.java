package com.gudanggaramtbk.myggsurvey.util;

import android.content.Context;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.model.CQuestionSet;

import java.util.ArrayList;
import java.util.List;

public class PreLoadData {
    List<CQuestionSet>            list_pertanyaan;
    List<List<CAnswerOption>>     list_pilihan;
    MyPreDefData                  predef;
    MySQLiteHelper                odbHelper;
    String                        osurveyTag;

    public PreLoadData(MySQLiteHelper dbHelper, String strSurveyTag){
        odbHelper  = dbHelper;
        osurveyTag = strSurveyTag;

        Log.d("[GudangGaram]", "PreLoadData (" + strSurveyTag + ")");
        try{
            // --------- read value from db master -----------
            list_pertanyaan  = dbHelper.GenerateQuestion(strSurveyTag);
            list_pilihan     = dbHelper.GenerateAnswerOption(strSurveyTag);

            // --------- object data source from predefined sample (for testing purpose only)---------
            //predef = new MyPreDefData();
            //list_pertanyaan = predef.GenerateQuestion();
            //list_pilihan    = predef.GenerateAnswerOption();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "PreLoadData (" + strSurveyTag + ") Exception : " + e.getMessage().toString());
        }
    }

    public CQuestionSet GetQuestion(String qTag){
        CQuestionSet result = null;
        try{
            Log.d("[GudangGaram]", "PreLoadData :: GetQuestion (" + qTag + ") ");
            for (CQuestionSet pertanyaan : list_pertanyaan) {
                if (pertanyaan.getQuestionTag().equals(qTag)) {
                    result =  pertanyaan;
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "PreLoadData :: GetQuestion (" + qTag + ") Exception : " + e.getMessage().toString());
        }
        return result;
    }


    public boolean IsThisNodeTheLastQuestion(String qTag){
        Log.d("[GudangGaram]", "PreLoadData :: IsThisNodeTheLastQuestion (" + qTag + ")");
        boolean result = false;
        CQuestionSet findQuestion;
        try{
            findQuestion = GetQuestion(qTag);
            Log.d("[GudangGaram]", "PreLoadData :: IsThisNodeTheLastQuestion (" + qTag + ") > " + findQuestion.getDefaultNext());
            if(findQuestion.getDefaultNext().equals("QE")){
                result = true;
            }
            else{
                result = false;
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "PreLoadData :: IsThisNodeTheLastQuestion (" + qTag + ") Exception : " + e.getMessage().toString());
            result = false;
        }
        return result;
    }



    public List<CAnswerOption> GetAnswerOptionByQtag(String qTag, String qsTag, String strSelectedTag){
        Log.d("[GudangGaram]", "PreLoadData :: GetAnswerOptionByQtag (" + qTag + ")");
        Integer idx;
        String strAnswerType;
        String strCoherenceWith;
        List<CAnswerOption> result = null;
        List<List<CAnswerOption>> selected_list_pilihan;

        strAnswerType    = GetAnswerTypeOfQuestionByQtag(qTag);
        Log.d("[GudangGaram]", "PreLoadData :: GetAnswerOptionByQtag (" + qTag + ") > CheckBoxGroup");
        // if coherence was set for next answer option then dont load from answer_option(qtag) rather than use prev data set selected
        strCoherenceWith = GetCoherenceWith(qTag).trim();
        if(strCoherenceWith.length() > 0){
                Log.d("[GudangGaram]", "PreLoadData :: GetAnswerOptionByQtag (" + qTag + ") > Coherence With " + strCoherenceWith);
                selected_list_pilihan = odbHelper.GenerateAnswerOptionOverride(osurveyTag, qTag, qsTag, strSelectedTag, strAnswerType);
        }
        else {
                Log.d("[GudangGaram]", "PreLoadData :: GetAnswerOptionByQtag (" + qTag + ") > No Coherence Set");
                selected_list_pilihan = list_pilihan;
        }

        Log.d("[GudangGaram]", "PreLoadData :: GetAnswerOptionByQtag (" + qTag + ") > selected_list_pilihan size = " + selected_list_pilihan.size());
        // -------- listing option jawaban terhadap Question Tag --------
        try{
            idx = 0;
            for (List<CAnswerOption> pilihan_jawaban : selected_list_pilihan) {
                Log.d("[GudangGaram]", "PreLoadData :: GetAnswerOptionByQtag (" + qTag + ")[" + idx + "]> " + pilihan_jawaban.get(0).getAnswerOptionTag() + " - " + pilihan_jawaban.get(0).getQuestionTag());
                if(pilihan_jawaban.get(0).getQuestionTag().equals(qTag.trim())){
                    result =  pilihan_jawaban;
                }
                idx = idx + 1;
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "PreLoadData :: GetAnswerOptionByQtag Exception :" + e.getMessage().toString());
        }
        return result;
    }



    public String GetOverrideNextByOptionATag(String aTag){
        Log.d("[GudangGaram]", "PreLoadData :: GetOverrideNextByOptionATag");
        Integer i,j;
        String result = "";

        try{
            for (List<CAnswerOption> ls : list_pilihan){
                for (CAnswerOption  cao  : ls) {
                    if (cao.getAnswerOptionTag().equals(aTag)) {
                        result =  cao.getOverrideNextDef();
                    }
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "PreLoadData :: GetOverrideNextByOptionATag Exception : " + e.getMessage());
        }

        return result;
    }

    public String GetDefaultNextQuestionByATag(String aTag){
        Log.d("[GudangGaram]", "PreLoadData :: GetDefaultNextQuestionByATag : aTag(" + aTag + ")");
        String result = "";
        String delimited_chr = ";";
        String[] separated;
        String keyword = "";
        Integer n_separated;
        Integer i;

        // for multiple answer collect indicate by ; delimiter
        if(aTag.contains(delimited_chr)){
           separated = aTag.split(delimited_chr);
           n_separated = separated.length;
           if(n_separated > 0){
                for(i=0; i<n_separated; i++){
                    keyword = separated[i];
                }
           }
           else{
               keyword = "";
           }
        }
        else{
            keyword = aTag;
        }

        Log.d("[GudangGaram]", "PreLoadData :: GetDefaultNextQuestionByATag : aTag(" + aTag + ") Keyword : " + keyword);
        if(keyword.equals("")){
            try {
                // iterate to find next route
                for (List<CAnswerOption> ls : list_pilihan) {
                    for (CAnswerOption cao : ls) {
                        if (cao.getAnswerOptionTag().equals(keyword)) {
                            result = GetOverrideDefaultNextQuestionByQTag(cao.getQuestionTag());
                        }
                    }
                }
            } catch (Exception e) {
                Log.d("[GudangGaram]", "PreLoadData :: GetDefaultNextQuestionByATag aTag(" + aTag + ") Exception : " + e.getMessage());
            }
        }
        else{
            result = "";
        }
        Log.d("[GudangGaram]", "PreLoadData :: GetDefaultNextQuestionByATag : aTag(" + aTag + ") Result : " + result);

        return result;
    }


    // ----------------------- getter ---------------------
    public String GetSectionByQTag(String qTag){
        return GetQuestion(qTag).getSectionLabel();
    }
    public String GetInterruptInfoByQTag(String qTag){
        return GetQuestion(qTag).getInterrupt_Info();
    }
    public Integer GetInterruptDurationByQTag(String qTag){
        return GetQuestion(qTag).getInterrupt_Duration();
    }
    public String GetSurveyTagOfQuestionByQTag(String qTag){
        Log.d("[GudangGaram]", "PreLoadData :: GetSurveyTagOfQuestionByQTag (" + qTag + ")");

        return GetQuestion(qTag).getSurveyTag();
    }
    public String GetQuestionTagByQTag(String qTag){
        return GetQuestion(qTag).getQuestionTag();
    }
    public String GetQuestionValueByQTag(String qTag){
        return GetQuestion(qTag).getQuestionValue();
    }
    public String GetQuestionHintByQTag(String qTag){
        return GetQuestion(qTag).getQuestionHint();
    }
    public String GetAnswerTypeOfQuestionByQtag(String qTag){
        return GetQuestion(qTag).getAnswerType();
    }
    public String GetDefaultNextQuestionByQTag(String qTag){
        return GetQuestion(qTag).getDefaultNext();
    }
    public String GetInnerLoopDequeueNodeByQTag(String qTag){
        String result = "";
        try{
            result = GetQuestion(qTag).getInnerLoop_Dequeue_Node().trim();
        }
        catch(Exception e){
            result = "";
        }
        return result;
    }

    public String GetOverrideDefaultNextQuestionByQTag(String qTag){
        return GetQuestion(qTag).getOverrideDefaultNext();
    }
    public String GetInnerLoopBlockNextByQtag(String qTag){
        String result = "";
        try{
            result = GetQuestion(qTag).getInnerLoop_Block_Next();
        }
        catch(Exception e){
            result = "";
        }
        return result;
    }
    public String GetCoherenceWith(String qTag){
        Log.d("[GudangGaram]", "PreLoadData :: GetCoherenceWith (" + qTag + ")");
         String result = "";
        try{
            result = String.valueOf(GetQuestion(qTag).getCoherenceWith());
        }
        catch(Exception e){
            result = "";
            Log.d("[GudangGaram]", "PreLoadData :: GetCoherenceWith (" + qTag + ") Exception : " + e.getMessage());
        }
        return result;
    }

    // ---------------------- setter ------------------------
    public void SetOverrideDefaultNextQuestionByQTag(String qTag, String aTag){
        GetQuestion(qTag).setOverrideDefaultNext(aTag);
    }

}
