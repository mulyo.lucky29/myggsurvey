package com.gudanggaramtbk.myggsurvey.model;

public class CSurveySet {
    private String SurveyTag;
    private String SurveyDescription;
    private String IsActive;
    private String OpeningNaration;
    private String ClosingNaration;
    private String NotValidNaration;
    private String Attribute1;
    private String Attribute2;
    private String Attribute3;
    private String Attribute4;

    // ---------------------- getter -----------------------
    public String getSurveyTag() {
        return SurveyTag;
    }
    public String getSurveyDescription() {
        return SurveyDescription;
    }
    public String getIsActive() {
        return IsActive;
    }
    public String getOpeningNaration() {
        return OpeningNaration;
    }
    public String getClosingNaration() {
        return ClosingNaration;
    }
    public String getNotValidNaration() {
        return NotValidNaration;
    }
    public String getAttribute1() {
        return Attribute1;
    }
    public String getAttribute2() {
        return Attribute2;
    }
    public String getAttribute3() {
        return Attribute3;
    }
    public String getAttribute4() {
        return Attribute4;
    }

    // -------------------- setter ------------------------
    public void setSurveyTag(String surveyTag) {
        SurveyTag = surveyTag;
    }
    public void setSurveyDescription(String surveyDescription) {
        SurveyDescription = surveyDescription;
    }
    public void setIsActive(String isActive) {
        IsActive = isActive;
    }
    public void setOpeningNaration(String openingNaration) {
        OpeningNaration = openingNaration;
    }
    public void setClosingNaration(String closingNaration) {
        ClosingNaration = closingNaration;
    }
    public void setNotValidNaration(String notValidNaration) {
        NotValidNaration = notValidNaration;
    }
    public void setAttribute1(String attribute1) {
        Attribute1 = attribute1;
    }
    public void setAttribute2(String attribute2) {
        Attribute2 = attribute2;
    }
    public void setAttribute3(String attribute3) {
        Attribute3 = attribute3;
    }
    public void setAttribute4(String attribute4) {
        Attribute4 = attribute4;
    }
}
