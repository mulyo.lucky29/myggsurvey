package com.gudanggaramtbk.myggsurvey.model;

public class CDynVariable {
    private String strVariableName;
    private String strVariableReplaced;

    // ---------------- getter ------------------------
    public String getStrVariableName() {
        return strVariableName;
    }
    public String getStrVariableReplaced() {
        return strVariableReplaced;
    }

    // ---------------- setter ------------------------
    public void setStrVariableName(String strVariableName) {
        this.strVariableName = strVariableName;
    }
    public void setStrVariableReplaced(String strVariableReplaced) {
        this.strVariableReplaced = strVariableReplaced;
    }
}
