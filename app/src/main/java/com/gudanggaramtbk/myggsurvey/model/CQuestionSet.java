package com.gudanggaramtbk.myggsurvey.model;

public class CQuestionSet {
    private String  SurveyTag;
    private String  QuestionTag;
    private String  QuestionValue;
    private String  QuestionHint;
    private String  AnswerType;
    private String  SelectedAnswerTag;
    private String  SelectedAnswerValue;
    private String  DefaultNext;
    private String  OverrideDefaultNext;
    private String  SectionLabel;
    private String  Interrupt_Info;
    private Integer Interrupt_Duration;
    private String  CoherenceWith;
    private String  InnerLoop_Block_Next;
    private String  InnerLoop_Dequeue_Node;

    // constructor

    public CQuestionSet(){}
    public CQuestionSet(String strSurveyTag, String strQuestionTag, String strDefaultNext, String strQuestionValue, String strQuestionHint, String strAnswerType){
        SurveyTag     = strSurveyTag;
        QuestionTag   = strQuestionTag;
        DefaultNext   = strDefaultNext;
        QuestionValue = strQuestionValue;
        QuestionHint  = strQuestionHint;
        AnswerType    = strAnswerType;
    }

    // ------------ getter --------------------
    public String getSurveyTag() {
        return SurveyTag;
    }
    public String getQuestionTag() {
        return QuestionTag;
    }
    public String getQuestionValue() {
        return QuestionValue;
    }
    public String getQuestionHint() {
        return QuestionHint;
    }
    public String getAnswerType() {
        return AnswerType;
    }
    public String getSelectedAnswerTag() { return SelectedAnswerTag; }
    public String getSelectedAnswerValue() {  return SelectedAnswerValue; }
    public String getDefaultNext() {
        return DefaultNext;
    }
    public String getOverrideDefaultNext() {
        return OverrideDefaultNext;
    }
    public String getSectionLabel() {
        return SectionLabel;
    }
    public String getInterrupt_Info() {
        return Interrupt_Info;
    }
    public Integer getInterrupt_Duration() {
        return Interrupt_Duration;
    }
    public String getCoherenceWith() {
        return CoherenceWith;
    }
    public String getInnerLoop_Block_Next() {
        return InnerLoop_Block_Next;
    }
    public String getInnerLoop_Dequeue_Node() {
        return InnerLoop_Dequeue_Node;
    }


    // ------------ setter --------------------
    public void setSurveyTag(String surveyTag) {
        SurveyTag = surveyTag;
    }
    public void setQuestionTag(String questionTag) {
        QuestionTag = questionTag;
    }
    public void setQuestionValue(String questionValue) {
        QuestionValue = questionValue;
    }
    public void setQuestionHint(String questionHint) {
        QuestionHint = questionHint;
    }
    public void setAnswerType(String answerType) {
        AnswerType = answerType;
    }
    public void setSelectedAnswerTag(String selectedAnswerTag) { SelectedAnswerTag = selectedAnswerTag; }
    public void setSelectedAnswerValue(String selectedAnswerValue) { SelectedAnswerValue = selectedAnswerValue; }
    public void setDefaultNext(String defaultNext) {
        DefaultNext = defaultNext;
    }
    public void setOverrideDefaultNext(String overrideDefaultNext) {
        OverrideDefaultNext = overrideDefaultNext;
    }
    public void setSectionLabel(String sectionLabel) {
        SectionLabel = sectionLabel;
    }
    public void setInterrupt_Info(String interrupt_Info) {
        Interrupt_Info = interrupt_Info;
    }
    public void setInterrupt_Duration(Integer interrupt_Duration) {
        Interrupt_Duration = interrupt_Duration;
    }
    public void setCoherenceWith(String coherenceWith) {
        CoherenceWith = coherenceWith;
    }
    public void setInnerLoop_Block_Next(String innerLoop_Block_Next) {
        InnerLoop_Block_Next = innerLoop_Block_Next;
    }
    public void setInnerLoop_Dequeue_Node(String innerLoop_Dequeue_Node) {
        InnerLoop_Dequeue_Node = innerLoop_Dequeue_Node;
    }
}
