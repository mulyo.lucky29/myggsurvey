package com.gudanggaramtbk.myggsurvey.model;

public class CMTag {
    private String strVal;
    private String strTag;
    private String isOther;
    private String otherVal;
    private String denialFlag;
    private String denialNextVal;


    public String getStrTag() {
        return strTag;
    }
    public String getStrVal() {
        return strVal;
    }
    public String getIsOther() {
        return isOther;
    }
    public String getOtherVal() {
        return otherVal;
    }
    public String getDenialFlag() {
        return denialFlag;
    }
    public String getDenialNextVal() {
        return denialNextVal;
    }

    public void setStrTag(String strTag) {
        this.strTag = strTag;
    }
    public void setStrVal(String strVal) {
        this.strVal = strVal;
    }
    public void setIsOther(String isOther) {
        this.isOther = isOther;
    }
    public void setOtherVal(String otherVal) {
        this.otherVal = otherVal;
    }
    public void setDenialFlag(String denialFlag) {
        this.denialFlag = denialFlag;
    }
    public void setDenialNextVal(String denialNextVal) {
        this.denialNextVal = denialNextVal;
    }



}
