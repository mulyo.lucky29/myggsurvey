package com.gudanggaramtbk.myggsurvey.model;

public class CLookup {
    private String LContext;
    private String LValue;
    private String LMmeaning;


    public String getLContext() {
        return LContext;
    }
    public String getLValue() {
        return LValue;
    }
    public String getLMmeaning() {
        return LMmeaning;
    }

    public void setLContext(String LContext) {
        this.LContext = LContext;
    }
    public void setLValue(String LValue) {
        this.LValue = LValue;
    }
    public void setLMmeaning(String LMmeaning) {
        this.LMmeaning = LMmeaning;
    }


}
