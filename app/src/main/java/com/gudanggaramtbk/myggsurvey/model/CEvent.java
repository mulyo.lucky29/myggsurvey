package com.gudanggaramtbk.myggsurvey.model;

public class CEvent {
    private String EventID;
    private String EventName;
    private String EventDescription;
    private String InterviewerName;
    private String EventStartDate;
    private String EventEndDate;
    private String EventSurveyTag;
    private String Attribute1;
    private String Attribute2;
    private String Attribute3;
    private String Attribute4;
    private String Attribute5;

    private String param_Opening_Naration;
    private String param_Closing_Naration;
    private String param_NotValid_Naration;


    // -------------------- getter ---------------------------
    public String getEventID() {
        return EventID;
    }
    public String getEventName() {
        return EventName;
    }
    public String getEventDescription() {
        return EventDescription;
    }
    public String getInterviewerName() {
        return InterviewerName;
    }
    public String getEventStartDate() {
        return EventStartDate;
    }
    public String getEventEndDate() {
        return EventEndDate;
    }
    public String getEventSurveyTag() {
        return EventSurveyTag;
    }
    public String getAttribute1() {
        return Attribute1;
    }
    public String getAttribute2() {
        return Attribute2;
    }
    public String getAttribute3() {
        return Attribute3;
    }
    public String getAttribute4() {
        return Attribute4;
    }
    public String getAttribute5() {
        return Attribute5;
    }
    public String getParam_Opening_Naration() {
        return param_Opening_Naration;
    }
    public String getParam_Closing_Naration() {
        return param_Closing_Naration;
    }
    public String getParam_NotValid_Naration() {
        return param_NotValid_Naration;
    }


    // -------------------- setter ---------------------------
    public void setEventID(String eventID) {
        EventID = eventID;
    }
    public void setEventName(String eventName) {
        EventName = eventName;
    }
    public void setEventDescription(String eventDescription) {
        EventDescription = eventDescription;
    }
    public void setInterviewerName(String interviewerName) {
        InterviewerName = interviewerName;
    }
    public void setEventStartDate(String eventStartDate) {
        EventStartDate = eventStartDate;
    }
    public void setEventEndDate(String eventEndDate) {
        EventEndDate = eventEndDate;
    }
    public void setEventSurveyTag(String eventSurveyTag) {
        EventSurveyTag = eventSurveyTag;
    }
    public void setAttribute1(String attribute1) {
        Attribute1 = attribute1;
    }
    public void setAttribute2(String attribute2) {
        Attribute2 = attribute2;
    }
    public void setAttribute3(String attribute3) {
        Attribute3 = attribute3;
    }
    public void setAttribute4(String attribute4) {
        Attribute4 = attribute4;
    }
    public void setAttribute5(String attribute5) {
        Attribute5 = attribute5;
    }
    public void setParam_Opening_Naration(String param_Opening_Naration) {
        this.param_Opening_Naration = param_Opening_Naration;
    }
    public void setParam_Closing_Naration(String param_Closing_Naration) {
        this.param_Closing_Naration = param_Closing_Naration;
    }
    public void setParam_NotValid_Naration(String param_NotValid_Naration) {
        this.param_NotValid_Naration = param_NotValid_Naration;
    }
}
