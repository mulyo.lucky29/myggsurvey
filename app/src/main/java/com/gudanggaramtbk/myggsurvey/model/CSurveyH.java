package com.gudanggaramtbk.myggsurvey.model;

public class CSurveyH {
    private String SurveyHID;
    private String InterviewerName;
    private String EventID;
    private String EventName;
    private String EventAttribute1;
    private String EventAttribute2;
    private String EventAttribute3;
    private String EventAttribute4;
    private String Session_ID;
    private String Survey_Tag;
    private String Survey_Desc;
    private String Device_ID;
    private String Status;
    private String Start_Date;
    private String End_Date;
    private String Cancel_Date;
    private String Cancel_Reason;
    private boolean Checked;

    // ------------------- getter ---------------------
    public String getSurveyHID() {
        return SurveyHID;
    }
    public String getSession_ID() {
        return Session_ID;
    }
    public String getSurvey_Tag() {
        return Survey_Tag;
    }
    public String getSurvey_Desc() {
        return Survey_Desc;
    }
    public String getDevice_ID() {
        return Device_ID;
    }
    public String getStatus() {
        return Status;
    }
    public String getStart_Date() {
        return Start_Date;
    }
    public String getEnd_Date() {
        return End_Date;
    }
    public String getCancel_Date() {
        return Cancel_Date;
    }
    public String getCancel_Reason() {
        return Cancel_Reason;
    }
    public boolean isChecked() {
        return Checked;
    }
    public String getInterviewerName() {
        return InterviewerName;
    }
    public String getEventID() {
        return EventID;
    }
    public String getEventName() {
        return EventName;
    }
    public String getEventAttribute1() {
        return EventAttribute1;
    }
    public String getEventAttribute2() {
        return EventAttribute2;
    }
    public String getEventAttribute3() {
        return EventAttribute3;
    }
    public String getEventAttribute4() {
        return EventAttribute4;
    }

    // ------------------- setter ----------------------
    public void setSurveyHID(String surveyHID) {
        SurveyHID = surveyHID;
    }
    public void setSession_ID(String session_ID) {
        Session_ID = session_ID;
    }
    public void setSurvey_Tag(String survey_Tag) {
        Survey_Tag = survey_Tag;
    }
    public void setSurvey_Desc(String survey_Desc) {
        Survey_Desc = survey_Desc;
    }
    public void setDevice_ID(String device_ID) {
        Device_ID = device_ID;
    }
    public void setStatus(String status) {
        Status = status;
    }
    public void setStart_Date(String start_Date) {
        Start_Date = start_Date;
    }
    public void setEnd_Date(String end_Date) {
        End_Date = end_Date;
    }
    public void setCancel_Date(String cancel_Date) {
        Cancel_Date = cancel_Date;
    }
    public void setCancel_Reason(String cancel_Reason) {
        Cancel_Reason = cancel_Reason;
    }
    public void setChecked(boolean checked) {
        Checked = checked;
    }
    public void setInterviewerName(String interviewerName) {
        InterviewerName = interviewerName;
    }
    public void setEventID(String eventID) {
        EventID = eventID;
    }
    public void setEventName(String eventName) {
        EventName = eventName;
    }
    public void setEventAttribute1(String eventAttribute1) {
        EventAttribute1 = eventAttribute1;
    }
    public void setEventAttribute2(String eventAttribute2) {
        EventAttribute2 = eventAttribute2;
    }
    public void setEventAttribute3(String eventAttribute3) {
        EventAttribute3 = eventAttribute3;
    }
    public void setEventAttribute4(String eventAttribute4) {
        EventAttribute4 = eventAttribute4;
    }

}
