package com.gudanggaramtbk.myggsurvey.model;

public class CSelectedAnswer {
    private String SelectedValue;
    private String SelectedTag;
    private String OtherFlag;
    private String SelectedAttribute;

    private String OverrideNextQuestion;
    private String InnerLoopNextQuestion;
    private String denialFlag;
    private String denialNextQuestion;

    // ------------- getter --------------
    public String getSelectedValue() {
        return SelectedValue;
    }
    public String getSelectedTag() {
        return SelectedTag;
    }
    public String getSelectedAttribute() {
        return SelectedAttribute;
    }
    public String getOverrideNextQuestion() {
        return OverrideNextQuestion;
    }
    public String getInnerLoopNextQuestion() {
        return InnerLoopNextQuestion;
    }
    public String getDenialFlag() {
        return denialFlag;
    }
    public String getDenialNextQuestion() {
        return denialNextQuestion;
    }
    public String getOtherFlag() {
        return OtherFlag;
    }

    // ------------- setter ----------------
    public void setSelectedValue(String selectedValue) {
        SelectedValue = selectedValue;
    }
    public void setSelectedTag(String selectedTag) {
        SelectedTag = selectedTag;
    }
    public void setSelectedAttribute(String selectedAttribute) {
        SelectedAttribute = selectedAttribute;
    }
    public void setOverrideNextQuestion(String overrideNextQuestion) {
        OverrideNextQuestion = overrideNextQuestion;
    }
    public void setInnerLoopNextQuestion(String innerLoopNextQuestion) {
        InnerLoopNextQuestion = innerLoopNextQuestion;
    }
    public void setDenialFlag(String denialFlag) {
        this.denialFlag = denialFlag;
    }
    public void setDenialNextQuestion(String denialNextQuestion) {
        this.denialNextQuestion = denialNextQuestion;
    }
    public void setOtherFlag(String otherFlag) {
        OtherFlag = otherFlag;
    }

}
