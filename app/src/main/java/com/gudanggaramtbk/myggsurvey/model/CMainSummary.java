package com.gudanggaramtbk.myggsurvey.model;

public class CMainSummary {
    private String EventID;
    private String EventName;
    private String  DateExec;
    private Integer RecValid;
    private Integer RecInvalid;
    private Integer RecCancelled;
    private Integer RecTotal;

    public String getEventID() {
        return EventID;
    }
    public String getEventName() {
        return EventName;
    }
    public String getDateExec() {
        return DateExec;
    }
    public Integer getRecValid() {
        return RecValid;
    }
    public Integer getRecInvalid() {
        return RecInvalid;
    }
    public Integer getRecCancelled() {
        return RecCancelled;
    }
    public Integer getRecTotal() {
        return RecTotal;
    }

    public void setEventID(String eventID) {
        EventID = eventID;
    }
    public void setEventName(String eventName) {
        EventName = eventName;
    }
    public void setDateExec(String dateExec) {
        DateExec = dateExec;
    }
    public void setRecValid(Integer recValid) {
        RecValid = recValid;
    }
    public void setRecInvalid(Integer recInvalid) {
        RecInvalid = recInvalid;
    }
    public void setRecCancelled(Integer recCancelled) {
        RecCancelled = recCancelled;
    }
    public void setRecTotal(Integer recTotal) {
        RecTotal = recTotal;
    }



}
