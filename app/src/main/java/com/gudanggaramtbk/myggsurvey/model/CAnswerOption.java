package com.gudanggaramtbk.myggsurvey.model;

public class CAnswerOption {
    String SurveyTag;
    String QuestionTag;
    String MatrixQuestionTag;
    String MatrixQuestionLabel;
    String TextBoxValidationType;
    String AnswerOptionTag;
    String AnswerOptionValue;
    String AnswerOtherFlag;
    String ImageName;
    String OverrideNextDef;
    String InnerLoopNextDef;
    String OtherValue;
    String OtherSelected;
    Boolean Selected;
    String DenialFlag;
    String DenialNextDef;



    public CAnswerOption(){
    }

    // -------------- constructor ------------------
    public CAnswerOption(String strQuestionTag, String strAnswerOptionTag, String strAnswerOptionValue, String strOverrideNextDef){
        QuestionTag       = strQuestionTag;
        AnswerOptionTag   = strAnswerOptionTag;
        AnswerOptionValue = strAnswerOptionValue;
        OverrideNextDef   = strOverrideNextDef;
    }
    // -------------- constructor matrix ------------------
    public CAnswerOption(String strQuestionTag, String strMatrixQuestionTag, String strMatrixQuestionLabel, String strAnswerOptionTag, String strAnswerOptionValue, String strOverrideNextDef){
        QuestionTag         = strQuestionTag;
        MatrixQuestionTag   = strMatrixQuestionTag;
        MatrixQuestionLabel = strMatrixQuestionLabel;
        AnswerOptionTag     = strAnswerOptionTag;
        AnswerOptionValue   = strAnswerOptionValue;
        OverrideNextDef     = strOverrideNextDef;
    }

    // --------------- getter ---------------------
    public String getSurveyTag() {
        return SurveyTag;
    }
    public String getQuestionTag() {
        return QuestionTag;
    }
    public String getAnswerOptionTag() {
        return AnswerOptionTag;
    }
    public String getAnswerOptionValue() {
        return AnswerOptionValue;
    }
    public String getOverrideNextDef() {
        return OverrideNextDef;
    }
    public String getMatrixQuestionTag() {
        return MatrixQuestionTag;
    }
    public String getMatrixQuestionLabel() {
        return MatrixQuestionLabel;
    }
    public String getTextBoxValidationType() {
        return TextBoxValidationType;
    }
    public String getImageName() {
        return ImageName;
    }
    public String getInnerLoopNextDef() {
        return InnerLoopNextDef;
    }
    public Boolean isSelected() {
        return Selected;
    }
    public String getAnswerOtherFlag() {
        return AnswerOtherFlag;
    }
    public String getOtherValue() {
        return OtherValue;
    }
    public String getOtherSelected() {
        return OtherSelected;
    }
    public String getDenialFlag() {
        return DenialFlag;
    }
    public String getDenialNextDef() {
        return DenialNextDef;
    }


    // -------------- setter ---------------------
    public void setSurveyTag(String surveyTag) {
        SurveyTag = surveyTag;
    }
    public void setQuestionTag(String questionTag) {
        QuestionTag = questionTag;
    }
    public void setAnswerOptionTag(String answerOptionTag) {
        AnswerOptionTag = answerOptionTag;
    }
    public void setAnswerOptionValue(String answerOptionValue) {
        AnswerOptionValue = answerOptionValue;
    }
    public void setOverrideNextDef(String overrideNextDef) {
        OverrideNextDef = overrideNextDef;
    }
    public void setMatrixQuestionTag(String MQuestionTag) {
        this.MatrixQuestionTag = MQuestionTag;
    }
    public void setMatrixQuestionLabel(String MQuestionLabel) {
        this.MatrixQuestionLabel = MQuestionLabel;
    }
    public void setTextBoxValidationType(String textBoxValidationType) {
        TextBoxValidationType = textBoxValidationType;
    }
    public void setImageName(String imageName) {
        ImageName = imageName;
    }
    public void setInnerLoopNextDef(String innerLoopNextDef) {
        InnerLoopNextDef = innerLoopNextDef;
    }
    public void setSelected(Boolean selected) {
        Selected = selected;
    }
    public void setAnswerOtherFlag(String answerOtherFlag) {
        AnswerOtherFlag = answerOtherFlag;
    }
    public void setOtherValue(String otherValue) {
        OtherValue = otherValue;
    }
    public void setOtherSelected(String otherSelected) {
        OtherSelected = otherSelected;
    }
    public void setDenialFlag(String denialFlag) {
        DenialFlag = denialFlag;
    }
    public void setDenialNextDef(String denialNextDef) {
        DenialNextDef = denialNextDef;
    }

}
