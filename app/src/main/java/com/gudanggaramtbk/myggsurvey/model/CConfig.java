package com.gudanggaramtbk.myggsurvey.model;

public class CConfig {
    private String CStrSoapNamespace;
    private String CstrSoapAddress;
    private String CStrUrlUpdater;

    public String getCStrSoapNamespace() {
        return CStrSoapNamespace;
    }
    public String getCstrSoapAddress() {
        return CstrSoapAddress;
    }
    public String getCStrUrlUpdater() {
        return CStrUrlUpdater;
    }


    public void setCStrSoapNamespace(String CStrSoapNamespace) {
        this.CStrSoapNamespace = CStrSoapNamespace;
    }
    public void setCstrSoapAddress(String cstrSoapAddress) {
        CstrSoapAddress = cstrSoapAddress;
    }
    public void setCStrUrlUpdater(String CStrUrlUpdater) {
        this.CStrUrlUpdater = CStrUrlUpdater;
    }

}
