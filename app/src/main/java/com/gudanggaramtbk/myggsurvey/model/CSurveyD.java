package com.gudanggaramtbk.myggsurvey.model;

public class CSurveyD {

    private String SurveyDID;
    private String SurveyHID;
    private String Seq_No;
    private String Question_Tag;
    private String Question_Value;
    private String Answer_Option_Tag;
    private String Answer_Option_Value;
    private String Start_Date;
    private String End_Date;
    private String Created_Date;
    private String Cancel_Date;
    private String Attribute1;
    private String Attribute2;
    private String Attribute3;
    private String Attribute4;
    private String Attribute5;


    public String getSurveyDID() {
        return SurveyDID;
    }
    public String getSurveyHID() {
        return SurveyHID;
    }
    public String getSeq_No() {
        return Seq_No;
    }
    public String getQuestion_Tag() {
        return Question_Tag;
    }
    public String getQuestion_Value() {
        return Question_Value;
    }
    public String getAnswer_Option_Tag() {
        return Answer_Option_Tag;
    }
    public String getAnswer_Option_Value() {
        return Answer_Option_Value;
    }
    public String getStart_Date() {
        return Start_Date;
    }
    public String getEnd_Date() {
        return End_Date;
    }
    public String getCreated_Date() {
        return Created_Date;
    }
    public String getCancel_Date() {
        return Cancel_Date;
    }
    public String getAttribute1() {
        return Attribute1;
    }
    public String getAttribute2() {
        return Attribute2;
    }
    public String getAttribute3() {
        return Attribute3;
    }
    public String getAttribute4() {
        return Attribute4;
    }
    public String getAttribute5() {
        return Attribute5;
    }


    public void setSurveyDID(String surveyDID) {
        SurveyDID = surveyDID;
    }
    public void setSurveyHID(String surveyHID) {
        SurveyHID = surveyHID;
    }
    public void setSeq_No(String seq_No) {
        Seq_No = seq_No;
    }
    public void setQuestion_Tag(String question_Tag) {
        Question_Tag = question_Tag;
    }
    public void setQuestion_Value(String question_Value) {
        Question_Value = question_Value;
    }
    public void setAnswer_Option_Tag(String answer_Option_Tag) {
        Answer_Option_Tag = answer_Option_Tag;
    }
    public void setAnswer_Option_Value(String answer_Option_Value) {
        Answer_Option_Value = answer_Option_Value;
    }
    public void setStart_Date(String start_Date) {
        Start_Date = start_Date;
    }
    public void setEnd_Date(String end_Date) {
        End_Date = end_Date;
    }
    public void setCreated_Date(String created_Date) {
        Created_Date = created_Date;
    }
    public void setCancel_Date(String cancel_Date) {
        Cancel_Date = cancel_Date;
    }
    public void setAttribute1(String attribute1) {
        Attribute1 = attribute1;
    }
    public void setAttribute2(String attribute2) {
        Attribute2 = attribute2;
    }
    public void setAttribute3(String attribute3) {
        Attribute3 = attribute3;
    }
    public void setAttribute4(String attribute4) {
        Attribute4 = attribute4;
    }
    public void setAttribute5(String attribute5) {
        Attribute5 = attribute5;
    }


}
