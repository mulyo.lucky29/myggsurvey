package com.gudanggaramtbk.myggsurvey.model;

public class CImage {
    private String pict_code;
    private String pict_name;

    // -------------- getter -------------------
    public String getPict_code() {
        return pict_code;
    }
    public String getPict_name() {
        return pict_name;
    }

    // ------------- setter ---------------------
    public void setPict_code(String pict_code) {
        this.pict_code = pict_code;
    }
    public void setPict_name(String pict_name) {
        this.pict_name = pict_name;
    }

}
