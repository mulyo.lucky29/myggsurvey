package com.gudanggaramtbk.myggsurvey.service;

public interface DownloadImageTaskAsyncResponse {
    void PostDownloadAction(String output);
}
