package com.gudanggaramtbk.myggsurvey.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.activity.MainActivity;
import com.gudanggaramtbk.myggsurvey.model.CSurveyH;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import java.util.List;

public class SendSurvey {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private MainActivity pActivity;

    // override constructor
    public SendSurvey(SharedPreferences PConfig,Context ctx, MySQLiteHelper dbHelper){
        Log.d("[GudangGaram]", "SendSurvey :: Constructor");
        this.context          = ctx;
        this.config           = PConfig;
        this.dbHelper         = dbHelper;
    }
    public void setParentActivity(MainActivity pActivity){
        this.pActivity = pActivity;
    }

    public void Send(final List<CSurveyH> listselected, final  String pDeviceID) {
        try {
            // call WS To send Database
            SendDatabaseTaskAsync SoapRequest = new SendDatabaseTaskAsync(new SendDatabaseTaskAsync.SendDatabaseTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                    // call WS to Send Log file
                    SendLogTaskAsync SoapRequest = new SendLogTaskAsync(new SendLogTaskAsync.SendLogTaskAsyncResponse() {
                        @Override
                        public void PostSentAction(String output) {
                            // call WS To Send Survey
                            SendSurveyTaskAsync SoapRequest = new SendSurveyTaskAsync(new SendSurveyTaskAsync.SendSurveyTaskAsyncResponse() {
                                @Override
                                public void PostSentAction(String HeaderID) {
                                    pActivity.EH_cmd_Refresh();
                                }
                            });
                            SoapRequest.setAttribute(context, pActivity, dbHelper, config, pDeviceID, listselected);
                            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                                //work on sgs3 android 4.0.4
                                Log.d("[GudangGaram]", "SendSurvey :: SendSurveyTaskAsync :: Using HC Higer");
                                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
                            }
                            else {
                                Log.d("[GudangGaram]", "SendSurvey :: SendSurveyTaskAsync :: Using HC Lower");
                                SoapRequest.execute(); // work on sgs2 android 2.3
                            }
                        }
                    });
                    SoapRequest.setAttribute(context, dbHelper, config, pDeviceID);
                    if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                        //work on sgs3 android 4.0.4
                        Log.d("[GudangGaram]", "SendSurvey :: SendLogTaskAsync :: Using HC Higer");
                        SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
                    }
                    else {
                        Log.d("[GudangGaram]", "SendSurvey :: SendLogTaskAsync :: Using HC Lower");
                        SoapRequest.execute(); // work on sgs2 android 2.3
                    }
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, pDeviceID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                Log.d("[GudangGaram]", "SendSurvey :: SendDatabaseTaskAsync :: Send Using HC Higer");
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                Log.d("[GudangGaram]", "SendSurvey :: SendDatabaseTaskAsync :: Send Using HC Lower");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "SendDatabase :: SendDatabaseTaskAsync :: Send Exception >> " + e.getMessage().toString());
        }
    }
}
