package com.gudanggaramtbk.myggsurvey.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import com.gudanggaramtbk.myggsurvey.model.CQuestionSet;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

public class DownloadQuestionSetTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context           context;
    private ProgressDialog    pd;
    private MySQLiteHelper    dbHelper;
    private String SentResponse;
    private String strPsurvey_tag;
    private String strDeviceID;

    private String wsr_id;
    private String wsr_survey_tag;
    private String wsr_question_tag;
    private String wsr_question_value;
    private String wsr_question_hint;
    private String wsr_answer_type;
    private String wsr_default_next;
    private String wsr_section_label;
    private String wsr_interrupt_info;
    private String wsr_coherence_with;
    private String wsr_innerloop_block_next;
    private String wsr_innerloop_dequeue_node;
    private Integer wsr_interrupt_duration;

    private ArrayAdapter<String> adapter;

    public DownloadQuestionSetTaskAsync(){ }
    public interface DownloadQuestionSetTaskAsyncResponse {
        void PostDownloadAction(String output);
    }
    public DownloadQuestionSetTaskAsync.DownloadQuestionSetTaskAsyncResponse delegate = null;
    public DownloadQuestionSetTaskAsync(DownloadQuestionSetTaskAsync.DownloadQuestionSetTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String p_survey_tag,
                             String StrDeviceID){
        Log.d("[GudangGaram]", "DownloadQuestionSetTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.strPsurvey_tag    = p_survey_tag;
        this.strDeviceID       = StrDeviceID;
        this.pd = new ProgressDialog(this.context);
    }


    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "DownloadQuestionSetTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.dismiss();
        pd.setMessage("Retrieving Qeustion Data");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        if(output!=null){
            Log.d("[GudangGaram]", "DownloadQuestionSetTaskAsync :: onPostExecute >> " + output);
            try{
                delegate.PostDownloadAction(output);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "DownloadQuestionSetTaskAsync :: onPostExecute Exception : " + e.getMessage().toString());
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Question_Set";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_survey_tag ===================
        PropertyInfo prop_p_survey_tag = new PropertyInfo();
        prop_p_survey_tag.setName("p_survey_tag");
        prop_p_survey_tag.setValue(strPsurvey_tag);
        prop_p_survey_tag.setType(String.class);

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(strDeviceID);
        prop_p_device_id.setType(String.class);

        request.addProperty(prop_p_device_id);
        request.addProperty(prop_p_survey_tag);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                Log.d("[GudangGaram]", "DownloadQuestionSetTaskAsync :: doInBackground >>> count  >>> " + ncount.toString());
                if(ncount > 0){
                    List<CQuestionSet> LCQuestionSet = new ArrayList<>();

                    // format PDU :
                    //0. <ID>1</ID>
                    //1. <SURVEY_TAG>SVT2</SURVEY_TAG>
                    //2. <QUESTION_TAG>Q1</QUESTION_TAG>
                    //3. <QUESTION_VALUE>Q1. Nama respondent </QUESTION_VALUE>
                    //4. <QUESTION_HINT xml:space="preserve"> </QUESTION_HINT>
                    //5. <ANSWER_TYPE>TextBox</ANSWER_TYPE>
                    //6. <DEFAULT_NEXT>Q2</DEFAULT_NEXT>
                    //7. <SECTION_LABEL>SECTION I. RESPONDENT PROFILE AND BACKGROUND</SECTION_LABEL>
                    //8. <INTERRUPT_INFO xml:space="preserve"> </INTERRUPT_INFO>
                    //9. <INTERRUPT_DELAY>-1</INTERRUPT_DELAY>
                    //10.<COHERENCE_TO_QUESTION_TAG xml:space="preserve"> </COHERENCE_TO_QUESTION_TAG>
                    //11.<INNERLOOP_BLOCK_NEXT xml:space="preserve"> </INNERLOOP_BLOCK_NEXT>
                    //12.<INNERLOOP_DEQUEUE_NODE xml:space="preserve"> </INNERLOOP_DEQUEUE_NODE>

                    for (int i = 0; i < ncount ;i++){
                        obj3 = (SoapObject) obj2.getProperty(i);

                        wsr_id                    = obj3.getProperty(0).toString();
                        wsr_survey_tag            = obj3.getProperty(1).toString();
                        wsr_question_tag          = obj3.getProperty(2).toString();
                        wsr_question_value        = obj3.getProperty(3).toString();
                        wsr_question_hint         = obj3.getProperty(4).toString();
                        wsr_answer_type           = obj3.getProperty(5).toString();
                        wsr_default_next          = obj3.getProperty(6).toString();
                        wsr_section_label         = obj3.getProperty(7).toString();
                        wsr_interrupt_info        = obj3.getProperty(8).toString();
                        wsr_interrupt_duration    = Integer.parseInt(obj3.getProperty(9).toString());
                        wsr_coherence_with        = obj3.getProperty(10).toString();
                        wsr_innerloop_block_next  = obj3.getProperty(11).toString();
                        wsr_innerloop_dequeue_node = obj3.getProperty(12).toString();


                        CQuestionSet oClass = new CQuestionSet();
                        oClass.setSurveyTag(wsr_survey_tag);
                        oClass.setQuestionTag(wsr_question_tag);
                        oClass.setQuestionValue(wsr_question_value);
                        oClass.setQuestionHint(wsr_question_hint);
                        oClass.setAnswerType(wsr_answer_type);
                        oClass.setDefaultNext(wsr_default_next);
                        oClass.setSectionLabel(wsr_section_label);
                        oClass.setInterrupt_Info(wsr_interrupt_info);
                        oClass.setInterrupt_Duration(wsr_interrupt_duration);
                        oClass.setCoherenceWith(wsr_coherence_with);
                        oClass.setInnerLoop_Block_Next(wsr_innerloop_block_next);
                        oClass.setInnerLoop_Dequeue_Node(wsr_innerloop_dequeue_node);

                        // add class asset to List
                        LCQuestionSet.add(oClass);

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        // publish progress
                        publishProgress("Downloading Question Set [" + Integer.toString(i+1) + " of " + Integer.toString(ncount) + "]");
                    }
                    // save to db Question Set
                    dbHelper.AddBulkQuestionSet(LCQuestionSet);
                }
            }
            catch(NullPointerException e){
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "SoapRetrieveLocAsyncTask  :: end doInBackground");

        return strPsurvey_tag;
    }
}