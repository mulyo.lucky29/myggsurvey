package com.gudanggaramtbk.myggsurvey.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.gudanggaramtbk.myggsurvey.activity.DownloadMasterActivity;
import com.gudanggaramtbk.myggsurvey.activity.DownloadSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CLookup;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

public class DownloadLookupTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private List<CLookup> result;
    private DownloadMasterActivity oPactivity;
    private String strDeviceID;

    public DownloadLookupTaskAsync(){ }
    public interface DownloadLookupTaskAsyncResponse {
        void PostDownloadAction(String output);
    }
    public DownloadLookupTaskAsync.DownloadLookupTaskAsyncResponse delegate = null;
    public DownloadLookupTaskAsync(DownloadLookupTaskAsync.DownloadLookupTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String StrDeviceID){
        Log.d("[GudangGaram]", "DownloadLookupTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.strDeviceID       = StrDeviceID;
        this.pd = new ProgressDialog(this.context);

        result = new ArrayList<CLookup>();
    }
    public void setParentActivity(DownloadMasterActivity oParent){
        this.oPactivity  = oParent;
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "DownloadLookupTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.dismiss();
        pd.setMessage("Retrieving Lookup Data");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        if (pd.isShowing()) {
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        if(output!=null){
            Log.d("[GudangGaram]", "DownloadLookupTaskAsync :: onPostExecute >> " + output);
            // refresh lookup record number
            oPactivity.EH_Reload_Record_Count("look");
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Lookup";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(strDeviceID);
        prop_p_device_id.setType(String.class);

        request.addProperty(prop_p_device_id);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                Log.d("[GudangGaram]", "DownloadLookupTaskAsync :: doInBackground >>> count  >>> " + ncount.toString());
                if(ncount > 0){
                    // format PDU :
                    // 0. <LOOKUP_CONTEXT>GS_MS_AREA</LOOKUP_CONTEXT>
                    // 1. <LOOKUP_VALUE>AO Jakarta</LOOKUP_VALUE>
                    // 2. <LOOKUP_DESCRIPTION>AO Jakarta</LOOKUP_DESCRIPTION>

                    // clear master lookup first before download
                    dbHelper.flushTable(dbHelper.getTableMLookup());
                    result.clear();
                    for (int i = 0; i < ncount ;i++){
                        obj3 = (SoapObject) obj2.getProperty(i);
                        CLookup oClass = new CLookup();
                        oClass.setLContext(obj3.getProperty(0).toString());
                        oClass.setLValue(obj3.getProperty(1).toString());
                        oClass.setLMmeaning(obj3.getProperty(2).toString());
                        // add class asset to List result
                        result.add(oClass);
                    }
                    dbHelper.AddBulkLookup(result);
                }
            }
            catch(NullPointerException e){
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadLookupTaskAsync  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "DownloadLookupTaskAsync  :: end doInBackground");

        return "";
    }
}