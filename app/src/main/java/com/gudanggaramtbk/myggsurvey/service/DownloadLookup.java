package com.gudanggaramtbk.myggsurvey.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.activity.DownloadMasterActivity;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;

public class DownloadLookup {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private DownloadMasterActivity PActivity;

    // override constructor
    public DownloadLookup(SharedPreferences PConfig) {
        Log.d("[GudangGaram]", "DownloadLookup :: Constructor");
        this.config = PConfig;
    }

    public void setContext(Context context) {
        Log.d("[GudangGaram]", "DownloadLookup :: SetContext");
        this.context = context;
    }

    public void setParentActivity(DownloadMasterActivity oParent) {
        this.PActivity = oParent;
    }

    public void setDBHelper(MySQLiteHelper dbHelper) {
        Log.d("[GudangGaram]", "DownloadLookup :: SetDBHelper");
        this.dbHelper = dbHelper;
    }

    public String Retrieve(String strDeviceID) {
        Log.d("[GudangGaram]", "DownloadLookup :: Retrieve Begin");
        try {
            DownloadLookupTaskAsync SoapRequest = new DownloadLookupTaskAsync(new DownloadLookupTaskAsync.DownloadLookupTaskAsyncResponse() {
                @Override
                public void PostDownloadAction(String output) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, strDeviceID);
            SoapRequest.setParentActivity(PActivity);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            } else {
                Log.d("[GudangGaram]", "DownloadLookup :: Execute Begin");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Log.d("[GudangGaram]", "DownloadLookup :: Retrieve End");
        }
        return "";
    }
}