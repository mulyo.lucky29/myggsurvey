package com.gudanggaramtbk.myggsurvey.service;

public interface DownloadTaskAsyncResponse {
    void PostDownloadAction(String output);
}
