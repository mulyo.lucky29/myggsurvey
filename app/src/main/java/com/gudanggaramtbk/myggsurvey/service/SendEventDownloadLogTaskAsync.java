package com.gudanggaramtbk.myggsurvey.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.model.CLookup;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

public class SendEventDownloadLogTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private List<CLookup> result;
    private String strDeviceID;
    private String strEventID;

    public SendEventDownloadLogTaskAsync(){ }
    public interface SendEventDownloadLogTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SendEventDownloadLogTaskAsync.SendEventDownloadLogTaskAsyncResponse delegate = null;
    public SendEventDownloadLogTaskAsync(SendEventDownloadLogTaskAsync.SendEventDownloadLogTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String StrDeviceID,
                             String StrEventID){
        Log.d("[GudangGaram]", "SendEventDownloadLogTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.strDeviceID       = StrDeviceID;
        this.strEventID        = StrEventID;
        this.pd = new ProgressDialog(this.context);

        result = new ArrayList<CLookup>();
    }

    /*
    public void setParentActivity(DownloadMasterActivity oParent){
        this.oPactivity  = oParent;
    }
    */

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SendEventDownloadLogTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.dismiss();
        pd.setMessage("Sending Download Event State");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        if (pd.isShowing()) {
            pd.dismiss();
            pd.setMessage(args[0].toString());
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SendDatabaseTaskAsync:: onPostExecute >> " + SentResponse);
        delegate.PostSentAction(output);
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Insert_Log_Download_Event";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;                        //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(strDeviceID);
        prop_p_device_id.setType(String.class);

        // =============== p_event_id ===================
        PropertyInfo prop_p_event_id = new PropertyInfo();
        prop_p_event_id.setName("p_event_id");
        prop_p_event_id.setValue(strEventID);
        prop_p_event_id.setType(String.class);

        request.addProperty(prop_p_device_id);
        request.addProperty(prop_p_event_id);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);
            SoapObject result = (SoapObject) envelope.bodyIn;
            String TResult = result.getProperty(0).toString();
            Log.d("[GudangGaram]", "SendEventDownloadLogTaskAsync Response > " + String.valueOf(TResult));
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "SendEventDownloadLogTaskAsync  :: Exception " + e.getMessage().toString());
        }

        Log.d("[GudangGaram]", "SendEventDownloadLogTaskAsync  :: end doInBackground");

        return "";
    }
}
