package com.gudanggaramtbk.myggsurvey.service;

public interface SendLogTaskAsyncResponse {
    void PostSentAction(String output);
}
