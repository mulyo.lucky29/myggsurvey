package com.gudanggaramtbk.myggsurvey.service;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;

public class DownloadSurveySet {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private Activity pActivity;

    // override constructor
    public DownloadSurveySet(SharedPreferences PConfig,
                             Context ctx,
                             MySQLiteHelper dbHelper) {
        Log.d("[GudangGaram]", "DownloadSurveySet :: Constructor");
        this.context   = ctx;
        this.config    = PConfig;
        this.dbHelper  = dbHelper;
    }

    public void setParentActivity(Activity pActivity) {
        this.pActivity = pActivity;
    }

    public void Download(final String strSurveyTag, String strDeviceID) {
        Log.d("[GudangGaram]", "DownloadSurveySet :: Download > Begin");
        try{
            Download_Survey_Set(strSurveyTag, strDeviceID);
        }
        catch(Exception e){
        }
    }

    public void Download_Survey_Set(String strSurveyTag, final String strDeviceID){
        Log.d("[GudangGaram]", "DownloadSurveySet :: Download_Survey_Set : SurveyTag = " + strSurveyTag);
        try {
            // call WS To Download Survey Set
            DownloadSurveySetTaskAsync SoapRequest = new DownloadSurveySetTaskAsync(new DownloadSurveySetTaskAsync.DownloadSurveySetTaskAsyncResponse() {
                @Override
                public void PostDownloadAction(String strSurveyTag) {
                    Log.d("[GudangGaram]", "DownloadSurveySet :: Download_Survey_Set :: PostSentAction : " + strSurveyTag);
                    // continue to download Question Set
                    Download_Question_Set(strSurveyTag, strDeviceID);
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, strSurveyTag, strDeviceID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            } else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadSurveySet :: Download_Survey_Set > Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void Download_Question_Set(String strSurveyTag, final String strDeviceID){
        Log.d("[GudangGaram]", "DownloadSurveySet :: Download_Question_Set : SurveyTag = " + strSurveyTag);
        try {
            // call WS To Download Survey Question
            DownloadQuestionSetTaskAsync SoapRequest = new DownloadQuestionSetTaskAsync(new DownloadQuestionSetTaskAsync.DownloadQuestionSetTaskAsyncResponse() {
                @Override
                public void PostDownloadAction(String strSurveyTag) {
                    Log.d("[GudangGaram]", "DownloadSurveySet :: Download_Question_Set :: PostSentAction : " + strSurveyTag);
                    // continue to download survey answer option
                    Download_Survey_Answer_Option(strSurveyTag, strDeviceID);
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, strSurveyTag, strDeviceID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            } else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadSurveySet :: Download_Question_Set > Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void Download_Survey_Answer_Option(String strSurveyTag, final String strDeviceID){
        Log.d("[GudangGaram]", "DownloadSurveySet :: Download_Survey_Answer_Option : SurveyTag = " + strSurveyTag);
        // call ws to download survey answer option based on Survey Tag
        try {
            DownloadAnswerOptionTaskAsync SoapRequest = new DownloadAnswerOptionTaskAsync(new DownloadAnswerOptionTaskAsync.DownloadAnswerOptionTaskAsyncResponse() {
                @Override
                public void PostDownloadAction(String StrID) {
                    Download_Survey_Image(strDeviceID);
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, strSurveyTag, strDeviceID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            } else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]:", "DownloadSurveySet :: Download_Survey_Answer_Option Exception " + e.getMessage().toString());
        }
    }

    public void Download_Survey_Image(String strDeviceID){
        Log.d("[GudangGaram]", "DownloadSurveySet :: Download_Survey_Image");
        // call ws to download survey answer option based on Survey Tag
        try {
            DownloadImageTaskAsync SoapRequest = new DownloadImageTaskAsync(new DownloadImageTaskAsync.DownloadImageTaskAsyncResponse() {
                @Override
                public void PostDownloadAction(String StrID) {
                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, strDeviceID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            } else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]:", "DownloadSurveySet :: Download_Survey_Image Exception " + e.getMessage().toString());
        }
    }
}