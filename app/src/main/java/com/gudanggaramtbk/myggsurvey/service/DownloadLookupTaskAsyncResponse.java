package com.gudanggaramtbk.myggsurvey.service;

public interface DownloadLookupTaskAsyncResponse {
    void PostDownloadAction(String output);
}
