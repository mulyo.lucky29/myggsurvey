package com.gudanggaramtbk.myggsurvey.service;

public interface DownloadAnswerOptionTaskAsyncResponse {
    void PostDownloadAction(String output);
}
