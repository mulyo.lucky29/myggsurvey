package com.gudanggaramtbk.myggsurvey.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.activity.DownloadSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CImage;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class DownloadImageTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private List<CImage> result;
    private String fileName;
    private String folder;
    private String strDeviceID;
    private boolean isDownloaded;


    public DownloadImageTaskAsync(){ }
    public interface DownloadImageTaskAsyncResponse {
        void PostDownloadAction(String output);
    }
    public DownloadImageTaskAsync.DownloadImageTaskAsyncResponse delegate = null;
    public DownloadImageTaskAsync(DownloadImageTaskAsync.DownloadImageTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config, String StrDeviceID){
        Log.d("[GudangGaram]", "DownloadImageTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.strDeviceID       = StrDeviceID;
        this.pd = new ProgressDialog(this.context);
        result = new ArrayList<CImage>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "DownloadImageTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.dismiss();
        pd.setMessage("Retrieving Image Data");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        if(output!=null){
            Log.d("[GudangGaram]", "DownloadImageTaskAsync :: onPostExecute >> " + output);
            try{
                delegate.PostDownloadAction(output);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "DownloadImageTaskAsync :: onPostExecute Exception : " + e.getMessage().toString());
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;
        String durl;
        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        //String IMAGE_ADDRESS  = get_image_address(SOAP_ADDRESS);
        String OPERATION_NAME = "Get_List_Survey_Image";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(strDeviceID);
        prop_p_device_id.setType(String.class);

        request.addProperty(prop_p_device_id);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                Log.d("[GudangGaram]", "DownloadImageTaskAsync :: doInBackground >>> count  >>> " + ncount.toString());
                if(ncount > 0){
                    // format PDU :
                    //0. <PICT_CODE>Q8_1_SPANDUK</PICT_CODE>
                    //1. <PICT_FILE_NAME>Q8_1_SPANDUK.png</PICT_FILE_NAME>

                    result.clear();
                    for (int i = 0; i < ncount ;i++){
                        obj3 = (SoapObject) obj2.getProperty(i);
                        CImage oClass = new CImage();
                        oClass.setPict_code(obj3.getProperty(0).toString());
                        oClass.setPict_name(obj3.getProperty(1).toString());


                        durl = download_image(obj3.getProperty(1).toString());
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        // publish progress
                        publishProgress("Downloading Image From " + durl);

                        // add class image to List result
                        result.add(oClass);
                    }
                    // save to db
                    dbHelper.AddBulkImage(result);
                }
            }
            catch(NullPointerException e){
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadImageTaskAsync  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "DownloadImageTaskAsync  :: end doInBackground");

        return "";
    }

    private String get_path_image_url(String strSoapAddress){
        String result = "";
        String root_path_image;
        // strSoapAddress = http://10.50.131.18/WSGGSurvey/ggsurvey.asmx
        // imageURL = "http://10.50.131.18/WSGGSurvey/image/" + strImageName;
        Log.d("[GudangGaram]", "DownloadImageTaskAsync  :: get_image_address");
        try{
            if(strSoapAddress.contains(".asmx")){
                root_path_image = strSoapAddress.substring(0,strSoapAddress.lastIndexOf("/"));
                result = root_path_image + "/image/";
            }
            else{
                result = "";
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "DownloadImageTaskAsync  :: get_image_address Exception : " + e.getMessage().toString());
            result = "";
        }
        Log.d("[GudangGaram]", "DownloadImageTaskAsync  :: get_image_address >> " + result);
        return result;
    }


    private String download_image(String strImageName){
        String imageURL;
        Bitmap bitmap = null;
        int count;
        //imageURL = "http://10.50.131.18/WSGGSurvey/image/" + strImageName;
        imageURL = get_path_image_url(config.getString("SoapAddress", "")) + strImageName;
        if(imageURL.length() > 0){
            try {
                if(isSDCardPresent()){
                    URL url = new URL(imageURL);
                    URLConnection connection = url.openConnection();
                    connection.connect();
                    // getting file length
                    int lengthOfFile = connection.getContentLength();
                    InputStream input = new BufferedInputStream(url.openStream(), 8192);
                    //External directory path to save file
                    folder = Environment.getExternalStorageDirectory() + File.separator + "ggsurvey/";
                    //Create ggsurvey folder if it does not exist
                    File directory = new File(folder);
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }
                    // Output stream to write file
                    OutputStream output = new FileOutputStream(folder + strImageName);

                    byte data[] = new byte[1024];
                    long total = 0;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        //publishProgress("Downloading Image File " + strImageName + " " + (int) ((total * 100) / lengthOfFile));
                        // writing data to file
                        output.write(data, 0, count);
                    }

                    // flushing output
                    output.flush();
                    // closing streams
                    output.close();
                    input.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } // end if
        return imageURL;
    }

    public static boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }
}

