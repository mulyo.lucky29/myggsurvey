package com.gudanggaramtbk.myggsurvey.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.util.Logsutil;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class SendLogTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private Integer LPosition;
    private String deviceid;
    private Logsutil lgu;

    public SendLogTaskAsync(){
    }

    public interface SendLogTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SendLogTaskAsync.SendLogTaskAsyncResponse delegate = null;

    public SendLogTaskAsync(SendLogTaskAsync.SendLogTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String deviceid){

        Log.d("[GudangGaram]", "SendLogTaskAsync :: setAttribute");
        this.context        = context;
        this.dbHelper       = dbHelper;
        this.config         = config;
        this.deviceid       = deviceid;
        this.pd = new ProgressDialog(this.context);
        lgu = new Logsutil();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SendLogTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Process Backup Log");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }
    @Override
    protected  void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        Log.d("[GudangGaram]", "SendLogTaskAsync :: onProgressUpdate");
        pd.setMessage(args[0].toString());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
    }

    @Override
    protected void onPostExecute(String output) {
        pd.dismiss();
        Log.d("[GudangGaram]", "SendLogTaskAsync :: onPostExecute >> " + SentResponse);
        // clean up logs
        try{
            lgu.clearLogs();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "SendLogTaskAsync :: onPostExecute Exception : " + e.getMessage());
        }
        delegate.PostSentAction(output);
    }

    @Override
    protected String doInBackground(String... params) {
        int Result;
        int count;
        String dbName;

        String NAMESPACE        = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS     = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME   = "Backup_Log";

        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;                         //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        try{

                    byte[] bytesArray = lgu.readLogs().toString().getBytes();
                    String  xp_byte_file            = Base64.encodeToString(bytesArray, Base64.DEFAULT);
                    String  xp_device_id            = deviceid;
                    // =============== p_device_id ====================
                    PropertyInfo prop_p_device_id = new PropertyInfo();
                    prop_p_device_id.setName("p_device_id");
                    prop_p_device_id.setValue(xp_device_id);
                    prop_p_device_id.setType(String.class);
                    request.addProperty(prop_p_device_id);

                    // =============== p_dbcontent ===================
                    PropertyInfo prop_p_dbcontent = new PropertyInfo();
                    prop_p_dbcontent.setName("p_logcontent");
                    prop_p_dbcontent.setValue(xp_byte_file);
                    prop_p_dbcontent.setType(String.class);
                    request.addProperty(prop_p_dbcontent);

                    Log.d("[GudangGaram]", "SendLogTaskAsync : p_device_id : " + xp_device_id);

                    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                    envelope.dotNet = true;
                    envelope.setOutputSoapObject(request);

                    //Web method call
                    try {
                        HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                        httpTransport.debug = true;
                        httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                        httpTransport.call(SOAP_ACTION, envelope);
                        SoapObject result = (SoapObject) envelope.bodyIn;
                        String TResult = result.getProperty(0).toString();
                        Log.d("[GudangGaram]", "SendLogTaskAsync Response > " + String.valueOf(TResult));
                    }
                    catch (Exception ex) {
                        Log.d("[GudangGaram]", "SendLogTaskAsync Catch : " + ex.getMessage().toString());
                    }

                    try {
                        Thread.sleep(20);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // publish progress
                    publishProgress("Sending Backup Log inprogress");
        }
        catch(Exception e){
        }

        Log.d("[GudangGaram]:", "SendLogTaskAsync  :: end doInBackground");

        return SentResponse;
    }
}
