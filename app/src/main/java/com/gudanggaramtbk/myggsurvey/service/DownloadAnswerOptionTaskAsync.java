package com.gudanggaramtbk.myggsurvey.service;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

public class DownloadAnswerOptionTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private String strPsurvey_tag;
    private String strDeviceID;
    private String wsr_id;
    private String wsr_survey_tag;
    private String wsr_question_tag;
    private String wsr_answer_option_tag;
    private String wsr_answer_option_value;
    private String wsr_override_next_def;
    private String wsr_matrix_question_tag;
    private String wsr_matrix_question_label;
    private String wsr_textbox_validation_type;
    private String wsr_image_name;
    private String wsr_innerloop_next_def;
    private String wsr_other_flag;
    private String wsr_denial_flag;
    private String wsr_denial_next_def;


    private ArrayAdapter<String> adapter;
    private Activity PActivity;

    public DownloadAnswerOptionTaskAsync(){ }

    public interface DownloadAnswerOptionTaskAsyncResponse {
        void PostDownloadAction(String output);
    }
    public DownloadAnswerOptionTaskAsync.DownloadAnswerOptionTaskAsyncResponse delegate = null;
    public DownloadAnswerOptionTaskAsync(DownloadAnswerOptionTaskAsync.DownloadAnswerOptionTaskAsyncResponse delegate){
        this.delegate = delegate;
    }
    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String p_survey_tag,
                             String StrDeviceID){
        Log.d("[GudangGaram]", "DownloadAnswerOptionTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.strPsurvey_tag    = p_survey_tag;
        this.strDeviceID       = StrDeviceID;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "DownloadAnswerOptionTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.dismiss();
        pd.setMessage("Retrieving Answer Option Data");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        if(output!=null){
            Log.d("[GudangGaram]", "DownloadAnswerOptionTaskAsync :: onPostExecute >> " + output);
            try{
                delegate.PostDownloadAction(output);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "DownloadAnswerOptionTaskAsync :: onPostExecute Exception : " + e.getMessage().toString());
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Answer_Option";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_survey_tag ===================
        PropertyInfo prop_p_survey_tag = new PropertyInfo();
        prop_p_survey_tag.setName("p_survey_tag");
        prop_p_survey_tag.setValue(strPsurvey_tag);
        prop_p_survey_tag.setType(String.class);

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(strDeviceID);
        prop_p_device_id.setType(String.class);

        request.addProperty(prop_p_device_id);
        request.addProperty(prop_p_survey_tag);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                Log.d("[GudangGaram]", "DownloadAnswerOptionTaskAsync :: doInBackground >>> count  >>> " + ncount.toString());
                if(ncount > 0){
                    List<CAnswerOption> LCAnswerOption = new ArrayList<>();

                    // format PDU :
                    // 0.  <ID>185</ID>
                    // 1.  <SURVEY_TAG>SVT2</SURVEY_TAG>
                    // 2.  <QUESTION_TAG>Q1</QUESTION_TAG>
                    // 3.  <MATRIX_QUESTION_TAG xml:space="preserve"> </MATRIX_QUESTION_TAG>
                    // 4.  <MATRIX_QUESTION_LABEL xml:space="preserve"> </MATRIX_QUESTION_LABEL>
                    // 5.  <TEXTBOX_VALIDATION_TYPE xml:space="preserve"> </TEXTBOX_VALIDATION_TYPE>
                    // 6.  <ANSWER_OPTION_TAG>Q1_1</ANSWER_OPTION_TAG>
                    // 7.  <ANSWER_OPTION_VALUE xml:space="preserve"> </ANSWER_OPTION_VALUE>
                    // 8.  <IMAGE_NAME xml:space="preserve"> </IMAGE_NAME>
                    // 9.  <OVERRIDE_NEXT_DEF>Q2</OVERRIDE_NEXT_DEF>
                    //10.  <INNER_LOOP_NEXT_DEF xml:space="preserve"> </INNER_LOOP_NEXT_DEF>
                    //11.  <OTHER_FLAG xml:space="preserve"> </OTHER_FLAG>
                    //12.  <DENIAL_FLAG xml:space="preserve"> </DENIAL_FLAG>
                    //13.  <DENIAL_NEXT_DEF xml:space="preserve"> </DENIAL_NEXT_DEF>

                    for (int i = 0; i < ncount ;i++){
                        obj3 = (SoapObject) obj2.getProperty(i);

                        wsr_id                      = obj3.getProperty(0).toString();
                        wsr_survey_tag              = obj3.getProperty(1).toString();
                        wsr_question_tag            = obj3.getProperty(2).toString();
                        wsr_matrix_question_tag     = obj3.getProperty(3).toString();
                        wsr_matrix_question_label   = obj3.getProperty(4).toString();
                        wsr_textbox_validation_type = obj3.getProperty(5).toString();
                        wsr_answer_option_tag       = obj3.getProperty(6).toString();
                        wsr_answer_option_value     = obj3.getProperty(7).toString();
                        wsr_image_name              = obj3.getProperty(8).toString();
                        wsr_override_next_def       = obj3.getProperty(9).toString();
                        wsr_innerloop_next_def      = obj3.getProperty(10).toString();
                        wsr_other_flag              = obj3.getProperty(11).toString();
                        wsr_denial_flag             = obj3.getProperty(12).toString();
                        wsr_denial_next_def         = obj3.getProperty(13).toString();

                        CAnswerOption oClass = new CAnswerOption();
                        oClass.setQuestionTag(wsr_question_tag);
                        oClass.setSurveyTag(wsr_survey_tag);
                        oClass.setMatrixQuestionTag(wsr_matrix_question_tag);
                        oClass.setMatrixQuestionLabel(wsr_matrix_question_label);
                        oClass.setTextBoxValidationType(wsr_textbox_validation_type);
                        oClass.setAnswerOptionTag(wsr_answer_option_tag);
                        oClass.setAnswerOptionValue(wsr_answer_option_value);
                        oClass.setImageName(wsr_image_name);
                        oClass.setOverrideNextDef(wsr_override_next_def);
                        oClass.setInnerLoopNextDef(wsr_innerloop_next_def);
                        oClass.setAnswerOtherFlag(wsr_other_flag);
                        oClass.setDenialFlag(wsr_denial_flag);
                        oClass.setDenialNextDef(wsr_denial_next_def);

                        // add class answer option to List
                        LCAnswerOption.add(oClass);
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        // publish progress
                        publishProgress("Downloading Answer Option [" + Integer.toString(i+1) + " of " + Integer.toString(ncount) + "]");

                    }

                    // save to db Question Set
                    dbHelper.AddBulkAnswerOption(LCAnswerOption);
                }
            }
            catch(NullPointerException e){
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadAnswerOptionTaskAsync  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "DownloadAnswerOptionTaskAsync  :: end doInBackground");

        return strPsurvey_tag;
    }
}