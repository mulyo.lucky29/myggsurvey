package com.gudanggaramtbk.myggsurvey.service;

public interface DownloadQuestionSetTaskAsyncResponse {
    void PostDownloadAction(String output);
}
