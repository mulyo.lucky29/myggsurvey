package com.gudanggaramtbk.myggsurvey.service;

public interface DownloadSurveySetTaskAsyncResponse {
    void PostDownloadAction(String output);
}
