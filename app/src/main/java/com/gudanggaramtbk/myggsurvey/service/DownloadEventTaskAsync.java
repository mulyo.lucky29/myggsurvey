package com.gudanggaramtbk.myggsurvey.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.activity.DownloadSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

public class DownloadEventTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private String strArea;
    private String strDeviceID;
    private List<CEvent> result;
    private DownloadSurveyActivity oPactivity;

    public DownloadEventTaskAsync(){ }
    public interface DownloadEventTaskAsyncResponse {
        void PostDownloadAction(String output);
    }
    public DownloadEventTaskAsync.DownloadEventTaskAsyncResponse delegate = null;
    public DownloadEventTaskAsync(DownloadEventTaskAsync.DownloadEventTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config, DownloadSurveyActivity pActivity, String StrAreaCode, String StrDeviceID){
        Log.d("[GudangGaram]", "DownloadEventTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.pd = new ProgressDialog(this.context);
        this.oPactivity  = pActivity;
        this.strArea     = StrAreaCode;
        this.strDeviceID = StrDeviceID;
        result = new ArrayList<CEvent>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "DownloadEventTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.dismiss();
        pd.setMessage("Retrieving Event Data");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        if(output!=null){
            Log.d("[GudangGaram]", "DownloadEventTaskAsync :: onPostExecute >> " + output);
            try{
                // --------- reload to download active event list
                oPactivity.EH_CMD_REFRESH(result);
                delegate.PostDownloadAction(output);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "DownloadEventTaskAsync :: onPostExecute Exception : " + e.getMessage().toString());
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Event";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(strDeviceID);
        prop_p_device_id.setType(String.class);

        // =============== p_area_code ===================
        PropertyInfo prop_p_area_code = new PropertyInfo();
        prop_p_area_code.setName("p_area_code");
        prop_p_area_code.setValue(strArea);
        prop_p_area_code.setType(String.class);

        Log.d("[GudangGaram]", "DownloadEventTaskAsync : doInBackground : p_area_code  :" + strArea);

        request.addProperty(prop_p_device_id);
        request.addProperty(prop_p_area_code);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                Log.d("[GudangGaram]", "DownloadEventTaskAsync :: doInBackground >>> count  >>> " + ncount.toString());
                if(ncount > 0){
                    // format PDU :
                    // 0. <EVENT_ID>SHVR01</EVENT_ID>
                    // 1. <EVENT_NAME>SHVR Ground Festival 2019</EVENT_NAME>
                    // 2. <EVENT_DESCRIPTION>Convention Exhibition - ICE, BSD</EVENT_DESCRIPTION>
                    // 3. <START_DATE>2019-09-06T00:00:00+07:00</START_DATE>
                    // 4. <END_DATE>2019-09-07T00:00:00+07:00</END_DATE>
                    // 5. <SURVEY_TAG>SVT2</SURVEY_TAG>
                    // 6. <ATTRIBUTE1 xml:space="preserve"> </ATTRIBUTE1>
                    // 7. <ATTRIBUTE2 xml:space="preserve"> </ATTRIBUTE2>
                    // 8. <ATTRIBUTE3 xml:space="preserve"> </ATTRIBUTE3>
                    // 9. <ATTRIBUTE4 xml:space="preserve"> </ATTRIBUTE4>
                    // 10. <ATTRIBUUTE5 xml:space="preserve"> </ATTRIBUUTE5>

                    result.clear();
                    for (int i = 0; i < ncount ;i++){
                        obj3 = (SoapObject) obj2.getProperty(i);
                        CEvent oClass = new CEvent();
                        oClass.setEventID(obj3.getProperty(0).toString());
                        oClass.setEventName(obj3.getProperty(1).toString());
                        oClass.setEventDescription(obj3.getProperty(2).toString());
                        oClass.setEventStartDate(obj3.getProperty(3).toString());
                        oClass.setEventEndDate(obj3.getProperty(4).toString());
                        oClass.setEventSurveyTag(obj3.getProperty(5).toString());
                        oClass.setAttribute1(obj3.getProperty(6).toString());
                        oClass.setAttribute2(obj3.getProperty(7).toString());
                        oClass.setAttribute3(obj3.getProperty(8).toString());
                        oClass.setAttribute4(obj3.getProperty(9).toString());
                        oClass.setAttribute5(obj3.getProperty(10).toString());
                        // add class asset to List result
                        result.add(oClass);

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        // publish progress
                        publishProgress("Downloading Event [" + Integer.toString(i+1) + " of " + Integer.toString(ncount) + "]");
                    }
                }
            }
            catch(NullPointerException e){
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadEventTaskAsync  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "DownloadEventTaskAsync  :: end doInBackground");

        return "";
    }
}