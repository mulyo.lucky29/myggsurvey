package com.gudanggaramtbk.myggsurvey.service;

public interface SendEventDownloadLogTaskAsyncResponse {
    void PostSentAction(String output);
}
