package com.gudanggaramtbk.myggsurvey.service;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.gudanggaramtbk.myggsurvey.activity.DownloadSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;

import java.util.ArrayList;
import java.util.List;

public class DownloadEvent {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private DownloadSurveyActivity pActivity;
    private List<CEvent> TResult;

    // override constructor
    public DownloadEvent(SharedPreferences PConfig,
                             Context ctx,
                             MySQLiteHelper dbHelper) {
        Log.d("[GudangGaram]", "DownloadEvent :: Constructor");
        this.context   = ctx;
        this.config    = PConfig;
        this.dbHelper  = dbHelper;
    }

    public void setParentActivity(DownloadSurveyActivity pActivity) {
        this.pActivity = pActivity;
    }

    public void Retrieve(final String strAreaCode, final String strDeviceID) {
        Log.d("[GudangGaram]", "DownloadEvent :: Retrieve > Begin");
        try {
            // call WS To Reload Active Event
            DownloadEventTaskAsync SoapRequest = new DownloadEventTaskAsync(new DownloadEventTaskAsync.DownloadEventTaskAsyncResponse() {
                @Override
                public void PostDownloadAction(String result) {

                }
            });
            SoapRequest.setAttribute(context, dbHelper, config, pActivity, strAreaCode, strDeviceID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
                //work on sgs3 android 4.0.4
                SoapRequest.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            } else {
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadEvent > Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
    }

}
