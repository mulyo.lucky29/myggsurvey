package com.gudanggaramtbk.myggsurvey.service;

public interface DownloadEventTaskAsyncResponse {
    void PostDownloadAction(String output);
}
