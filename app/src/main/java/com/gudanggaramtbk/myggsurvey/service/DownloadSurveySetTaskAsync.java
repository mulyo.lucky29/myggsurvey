package com.gudanggaramtbk.myggsurvey.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import com.gudanggaramtbk.myggsurvey.activity.DownloadSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.model.CSurveySet;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.ArrayList;
import java.util.List;

public class DownloadSurveySetTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private ProgressDialog pd;
    private MySQLiteHelper dbHelper;
    private String SentResponse;
    private List<CSurveySet> result;
    private String strPsurvey_tag;
    private String strDeviceID;

    public DownloadSurveySetTaskAsync(){ }
    public interface DownloadSurveySetTaskAsyncResponse {
        void PostDownloadAction(String output);
    }
    public DownloadSurveySetTaskAsync.DownloadSurveySetTaskAsyncResponse delegate = null;
    public DownloadSurveySetTaskAsync(DownloadSurveySetTaskAsync.DownloadSurveySetTaskAsyncResponse delegate){
        this.delegate = delegate;
    }

    public void setContext(Context context){
        this.context = context;
    }
    public void setAttribute(Context context, MySQLiteHelper dbHelper, SharedPreferences config, String pSurveyTag, String StrDeviceID){
        Log.d("[GudangGaram]", "DownloadSurveySetTaskAsync :: setAttribute");
        this.context           = context;
        this.dbHelper          = dbHelper;
        this.config            = config;
        this.pd = new ProgressDialog(this.context);
        this.strPsurvey_tag    = pSurveyTag;
        this.strDeviceID       = StrDeviceID;

        result = new ArrayList<CSurveySet>();
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "DownloadSurveySetTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.dismiss();
        pd.setMessage("Retrieving Survey Set Data");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected  void onProgressUpdate(String[] args) {
        pd.setMessage(args[0].toString());
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        if(output!=null){
            Log.d("[GudangGaram]", "DownloadSurveySetTaskAsync :: onPostExecute >> " + output);
            try{
                delegate.PostDownloadAction(output);
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "DownloadSurveySetTaskAsync :: onPostExecute Exception : " + e.getMessage().toString());
            }
        }
    }

    @Override
    protected String doInBackground(String... params) {
        Integer  ncount;

        String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS   = config.getString("SoapAddress", "");     //"http://10.50.131.18/WSAMScanner/WSAMScanner.asmx";
        String OPERATION_NAME = "Get_List_Survey_Set";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;            //"http://gudanggaramtbk.com/InsertToOracle";

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        // =============== p_survey_tag ===================
        PropertyInfo prop_p_survey_tag = new PropertyInfo();
        prop_p_survey_tag.setName("p_survey_tag");
        prop_p_survey_tag.setValue(strPsurvey_tag);
        prop_p_survey_tag.setType(String.class);

        // =============== p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(strDeviceID);
        prop_p_device_id.setType(String.class);

        request.addProperty(prop_p_device_id);
        request.addProperty(prop_p_survey_tag);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            try{
                httpTransport.debug = true;
                httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                httpTransport.call(SOAP_ACTION, envelope);
                SoapObject obj, obj1, obj2, obj3;
                obj     = (SoapObject) envelope.getResponse();
                obj1    = (SoapObject) obj.getProperty("diffgram");
                obj2    = (SoapObject) obj1.getProperty("NewDataSet");

                ncount = obj2.getPropertyCount();
                Log.d("[GudangGaram]", "DownloadSurveySetTaskAsync :: doInBackground >>> count  >>> " + ncount.toString());
                if(ncount > 0){
                    // format PDU :
                    // 0. <SURVEY_TAG>SVT2</SURVEY_TAG>
                    // 1. <SURVEY_DESCRIPTION>Questionaire Event v04</SURVEY_DESCRIPTION>
                    // 2. <ISACTIVE>Y</ISACTIVE>
                    // 3. <OPENING_NARATION xml:space="preserve"> </OPENING_NARATION>
                    // 4. <CLOSING_NARATION xml:space="preserve"> </CLOSING_NARATION>
                    // 5. <ATTRIBUTE1 xml:space="preserve"> </ATTRIBUTE1>
                    // 6. <ATTRIBUTE2 xml:space="preserve"> </ATTRIBUTE2>
                    // 7. <ATTRIBUTE3 xml:space="preserve"> </ATTRIBUTE3>
                    // 8. <ATTRIBUTE4 xml:space="preserve"> </ATTRIBUTE4>
                    // 9. <NOT_VALID_NARATION>Terima kasih atas waktu yang sudah diberikan Bapak/Mas/Kakak, kami sangat menghargainya</NOT_VALID_NARATION>

                    result.clear();
                    for (int i = 0; i < ncount ;i++){
                        obj3 = (SoapObject) obj2.getProperty(i);
                        CSurveySet oClass = new CSurveySet();
                        oClass.setSurveyTag(obj3.getProperty(0).toString());
                        oClass.setSurveyDescription(obj3.getProperty(1).toString());
                        oClass.setIsActive(obj3.getProperty(2).toString());
                        oClass.setOpeningNaration(obj3.getProperty(3).toString());
                        oClass.setClosingNaration(obj3.getProperty(4).toString());
                        oClass.setAttribute1(obj3.getProperty(5).toString());
                        oClass.setAttribute2(obj3.getProperty(6).toString());
                        oClass.setAttribute3(obj3.getProperty(7).toString());
                        oClass.setAttribute4(obj3.getProperty(8).toString());
                        oClass.setNotValidNaration(obj3.getProperty(9).toString());

                        // add class asset to List result
                        result.add(oClass);

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        // publish progress
                        publishProgress("Downloading Survey Set [" + Integer.toString(i+1) + " of " + Integer.toString(ncount) + "]");

                    }
                    // save to db Question Set
                    dbHelper.AddBulkSurveySet(result);
                }
            }
            catch(NullPointerException e){
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadSurveySetTaskAsync  :: Exception " + e.getMessage().toString());
        }
        Log.d("[GudangGaram]", "DownloadSurveySetTaskAsync  :: end doInBackground");

        return strPsurvey_tag;
    }
}
