package com.gudanggaramtbk.myggsurvey.service;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import com.gudanggaramtbk.myggsurvey.activity.MainActivity;
import com.gudanggaramtbk.myggsurvey.model.CSurveyD;
import com.gudanggaramtbk.myggsurvey.model.CSurveyH;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import java.util.List;

public class SendSurveyTaskAsync extends AsyncTask<String, String, String> {
    private SharedPreferences config;
    private Context context;
    private MainActivity pActivity;
    private ProgressDialog pd;
    private String ResultResponse;
    private MySQLiteHelper dbHelper;
    private CSurveyH och;
    private List<CSurveyH> listselected;
    private String SentResponse;
    private String deviceID;

    public SendSurveyTaskAsync() {
    }

    public interface SendSurveyTaskAsyncResponse {
        void PostSentAction(String output);
    }
    public SendSurveyTaskAsync.SendSurveyTaskAsyncResponse delegate = null;
    public SendSurveyTaskAsync(SendSurveyTaskAsync.SendSurveyTaskAsyncResponse delegate) {
        this.delegate = delegate;
    }
    public void setContext(Context context) {
        this.context = context;
    }
    public void setAttribute(Context context,
                             MainActivity pActivity,
                             MySQLiteHelper dbHelper,
                             SharedPreferences config,
                             String deviceID,
                             List<CSurveyH> olist) {

        Log.d("[GudangGaram]", "SendSurveyTaskAsync :: setAttribute");
        this.context      = context;
        this.pActivity    = pActivity;
        this.dbHelper     = dbHelper;
        this.config       = config;
        this.listselected = olist;
        this.deviceID     = deviceID;
        this.pd = new ProgressDialog(this.context);
    }

    @Override
    protected void onPreExecute() {
        Log.d("[GudangGaram]", "SendSurveyTaskAsync :: onPreExecute");
        super.onPreExecute();
        pd.setMessage("Send Survey Data To Oracle ... ");
        pd.setIndeterminate(false);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected void onProgressUpdate(String... args) {
        super.onProgressUpdate(args);
        pd.setMessage(args[0].toString());
        Log.d("[GudangGaram]", "SendSurveyTaskAsync :: onProgressUpdate");
    }

    @Override
    protected void onPostExecute(String output) {
        super.onPostExecute(output);
        pd.dismiss();
        Log.d("[GudangGaram]", "SendSurveyTaskAsync :: onPostExecute >> " + ResultResponse);
        try {
            // -------- refresh list main -----------------
            //pActivity.EH_cmd_Refresh();
            delegate.PostSentAction(ResultResponse);
        }
        catch (Exception e) {
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String strHeaderID;
        Integer detailResult = 0;
        Integer sendcurr  = 0;
        Integer sendcount = 0;
        try{
            sendcount = listselected.size();
            for (CSurveyH och : listselected) {
                /*
                strHeaderID  = SEND_HEADER_SURVEY(och, deviceID);
                detailResult = SEND_DETAIL_SURVEY(strSessionID, strHeaderID, deviceID);
                */

                strHeaderID  = SEND_HEADER_SURVEY(och, deviceID);

                sendcurr = sendcurr + 1;
                try {
                    Thread.sleep(20);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // publish progress
                publishProgress("Sending Survey Data " + sendcurr  + " of " + sendcount  + " Records");
            }
        }
        catch(Exception e){
        }
        return ResultResponse;
    }

    private String SEND_HEADER_SURVEY(CSurveyH och, String v_deviceID) {
        String strSessionID;
        String v_header_id = "-1";

        strSessionID = och.getSurveyHID();
        String NAMESPACE = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
        String SOAP_ADDRESS = config.getString("SoapAddress", "");     //"http://http://10.50.131.18/WSGGSurvey/ggsurvey.asmx";

        String OPERATION_NAME = "Insert_SurveyH";
        String SOAP_ACTION = NAMESPACE + OPERATION_NAME;

        SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
        Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        /*
        Parameters :
        1.  p_session_h:
        2.  p_survey_tag:
        3.  p_survey_desc:
        4.  p_event_id:
        5.  p_event_name:
        6.  p_interviewer_name:
        7.  p_event_attribute1:
        8.  p_event_attribute2:
        9.  p_event_attribute3:
        10. p_event_attribute4:
        11. p_device_id:
        12. p_status:
        13. p_start_date:
        14. p_end_date:
        15. p_cancel_date:
        16. p_cancel_reason
         */

        // ===============1. p_session_h ===================
        PropertyInfo prop_p_session_h = new PropertyInfo();
        prop_p_session_h.setName("p_session_h");
        prop_p_session_h.setValue(och.getSurveyHID());
        prop_p_session_h.setType(String.class);

        // ===============2. p_survey_tag ===================
        PropertyInfo prop_p_survey_tag = new PropertyInfo();
        prop_p_survey_tag.setName("p_survey_tag");
        prop_p_survey_tag.setValue(och.getSurvey_Tag());
        prop_p_survey_tag.setType(String.class);

        // ===============3. p_survey_desc ===================
        PropertyInfo prop_p_survey_desc = new PropertyInfo();
        prop_p_survey_desc.setName("p_survey_desc");
        prop_p_survey_desc.setValue(och.getSurvey_Desc());
        prop_p_survey_desc.setType(String.class);

        // ===============4. p_event_id ===================
        PropertyInfo prop_p_event_id = new PropertyInfo();
        prop_p_event_id.setName("p_event_id");
        prop_p_event_id.setValue(och.getEventID());
        prop_p_event_id.setType(String.class);

        // ===============5. p_event_name ===================
        PropertyInfo prop_p_event_name = new PropertyInfo();
        prop_p_event_name.setName("p_event_name");
        prop_p_event_name.setValue(och.getEventName());
        prop_p_event_name.setType(String.class);

        // ===============6. p_interviewer_name ===================
        PropertyInfo prop_p_interviewer_name = new PropertyInfo();
        prop_p_interviewer_name.setName("p_interviewer_name");
        prop_p_interviewer_name.setValue(och.getInterviewerName());
        prop_p_interviewer_name.setType(String.class);

        // ===============7. p_event_attribute1 ===================
        PropertyInfo prop_p_event_attribute1 = new PropertyInfo();
        prop_p_event_attribute1.setName("p_event_attribute1");
        prop_p_event_attribute1.setValue(och.getEventAttribute1());
        prop_p_event_attribute1.setType(String.class);

        // ===============8. p_event_attribute2 ===================
        PropertyInfo prop_p_event_attribute2 = new PropertyInfo();
        prop_p_event_attribute2.setName("p_event_attribute2");
        prop_p_event_attribute2.setValue(och.getEventAttribute2());
        prop_p_event_attribute2.setType(String.class);

        // ===============9. p_event_attribute3 ===================
        PropertyInfo prop_p_event_attribute3 = new PropertyInfo();
        prop_p_event_attribute3.setName("p_event_attribute3");
        prop_p_event_attribute3.setValue(och.getEventAttribute3());
        prop_p_event_attribute3.setType(String.class);

        // ===============10. p_event_attribut4 ===================
        PropertyInfo prop_p_event_attribute4 = new PropertyInfo();
        prop_p_event_attribute4.setName("p_event_attribute4");
        prop_p_event_attribute4.setValue(och.getEventAttribute4());
        prop_p_event_attribute4.setType(String.class);

        // ===============11. p_device_id ===================
        PropertyInfo prop_p_device_id = new PropertyInfo();
        prop_p_device_id.setName("p_device_id");
        prop_p_device_id.setValue(v_deviceID);
        prop_p_device_id.setType(String.class);

        // ===============12. p_status ===================
        PropertyInfo prop_p_status = new PropertyInfo();
        prop_p_status.setName("p_status");
        prop_p_status.setValue(och.getStatus());
        prop_p_status.setType(String.class);

        // ===============13. p_start_date ===================
        PropertyInfo prop_p_start_date = new PropertyInfo();
        prop_p_start_date.setName("p_start_date");
        prop_p_start_date.setValue(och.getStart_Date());
        prop_p_start_date.setType(String.class);

        // ===============14. p_end_date ===================
        PropertyInfo prop_p_end_date = new PropertyInfo();
        prop_p_end_date.setName("p_end_date");
        prop_p_end_date.setValue(och.getEnd_Date());
        prop_p_end_date.setType(String.class);

        // ===============15. p_cancel_date ===================
        PropertyInfo prop_p_cancel_date = new PropertyInfo();
        prop_p_cancel_date.setName("p_cancel_date");
        prop_p_cancel_date.setValue(och.getCancel_Date());
        prop_p_cancel_date.setType(String.class);

        // ===============16. p_cancel_reason ===================
        PropertyInfo prop_p_cancel_reason = new PropertyInfo();
        prop_p_cancel_reason.setName("p_cancel_reason");
        prop_p_cancel_reason.setValue(och.getCancel_Reason());
        prop_p_cancel_reason.setType(String.class);

        Log.d("[GudangGaram]", "-----------------(H)------------------------");
        Log.d("[GudangGaram]", "p_session_h         :" + och.getSurveyHID());
        Log.d("[GudangGaram]", "p_survey_tag        :" + och.getSurvey_Tag());
        Log.d("[GudangGaram]", "p_survey_desc       :" + och.getSurvey_Desc());
        Log.d("[GudangGaram]", "p_event_id          :" + och.getEventID());
        Log.d("[GudangGaram]", "p_event_name        :" + och.getEventName());
        Log.d("[GudangGaram]", "p_interviewer_name  :" + och.getInterviewerName());
        Log.d("[GudangGaram]", "p_event_attribute1  :" + och.getEventAttribute1());
        Log.d("[GudangGaram]", "p_event_attribute2  :" + och.getEventAttribute2());
        Log.d("[GudangGaram]", "p_event_attribute3  :" + och.getEventAttribute3());
        Log.d("[GudangGaram]", "p_event_attribute4  :" + och.getEventAttribute4());
        Log.d("[GudangGaram]", "p_device_id         :" + v_deviceID);
        Log.d("[GudangGaram]", "p_status            :" + och.getStatus());
        Log.d("[GudangGaram]", "p_start_date        :" + och.getStart_Date());
        Log.d("[GudangGaram]", "p_end_date          :" + och.getEnd_Date());
        Log.d("[GudangGaram]", "p_cancel_date       :" + och.getCancel_Date());
        Log.d("[GudangGaram]", "p_cancel_reason     :" + och.getCancel_Reason());
        Log.d("[GudangGaram]", "-----------------------------------------");

        request.addProperty(prop_p_session_h);
        request.addProperty(prop_p_survey_tag);
        request.addProperty(prop_p_survey_desc);
        request.addProperty(prop_p_event_id);
        request.addProperty(prop_p_event_name);
        request.addProperty(prop_p_interviewer_name);
        request.addProperty(prop_p_event_attribute1);
        request.addProperty(prop_p_event_attribute2);
        request.addProperty(prop_p_event_attribute3);
        request.addProperty(prop_p_event_attribute4);
        request.addProperty(prop_p_device_id);
        request.addProperty(prop_p_status);
        request.addProperty(prop_p_start_date);
        request.addProperty(prop_p_end_date);
        request.addProperty(prop_p_cancel_date);
        request.addProperty(prop_p_cancel_reason);

        //Web method call
        try {
            HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
            httpTransport.debug = true;
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            httpTransport.call(SOAP_ACTION, envelope);

            SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
            v_header_id = resultsRequestSOAP.getProperty(0).toString();

            // sumon ws send survey detail
            SEND_DETAIL_SURVEY(strSessionID, v_header_id, deviceID);

            ResultResponse = v_header_id;
            Log.d("[GudangGaram]", "SendSurveyTaskAsync :: SendSurveyHTaskAsync : ResultResponse : " + ResultResponse);

        } catch (Exception ex) {
            Log.d("[GudangGaram]", "SendSurveyTaskAsync :: SendSurveyHTaskAsync :: Catch Http Transport : " + ex.getMessage().toString());
            Toast.makeText(context, ex.getMessage().toString(), Toast.LENGTH_LONG).show();
        } finally {
            Log.d("[GudangGaram]", "SendSurveyTaskAsync :: SendSurveyHTaskAsync :: end doInBackground");
        }
        return v_header_id;
    }

    private Integer SEND_DETAIL_SURVEY(String strSession, String strHeaderID, String strDeviceID) {
        Integer result = 0;
        Integer sent;
        Integer seq_total;
        String v_detail_id = "-1";

        List<CSurveyD> lstloadSurveyD;
        lstloadSurveyD = dbHelper.loadSurveyD(strSession);
        seq_total = lstloadSurveyD.size();

        sent = 0;
        if (seq_total > 0) {
            for (CSurveyD osd : lstloadSurveyD) {
                String NAMESPACE      = config.getString("SoapNamespace", "");   //"http://gudanggaramtbk.com/";
                String SOAP_ADDRESS   = config.getString("SoapAddress", "");  //"http://http://10.50.131.18/WSGGSurvey/ggsurvey.asmx";
                String OPERATION_NAME = "Insert_SurveyD";
                String SOAP_ACTION = NAMESPACE + OPERATION_NAME;

                SoapObject request = new SoapObject(NAMESPACE, OPERATION_NAME);
                Log.d("[GudangGaram]", "SOAP REQUEST   :" + request.toString());

                SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
                envelope.dotNet = true;
                envelope.setOutputSoapObject(request);

                    /*
                    Parameters :
                   1.  p_session_h:
                   2.  p_header_id:
                   3.  p_seq_no:
                   4.  p_question_tag:
                   5.  p_question_value:
                   6.  p_answer_option_tag:
                   7.  p_answer_option_value:
                   8.  p_start_date:
                   9.  p_end_date:
                   10. p_cancel_date:
                   11. p_created_date:
                   12. p_device_id:
                   13. p_attribute1
                     */

                // ===============1. p_session_h ===================
                PropertyInfo prop_p_session_h = new PropertyInfo();
                prop_p_session_h.setName("p_session_h");
                prop_p_session_h.setValue(strSession);
                prop_p_session_h.setType(String.class);

                // ===============2. p_header_id ===================
                PropertyInfo prop_p_header_id = new PropertyInfo();
                prop_p_header_id.setName("p_header_id");
                prop_p_header_id.setValue(strHeaderID);
                prop_p_header_id.setType(Integer.class);

                // ===============3. p_seq_no ===================
                PropertyInfo prop_p_seq_no = new PropertyInfo();
                prop_p_seq_no.setName("p_seq_no");
                prop_p_seq_no.setValue(osd.getSeq_No());
                prop_p_seq_no.setType(Integer.class);

                // ===============4. p_question_tag ===================
                PropertyInfo prop_p_question_tag = new PropertyInfo();
                prop_p_question_tag.setName("p_question_tag");
                prop_p_question_tag.setValue(osd.getQuestion_Tag());
                prop_p_question_tag.setType(String.class);

                // ===============5. p_question_value ===================
                PropertyInfo prop_p_question_value = new PropertyInfo();
                prop_p_question_value.setName("p_question_value");
                prop_p_question_value.setValue(osd.getQuestion_Value());
                prop_p_question_value.setType(String.class);

                // ===============6. p_answer_option_tag ===================
                PropertyInfo prop_p_answer_option_tag = new PropertyInfo();
                prop_p_answer_option_tag.setName("p_answer_option_tag");
                prop_p_answer_option_tag.setValue(osd.getAnswer_Option_Tag());
                prop_p_answer_option_tag.setType(String.class);

                // ===============7. p_answer_option_value  ===================
                PropertyInfo prop_p_answer_option_value = new PropertyInfo();
                prop_p_answer_option_value.setName("p_answer_option_value");
                prop_p_answer_option_value.setValue(osd.getAnswer_Option_Value());
                prop_p_answer_option_value.setType(String.class);

                // ===============8. p_start_date  ===================
                PropertyInfo prop_p_start_date = new PropertyInfo();
                prop_p_start_date.setName("p_start_date");
                prop_p_start_date.setValue(osd.getStart_Date());
                prop_p_start_date.setType(Integer.class);

                // ===============9. p_end_date  ===================
                PropertyInfo prop_p_end_date = new PropertyInfo();
                prop_p_end_date.setName("p_end_date");
                prop_p_end_date.setValue(osd.getEnd_Date());
                prop_p_end_date.setType(Integer.class);

                // ===============10. p_cancel_date  ===================
                PropertyInfo prop_p_cancel_date = new PropertyInfo();
                prop_p_cancel_date.setName("p_cancel_date");
                prop_p_cancel_date.setValue(osd.getCancel_Date());
                prop_p_cancel_date.setType(String.class);

                // ===============11. p_created_date  ===================
                PropertyInfo prop_p_created_date = new PropertyInfo();
                prop_p_created_date.setName("p_created_date");
                prop_p_created_date.setValue(osd.getCreated_Date());
                prop_p_created_date.setType(String.class);

                // ===============12. p_device_id ===================
                PropertyInfo prop_p_device_id = new PropertyInfo();
                prop_p_device_id.setName("p_device_id");
                prop_p_device_id.setValue(strDeviceID);
                prop_p_device_id.setType(String.class);

                // ===============13. p_attribute1 ===================
                PropertyInfo prop_p_attribute1 = new PropertyInfo();
                prop_p_attribute1.setName("p_attribute1");
                prop_p_attribute1.setValue(osd.getAttribute1());
                prop_p_attribute1.setType(String.class);


                request.addProperty(prop_p_session_h);
                request.addProperty(prop_p_header_id);
                request.addProperty(prop_p_seq_no);
                request.addProperty(prop_p_question_tag);
                request.addProperty(prop_p_question_value);
                request.addProperty(prop_p_answer_option_tag);
                request.addProperty(prop_p_answer_option_value);
                request.addProperty(prop_p_start_date);
                request.addProperty(prop_p_end_date);
                request.addProperty(prop_p_cancel_date);
                request.addProperty(prop_p_created_date);
                request.addProperty(prop_p_device_id);
                request.addProperty(prop_p_attribute1);

                Log.d("[GudangGaram]", "-----------------(D)------------------------");
                Log.d("[GudangGaram]", "p_session_h            :" + strSession);
                Log.d("[GudangGaram]", "p_header_id            :" + strHeaderID);
                Log.d("[GudangGaram]", "p_seq_no               :" + osd.getSeq_No());
                Log.d("[GudangGaram]", "p_question_tag         :" + osd.getQuestion_Tag());
                Log.d("[GudangGaram]", "p_question_value       :" + osd.getQuestion_Value());
                Log.d("[GudangGaram]", "p_answer_option_tag    :" + osd.getAnswer_Option_Tag());
                Log.d("[GudangGaram]", "p_answer_option_value  :" + osd.getAnswer_Option_Value());
                Log.d("[GudangGaram]", "p_start_date           :" + osd.getStart_Date());
                Log.d("[GudangGaram]", "p_end_date             :" + osd.getEnd_Date());
                Log.d("[GudangGaram]", "p_cancel_date          :" + osd.getCreated_Date());
                Log.d("[GudangGaram]", "p_created_date         :" + osd.getCreated_Date());
                Log.d("[GudangGaram]", "p_device_id            :" + strDeviceID);
                Log.d("[GudangGaram]", "p_attribute1           :" + osd.getAttribute1());
                Log.d("[GudangGaram]", "-----------------------------------------");

                //Web method call
                try {
                    HttpTransportSE httpTransport = new HttpTransportSE(SOAP_ADDRESS);
                    httpTransport.debug = true;
                    httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                    httpTransport.call(SOAP_ACTION, envelope);

                    SoapObject resultsRequestSOAP = (SoapObject) envelope.bodyIn;
                    v_detail_id = resultsRequestSOAP.getProperty(0).toString();

                    Log.d("[GudangGaram]", "Sending Detail Survey(" + strHeaderID + ") > " + v_detail_id);
                    try {
                        Thread.sleep(20);
                    }
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    try {
                        if (Integer.parseInt(v_detail_id) > 0) {
                            sent = sent + 1;
                        }
                    }
                    catch (Exception e) {
                    }
                }
                catch (Exception ex) {
                    Log.d("[GudangGaram]", "SendSurveyTaskAsync Http Transport Exception : " + ex.getMessage().toString());
                }
            } // end for
        } // end if

        if(sent == seq_total){
            result = 1;
            // --------- delete database record --------------
            dbHelper.DeleteSurveyD(strSession);
            try {
                Thread.sleep(20);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            dbHelper.DeleteSurveyH(strSession);
        }

        return result;
        }
    }

