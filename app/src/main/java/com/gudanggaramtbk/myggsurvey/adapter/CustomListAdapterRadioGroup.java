package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.model.CMTag;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderRadioGroup;
import java.util.ArrayList;
import java.util.List;

public class CustomListAdapterRadioGroup extends RecyclerView.Adapter<ViewHolderRadioGroup> {
    private Context mcontext;
    private LayoutInflater mInflater;
    private List<CAnswerOption> list_temp;
    private List<CAnswerOption> list_pilihan;
    private MySQLiteHelper odbhelper;
    private String strEventID;
    private CMTag ctag;

    public CustomListAdapterRadioGroup(Context ctx, MySQLiteHelper dbhelper, View vw, List<CAnswerOption> lst, String eventID) {
        Log.d("[GudangGaram]", "CustomListAdapterRadioGroup");
        list_temp         = lst;
        list_pilihan      = Construct_ListRadioGroup(lst);
        mcontext          = ctx;
        odbhelper         = dbhelper;
        strEventID        = eventID;
    }

    public List<CAnswerOption> getList_pilihan() {
        return list_pilihan;
    }

    private List<CAnswerOption> Construct_ListRadioGroup(List<CAnswerOption> lst){
        String RGroupTag = "R" + lst.get(0).getQuestionTag().toString();
        List<CAnswerOption> result = new ArrayList<CAnswerOption>();
        result.add(new CAnswerOption(RGroupTag,"","",""));
        return result;
    }

    @Override
    public ViewHolderRadioGroup onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("[GudangGaram]", "CustomListAdapterRadioGroup :: onCreateViewHolder");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rwlist_radiogroup, parent, false);
        ViewHolderRadioGroup viewHolder = new ViewHolderRadioGroup(view);
        viewHolder.setContext(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolderRadioGroup holder, int position) {
        String replaceValue;
        String answerCat = "N";

        Log.d("[GudangGaram]", "CustomListAdapterRadioGroup :: onBindViewHolder");
        holder.answerSet.setOrientation(LinearLayout.VERTICAL);

        for (CAnswerOption pilihan_jawaban : list_temp) {
            // ------------- define radio button  -------------
            final RadioButton rdbtn = new RadioButton(mcontext);
            ColorStateList myColorStateList = new ColorStateList(
                    new int[][]{
                            new int[]{mcontext.getResources().getColor(R.color.colorPrimaryDark)}
                    },
                    new int[]{mcontext.getResources().getColor(R.color.colorAccent)}
            );
            replaceValue = odbhelper.getReplacedSentenceOverVariable(strEventID, pilihan_jawaban.getAnswerOptionValue().toString());
            rdbtn.setBackgroundTintList(myColorStateList);
            rdbtn.setButtonTintList(myColorStateList);
            rdbtn.setPadding(0,0,0,20);
            rdbtn.setGravity(Gravity.CENTER_VERTICAL);
            rdbtn.setText(replaceValue);
            // encap tag to have tag name and value that will useful to extract next

            ctag  = new CMTag();
            ctag.setStrTag(pilihan_jawaban.getAnswerOptionTag());
            ctag.setStrVal(replaceValue);
            ctag.setIsOther(pilihan_jawaban.getAnswerOtherFlag());
            rdbtn.setTag(ctag);
            rdbtn.setTextSize(24);

            // ------------- add radio button to radio group -------------
            holder.answerSet.addView(rdbtn);
            holder.answerText.setVisibility(View.GONE);
            holder.answerText.addTextChangedListener(new TextWatcher() {
                // the user's changes are saved here
                public void onTextChanged(CharSequence c, int start, int before, int count) {
                    try{
                        ctag.setOtherVal(holder.answerText.getText().toString());
                    }
                    catch(Exception e){
                    }
                }
                public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                    // this space intentionally left blank
                }
                public void afterTextChanged(Editable c) {
                    // this one too
                }
            });

            holder.answerSet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int radioButtonID = group.getCheckedRadioButtonId();
                    View radioButton = group.findViewById(radioButtonID);

                    //int idx = radioGroup.indexOfChild(radioButton);
                    CMTag octag = (CMTag) radioButton.getTag();
                    holder.answerText.setText("");
                    if(octag.getIsOther().trim().equals("Y")){
                        holder.answerText.setVisibility(View.VISIBLE);
                        holder.answerText.setEnabled(true);
                        holder.answerText.requestFocus();
                    }
                    else{
                        holder.answerText.setVisibility(View.GONE);
                        holder.answerText.setEnabled(false);
                    }
                    Log.d("[GudangGaram]", "CustomListAdapterRadioGroup Selected > rg_tag       : " + octag.getStrTag());
                }
            });
        }
    }


    @Override
    public int getItemCount() {
        return list_pilihan.size();
    }
}
