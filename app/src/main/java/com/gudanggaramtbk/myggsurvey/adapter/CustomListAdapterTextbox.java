package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderTextbox;
import java.util.List;

public class CustomListAdapterTextbox extends RecyclerView.Adapter<ViewHolderTextbox> {
    private Context             mcontext;
    private LayoutInflater      mInflater;
    private List<CAnswerOption> list_pilihan;
    private String              validationType;
    private MySQLiteHelper odbhelper;
    private String strEventID;

    public CustomListAdapterTextbox(Context ctx, MySQLiteHelper dbhelper, View vw, List<CAnswerOption> lst, String eventID) {
        Log.d("[GudangGaram]", "CustomListAdapterTextbox");
        list_pilihan = lst;
        mcontext     = ctx;
        odbhelper    = dbhelper;
        strEventID        = eventID;
    }

    public List<CAnswerOption> getList_pilihan() {
        return list_pilihan;
    }

    @Override
    public ViewHolderTextbox onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolderTextbox viewHolder;

        Log.d("[GudangGaram]", "CustomListAdapterTextbox :: onCreateViewHolder");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rwlist_textbox, parent, false);

        try{
            validationType = list_pilihan.get(0).getTextBoxValidationType().trim();
            if(!validationType.isEmpty()){
                viewHolder = new ViewHolderTextbox(view,validationType);
            }
            else{
                viewHolder = new ViewHolderTextbox(view);
            }
        }
        catch(Exception e){
            viewHolder = new ViewHolderTextbox(view);
        }
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(ViewHolderTextbox holder, int position) {
        Log.d("[GudangGaram]", "CustomListAdapterTextbox :: onBindViewHolder");
        String replaceValue;
        String key, value;
        CAnswerOption pilihanjawabanModel = list_pilihan.get(position);
        replaceValue = odbhelper.getReplacedSentenceOverVariable(strEventID, list_pilihan.get(0).getAnswerOptionValue());
        holder.answerSet.setText(replaceValue);
        holder.answerSet.setTag(list_pilihan.get(0).getAnswerOptionTag());
    }
    @Override
    public int getItemCount() {
        //Log.d("[GudangGaram]", "CustomListAdapterTextbox :: getItemCount");
        return list_pilihan.size();
    }
}
