package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.model.CMTag;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderCheckBoxGroup;

import java.io.File;
import java.util.List;

public class CustomListAdapterCheckBoxGroup extends RecyclerView.Adapter<ViewHolderCheckBoxGroup> {
    private Context mcontext;
    private LayoutInflater mInflater;
    private List<CAnswerOption> list_pilihan;
    private Bitmap BlankImage;
    MySQLiteHelper odbHelper;
    private String strEventID;
    private CMTag ctag;
    private String[] toppings;
    ViewHolderCheckBoxGroup viewHolder;
    View view;

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }


    public CustomListAdapterCheckBoxGroup(Context ctx, MySQLiteHelper dbhelper, View vw, List<CAnswerOption> lst,String eventID) {
        Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup");
        list_pilihan = lst;
        mcontext    = ctx;
        odbHelper   = dbhelper;
        strEventID  = eventID;
        BlankImage  = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.no_image);
    }

    public List<CAnswerOption> getList_pilihan() {
        return list_pilihan;
    }

    // ok
    @Override
    public ViewHolderCheckBoxGroup onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: onCreateViewHolder");
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rwlist_checkbox, parent, false);
        viewHolder = new ViewHolderCheckBoxGroup(view);
        return viewHolder;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final ViewHolderCheckBoxGroup holder, final int position) {
            String replaceValue;
            String answerClass = "";
            String answerCat   = "N";
            Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: onBindViewHolder");

            final CAnswerOption pilihan_jawaban = list_pilihan.get(position);
            holder.setIsRecyclable(false);
            holder.answerText.setEnabled(false);
            holder.answerClass.setText(answerClass);
            if(answerClass.toString().trim().equals("")){
                holder.answerClass.setVisibility(View.GONE);
            }
            else{
                holder.answerClass.setVisibility(View.VISIBLE);
            }

            // -------- render image if there at least one image assigned in answer option, if not assigned then dont use image mode ---------------
            try{
                if(odbHelper.havingImage(pilihan_jawaban.getQuestionTag())) {
                    // ------ use image mode option ----------
                    holder.answerImage.setVisibility(View.VISIBLE);
                    load_imageview(holder.answerImage, pilihan_jawaban.getImageName());
                }
                else {
                    // ------ not use image mode ----------
                    holder.answerImage.setVisibility(View.GONE);
                }
            } catch(Exception e){
            }
            holder.answerSelect.setPadding(0,0,0,20);
            replaceValue = odbHelper.getReplacedSentenceOverVariable(strEventID, pilihan_jawaban.getAnswerOptionValue().toString());
            holder.answerSelect.setText(replaceValue);
            holder.answerSelect.setTextSize(24);
            //holder.answerSelect.setTag(pilihan_jawaban.getAnswerOptionTag().toString());

            // encap tag to have tag name and value that will useful to extract next
            ctag = new CMTag();
            ctag.setStrTag(pilihan_jawaban.getAnswerOptionTag());
            ctag.setStrVal(replaceValue);
            ctag.setIsOther(pilihan_jawaban.getAnswerOtherFlag());
            ctag.setDenialFlag(pilihan_jawaban.getDenialFlag());
            ctag.setDenialNextVal(pilihan_jawaban.getDenialNextDef());

            holder.answerSelect.setTag(ctag);

            Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: ---------------- > " + holder.answerSelect.getText());
            Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: other flag ----- > " + pilihan_jawaban.getAnswerOtherFlag());

            holder.answerSelect.setOnCheckedChangeListener(null);
            holder.answerSelect.setChecked(pilihan_jawaban.isSelected());

            // render pilihan jawaban
            // if the checkbox is for other option
            if (pilihan_jawaban.getAnswerOtherFlag().equals("Y")) {
                // if checkbox is selected
                if((pilihan_jawaban.isSelected() == true)) {
                    // get visibility for answer text other
                    holder.answerText.setVisibility(View.VISIBLE);
                    holder.answerText.setEnabled(true);
                    holder.answerText.setText(pilihan_jawaban.getOtherValue());
                    holder.answerText.requestFocus();
                }
                else {
                    // hide for answer text other
                    holder.answerText.setVisibility(View.GONE);
                    holder.answerText.setEnabled(false);
                    holder.answerText.setText("");
                }
            }
            else{
                // if not for other then hide for answer text other
                holder.answerText.setVisibility(View.GONE);
                holder.answerText.setEnabled(false);
                holder.answerText.setText("");
            }

            holder.answerSelect.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try{
                        pilihan_jawaban.setSelected(isChecked);

                        if (pilihan_jawaban.getAnswerOtherFlag().equals("Y")) {
                            // clear input first
                            holder.answerText.setText("");
                            // clear value in tag first
                            ctag.setOtherVal("");
                            // check if checked
                            if(isChecked){
                                holder.answerText.setVisibility(View.VISIBLE);
                                holder.answerText.setEnabled(true);
                                holder.answerText.requestFocus();
                            }
                            else {
                                holder.answerText.setVisibility(View.GONE);
                                holder.answerText.setEnabled(false);
                            }
                        }

                        // if the denial value selected then undo all option selected
                        if(pilihan_jawaban.getDenialFlag().equals("Y")){
                            if(isChecked) {
                                Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: holder.answerSelect.setOnCheckedChangeListener denial checked");
                                    // clear_all_checked_except_the_denial
                                    for(int x= 0; x< list_pilihan.size(); x++){
                                        if(!(list_pilihan.get(x).getDenialFlag().trim().equals("Y"))){
                                            list_pilihan.get(x).setSelected(false);
                                        }
                                    }
                                    // refresh front end using updated data adapter
                                    notifyDataSetChanged();
                            }
                        }
                        else{
                            // if others selected then clear_the denial
                            for(int x= 0; x< list_pilihan.size(); x++){
                                if((list_pilihan.get(x).getDenialFlag().trim().equals("Y"))){
                                    list_pilihan.get(x).setSelected(false);
                                }
                            }
                            // refresh front end using updated data adapter
                            notifyDataSetChanged();
                        }
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: holder.answerSelect.setOnCheckedChangeListener Exception : " + e.getMessage());
                    }
                }
            });

            holder.answerText.addTextChangedListener(new TextWatcher() {
                // the user's changes are saved here
                public void onTextChanged(CharSequence c, int start, int before, int count) {
                    try{
                        ctag.setOtherVal(holder.answerText.getText().toString());
                        pilihan_jawaban.setOtherValue(ctag.getOtherVal());
                    }
                    catch(Exception e){
                    }
                }
                public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                    // this space intentionally left blank
                }
                public void afterTextChanged(Editable c) {
                    // this one too
                }
            });
    }

    @Override
    public int getItemCount() {
        return list_pilihan.size();
    }

    private void load_imageview(ImageView imgv, String strImage){
        String imagename = odbHelper.getImage(strImage);
        String dir = Environment.getExternalStorageDirectory()+File.separator+"ggsurvey";
        File folder = new File(dir);
        File folderpath = new File(folder+ File.separator+imagename);
        try{
            if(folderpath.exists())
            {
                Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: load_imageview : Image Name :" + imagename + " Exists");
                String folderpath1 = folderpath.getAbsolutePath().toString().trim();
                imgv.setVisibility(View.VISIBLE);
                imgv.setImageBitmap(BitmapFactory.decodeFile(folderpath1));
            }
            else{
                Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: load_imageview : Image Name :" + imagename + " Not Exists");
                imgv.setVisibility(View.GONE);
                imgv.setImageBitmap(BlankImage);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "CustomListAdapterCheckBoxGroup :: load_imageview : Image Name Exception :" + e.getMessage().toString());
        }
    }

}