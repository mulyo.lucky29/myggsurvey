package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.model.CMainSummary;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderMainSummary;

import java.util.List;

public class CustomListAdapterMainSummary extends ArrayAdapter<CMainSummary> {
    private Context mcontext;
    private List<CMainSummary> originalData = null;
    private List<CMainSummary> filteredData = null;
    private LayoutInflater mInflater;
    private ListView olistsummary;
    private MySQLiteHelper dbHelper;

    public CustomListAdapterMainSummary(Context         context,
                                 List<CMainSummary>  list,
                                 MySQLiteHelper  dbHelper,
                                 ListView        listsummary)
    {
        super(context,0,list);
        this.mcontext = context;
        this.dbHelper = dbHelper;
        mInflater     = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.filteredData  = list;
        this.originalData  = list;
        this.olistsummary  = listsummary;

    }

    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }
    public CMainSummary getItemAtPosition(int position){
        return filteredData.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderMainSummary holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_main_summary, parent, false);
            holder = new ViewHolderMainSummary();
            holder.Txt_mns_eventid    = (TextView)    convertView.findViewById(R.id.txt_mns_eventid);
            holder.Txt_mns_eventname  = (TextView)    convertView.findViewById(R.id.txt_mns_eventname);
            holder.Txt_mns_event_date = (TextView)    convertView.findViewById(R.id.txt_mms_event_date);
            holder.Txt_mns_rec_val1   = (TextView)    convertView.findViewById(R.id.txt_mns_rec_val1);
            holder.Txt_mns_rec_val2   = (TextView)    convertView.findViewById(R.id.txt_mns_rec_val2);
            holder.Txt_mns_rec_val3   = (TextView)    convertView.findViewById(R.id.txt_mns_rec_val3);
            holder.Txt_mns_rec_val4   = (TextView)    convertView.findViewById(R.id.txt_mns_rec_val4);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderMainSummary) convertView.getTag();
        }

        try{
            holder.Txt_mns_eventid.setText(filteredData.get(position).getEventID());
            holder.Txt_mns_eventname.setText(filteredData.get(position).getEventName());
            holder.Txt_mns_event_date.setText(filteredData.get(position).getDateExec());
            holder.Txt_mns_rec_val1.setText(filteredData.get(position).getRecValid().toString());
            holder.Txt_mns_rec_val2.setText(filteredData.get(position).getRecInvalid().toString());
            holder.Txt_mns_rec_val3.setText(filteredData.get(position).getRecCancelled().toString());
            holder.Txt_mns_rec_val4.setText(filteredData.get(position).getRecTotal().toString());
        }
        catch(Exception e){
        }

        return convertView;
    }
}