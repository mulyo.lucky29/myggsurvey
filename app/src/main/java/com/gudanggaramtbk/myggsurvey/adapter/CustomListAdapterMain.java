package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.model.CSurveyH;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderMain;

import java.util.List;

public class CustomListAdapterMain extends ArrayAdapter<CSurveyH> {
    private Context        mcontext;
    private List<CSurveyH> originalData = null;
    private List<CSurveyH> filteredData = null;
    private LayoutInflater mInflater;
    private ListView       olistasset;
    private MySQLiteHelper dbHelper;

    public CustomListAdapterMain(Context         context,
                                 List<CSurveyH>  list,
                                 MySQLiteHelper  dbHelper,
                                 ListView        listasset)
    {
        super(context,0,list);
        this.mcontext = context;
        this.dbHelper = dbHelper;
        //this.cParent  = oParent;
        mInflater     = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.filteredData  = list;
        this.originalData  = list;
        this.olistasset    = listasset;

        // initialized list data to be uncheck
        for (int i = 0; i < list.size(); i++) {
            filteredData.get(i).setChecked(false);
            olistasset.setItemChecked(i, false);
        }
    }

    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }
    public CSurveyH getItemAtPosition(int position){
        return filteredData.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderMain holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_main, parent, false);
            holder = new ViewHolderMain();
            holder.Chk                     = (CheckBox)    convertView.findViewById(R.id.chk);
            holder.Txt_mn_eventid          = (TextView)    convertView.findViewById(R.id.txt_mn_eventid);
            holder.txt_mn_surveytag        = (TextView)    convertView.findViewById(R.id.txt_mn_surveytag);
            holder.Txt_mn_session_id       = (TextView)    convertView.findViewById(R.id.txt_mn_session_id);
            holder.Txt_mn_event_name       = (TextView)    convertView.findViewById(R.id.txt_mn_event_name);
            holder.Txt_mn_survey_startdate = (TextView)    convertView.findViewById(R.id.txt_mn_survey_startdate);
            holder.Txt_mn_survey_enddate   = (TextView)    convertView.findViewById(R.id.txt_mn_survey_enddate);
            holder.Txt_mn_survey_status    = (TextView)    convertView.findViewById(R.id.txt_mn_survey_status);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderMain) convertView.getTag();
            holder.Chk.setOnCheckedChangeListener(null);
        }

        try{
            holder.Chk.setChecked(filteredData.get(position).isChecked());
            holder.Txt_mn_eventid.setText(filteredData.get(position).getEventID());
            holder.txt_mn_surveytag.setText(filteredData.get(position).getSurvey_Tag());
            holder.Txt_mn_session_id.setText(filteredData.get(position).getSurveyHID());
            holder.Txt_mn_event_name.setText(filteredData.get(position).getEventName());
            holder.Txt_mn_survey_startdate.setText(filteredData.get(position).getStart_Date());
            holder.Txt_mn_survey_enddate.setText(filteredData.get(position).getEnd_Date());
            holder.Txt_mn_survey_status.setText(filteredData.get(position).getStatus());

            holder.Chk.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    EH_CMD_CHECK(v,holder, position);
                }
            });
        }
        catch(Exception e){
        }

        return convertView;
    }

    public void EH_CMD_SELECT_ALL(){
        Integer idx = 0;
        Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_SELECT_ALL  Begin");
        try{
            for(idx=0; idx<filteredData.size(); idx++){
                filteredData.get(idx).setChecked(true);
                olistasset.setItemChecked(idx, true);
            }
        }
        catch(Exception e){
        }
        Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_SELECT_ALL  End");
    }

    public void EH_CMD_CLEAR_ALL(){
        Integer idx = 0;
        Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CLEAR_ALL");
        try{
            for(idx=0; idx<filteredData.size(); idx++){
                filteredData.get(idx).setChecked(false);
                olistasset.setItemChecked(idx, false);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CLEAR_ALL Exception : " + e.getMessage().toString());
        }
    }

    public void EH_CMD_CHECK(View v, ViewHolderMain hd, Integer position){
        Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CHECK");
        Boolean selection;
        CheckBox cb = (CheckBox) v ;
        selection = ((CheckBox) v).isChecked();

        try{
            if(selection == true){
                Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CHECK :: " +  position + " Checked");
                filteredData.get(position).setChecked(true);
                olistasset.setItemChecked(position, true);
            }
            else{
                Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CHECK :: " + position + " UnChecked");
                filteredData.get(position).setChecked(false);
                olistasset.setItemChecked(position, false);
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]:", "CustomListAdapterMain :: EH_CMD_CHECK Exception " + e.getMessage().toString());
        }
    }
}