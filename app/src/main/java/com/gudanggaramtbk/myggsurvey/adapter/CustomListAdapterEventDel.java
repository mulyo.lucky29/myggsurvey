package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.activity.DownloadSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderEventDel;
import java.util.List;

public class CustomListAdapterEventDel extends ArrayAdapter<CEvent> {
    private Context mcontext;
    private List<CEvent> filteredData = null;
    private LayoutInflater mInflater;
    private MySQLiteHelper dbHelper;
    private DownloadSurveyActivity PActivity;

    public CustomListAdapterEventDel(Context         context,
                                     MySQLiteHelper  dbHelper,
                                     List<CEvent>     list) {
        super(context,0,list);
        this.mcontext = context;
        this.dbHelper = dbHelper;
        mInflater     = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.filteredData  = list;
    }

    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }
    public void setParentAtivity(DownloadSurveyActivity oParent){
        Log.d("[GudangGaram]", "CustomFilterListAdapterEventDel :: SetParentActivity");
        this.PActivity = oParent;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderEventDel holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_eventdel, parent, false);
            holder = new ViewHolderEventDel();
            holder.Txt_evtdel_eventid         = (TextView) convertView.findViewById(R.id.txt_evtdel_eventid);
            holder.Txt_evtdel_eventname       = (TextView) convertView.findViewById(R.id.txt_evtdel_eventname);
            holder.Txt_evtdel_eventdesc       = (TextView) convertView.findViewById(R.id.txt_evtdel_eventdesc);
            holder.Txt_evtdel_event_startdate = (TextView) convertView.findViewById(R.id.txt_evtdel_event_startdate);
            holder.Txt_evtdel_event_enddate   = (TextView) convertView.findViewById(R.id.txt_evtdel_event_enddate);
            holder.Txt_evtdel_surveytag       = (TextView) convertView.findViewById(R.id.txt_evtdel_surveytag);
            holder.Txt_evtdel_attribute1      = (TextView) convertView.findViewById(R.id.txt_evtdel_attribute1);
            holder.Txt_evtdel_attribute2      = (TextView) convertView.findViewById(R.id.txt_evtdel_attribute2);
            holder.Txt_evtdel_attribute3      = (TextView) convertView.findViewById(R.id.txt_evtdel_attribute3);
            holder.Txt_evtdel_attribute4      = (TextView) convertView.findViewById(R.id.txt_evtdel_attribute4);
            holder.Cmd_evtdel_delete          = (ImageButton) convertView.findViewById(R.id.cmd_evtdel_delete);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderEventDel) convertView.getTag();
        }

        holder.Cmd_evtdel_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", "CustomListAdapterEventDel :: Cmd_evt_delete OnClick");
                try{
                    // call parent to go to delete event id
                    PActivity.EH_CMD_DELETE_EVENT_SAVED(holder.Txt_evtdel_eventid.getText().toString());
                }
                catch(Exception e){
                }
            }
        });

        try{
            holder.Txt_evtdel_eventid.setText(filteredData.get(position).getEventID());
            holder.Txt_evtdel_eventname.setText(filteredData.get(position).getEventName());
            holder.Txt_evtdel_eventdesc.setText(filteredData.get(position).getEventDescription());
            holder.Txt_evtdel_event_startdate.setText(filteredData.get(position).getEventStartDate().replace("T00:00:00+07:00",""));
            holder.Txt_evtdel_event_enddate.setText(filteredData.get(position).getEventEndDate().replace("T00:00:00+07:00",""));
            holder.Txt_evtdel_surveytag.setText(filteredData.get(position).getEventSurveyTag());
            holder.Txt_evtdel_attribute1.setText(filteredData.get(position).getAttribute1());
            holder.Txt_evtdel_attribute2.setText(filteredData.get(position).getAttribute2());
            holder.Txt_evtdel_attribute3.setText(filteredData.get(position).getAttribute3());
            holder.Txt_evtdel_attribute4.setText(filteredData.get(position).getAttribute4());
        }
        catch(Exception e){
        }

        return convertView;
    }

}
