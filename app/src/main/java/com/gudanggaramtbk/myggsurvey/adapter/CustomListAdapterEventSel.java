package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.activity.GoToTakeSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderEventSel;
import java.util.List;

public class CustomListAdapterEventSel extends ArrayAdapter<CEvent> {
    private Context mcontext;
    private List<CEvent> filteredData = null;
    private LayoutInflater mInflater;
    private MySQLiteHelper dbHelper;
    private GoToTakeSurveyActivity PActivity;

    public CustomListAdapterEventSel(Context         context,
                                  MySQLiteHelper  dbHelper,
                                  List<CEvent>     list) {
        super(context,0,list);
        this.mcontext = context;
        this.dbHelper = dbHelper;
        mInflater     = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.filteredData  = list;
    }

    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }

    public void setParentAtivity(GoToTakeSurveyActivity oParent){
        Log.d("[GudangGaram]", "CustomFilterListAdapterEventSel :: SetParentActivity");
        this.PActivity = oParent;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolderEventSel holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_eventsel, parent, false);
            holder = new ViewHolderEventSel();
            holder.Txt_evtsel_eventid         = (TextView) convertView.findViewById(R.id.txt_evtsel_eventid);
            holder.Txt_evtsel_eventname       = (TextView) convertView.findViewById(R.id.txt_evtsel_eventname);
            holder.Txt_evtsel_eventdesc       = (TextView) convertView.findViewById(R.id.txt_evtsel_eventdesc);
            holder.Txt_evtsel_event_startdate = (TextView) convertView.findViewById(R.id.txt_evtsel_event_startdate);
            holder.Txt_evtsel_event_enddate   = (TextView) convertView.findViewById(R.id.txt_evtsel_event_enddate);
            holder.Txt_evtsel_surveytag       = (TextView) convertView.findViewById(R.id.txt_evtsel_surveytag);
            holder.Txt_evtsel_attribute1      = (TextView) convertView.findViewById(R.id.txt_evtsel_attribute1);
            holder.Txt_evtsel_attribute2      = (TextView) convertView.findViewById(R.id.txt_evtsel_attribute2);
            holder.Txt_evtsel_attribute3      = (TextView) convertView.findViewById(R.id.txt_evtsel_attribute3);
            holder.Txt_evtsel_attribute4      = (TextView) convertView.findViewById(R.id.txt_evtsel_attribute4);
            holder.Cmd_evtsel_goTo            = (ImageButton) convertView.findViewById(R.id.cmd_evtsel_goto);


            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderEventSel) convertView.getTag();
        }

        holder.Cmd_evtsel_goTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", "CustomListAdapterEventSel :: Cmd_evt_download OnClick");
                try{
                    // call parent to go to takesurvey activity and load survey tag selected
                    CEvent oCParam = new CEvent();
                    oCParam.setEventSurveyTag(holder.Txt_evtsel_surveytag.getText().toString());
                    oCParam.setEventID(holder.Txt_evtsel_eventid.getText().toString());
                    oCParam.setEventName(holder.Txt_evtsel_eventname.getText().toString());
                    oCParam.setEventDescription(holder.Txt_evtsel_eventdesc.getText().toString());
                    oCParam.setEventStartDate(holder.Txt_evtsel_event_startdate.getText().toString());
                    oCParam.setEventEndDate(holder.Txt_evtsel_event_enddate.getText().toString());
                    oCParam.setAttribute1(holder.Txt_evtsel_attribute1.getText().toString());
                    oCParam.setAttribute2(holder.Txt_evtsel_attribute2.getText().toString());
                    oCParam.setAttribute3(holder.Txt_evtsel_attribute3.getText().toString());
                    oCParam.setAttribute4(holder.Txt_evtsel_attribute4.getText().toString());

                    PActivity.EH_CMD_GO_TO_SELECTED_SURVEY(oCParam);
                }
                catch(Exception e){
                }
            }
        });

        try{
            holder.Txt_evtsel_eventid.setText(filteredData.get(position).getEventID());
            holder.Txt_evtsel_eventname.setText(filteredData.get(position).getEventName());
            holder.Txt_evtsel_eventdesc.setText(filteredData.get(position).getEventDescription());
            holder.Txt_evtsel_event_startdate.setText(filteredData.get(position).getEventStartDate().replace("T00:00:00+07:00",""));
            holder.Txt_evtsel_event_enddate.setText(filteredData.get(position).getEventEndDate().replace("T00:00:00+07:00",""));
            holder.Txt_evtsel_surveytag.setText(filteredData.get(position).getEventSurveyTag());
            holder.Txt_evtsel_attribute1.setText(filteredData.get(position).getAttribute1());
            holder.Txt_evtsel_attribute2.setText(filteredData.get(position).getAttribute2());
            holder.Txt_evtsel_attribute3.setText(filteredData.get(position).getAttribute3());
            holder.Txt_evtsel_attribute4.setText(filteredData.get(position).getAttribute4());
        }
        catch(Exception e){
        }

        return convertView;
    }

}
