package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.activity.TakeSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.model.CMTag;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderMRadioGroup;

import org.kobjects.util.Strings;

import java.util.ArrayList;
import java.util.List;

/*
    keterangan untuk data model :
    =============================
    survey_tag          = SVT2
    question_tag        = Q21
    matrix_question_tag = Q21_a
    answer_option_tag   = {Q21_a_1 | Q21_a_2 | Q21_a_3 | Q21_a_4 | Q21_a_5}
    answer_option_value = {Sangat tidak ingin | Kurang ingin | Belum bisa memastikan | Cukup ingin | Sangat ingin }
    override_next_def   = {QE | QE | QE | QE | QE}
 */

public class CustomListAdapterMRadioGroup extends RecyclerView.Adapter<ViewHolderMRadioGroup> {
    private Context mcontext;
    private LayoutInflater mInflater;
    private List<CAnswerOption> list_pilihan;
    private String strEventID;

    TakeSurveyActivity tsa;
    MySQLiteHelper odbhelper;

    public CustomListAdapterMRadioGroup(Context ctx, MySQLiteHelper dbhelper, View vw, List<CAnswerOption> lst,String eventID) {
        Log.d("[GudangGaram]", "CustomListAdapterMRadioGroup");
        list_pilihan = lst;
        mcontext    = ctx;
        odbhelper   = dbhelper;
        strEventID  = eventID;
        tsa = new TakeSurveyActivity();
    }

    public List<CAnswerOption> getList_pilihan() {
        return list_pilihan;
    }

    @Override
    public ViewHolderMRadioGroup onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("[GudangGaram]", "CustomListAdapterMRadioGroup :: onCreateViewHolder");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rwlist_mradiogroup, parent, false);
        ViewHolderMRadioGroup viewHolder = new ViewHolderMRadioGroup(view);

        return viewHolder;
    }

    private List<String> parsing_string_delimited(String strDelimited){
        List<String> result = null;
        String [] delim;

        result = new ArrayList<>();
        delim = strDelimited.replace("{","").replace("}","").split("\\|");
        for (int i = 0; i< delim.length; i++){
            result.add(delim[i].trim());
        }
        return result;
    }


    @Override
    public void onBindViewHolder(ViewHolderMRadioGroup holder, int position) {
        Log.d("[GudangGaram]", "CustomListAdapterRadioGroup :: onBindViewHolder");
        List<String> Lanswer_option;
        List<String> Lanswer_tag_option;
        List<String> Lanswer_next_route;
        Integer idx;
        String replaceValueLabel;
        String replaceValueLabelOption;


        CAnswerOption pilihan_jawaban = list_pilihan.get(position);
        holder.answercaption.setText(pilihan_jawaban.getMatrixQuestionLabel());
        try{
            replaceValueLabelOption = odbhelper.getReplacedSentenceOverVariable(strEventID, pilihan_jawaban.getAnswerOptionValue());
            Lanswer_option     = parsing_string_delimited(replaceValueLabelOption);
            Lanswer_tag_option = parsing_string_delimited(pilihan_jawaban.getAnswerOptionTag());
            Lanswer_next_route = parsing_string_delimited(pilihan_jawaban.getOverrideNextDef());

            if(Lanswer_option.size() == Lanswer_tag_option.size()) {
                if (Lanswer_option.size() > 0) {
                    idx = 0;
                    for (String tag_option : Lanswer_tag_option) {

                        // ------------- define radio button  -------------
                        RadioButton rdbtn = new RadioButton(mcontext);
                        rdbtn.setLayoutParams(new RadioGroup.LayoutParams(200, RadioGroup.LayoutParams.MATCH_PARENT));
                        rdbtn.setPadding(0,0,20,0);
                        rdbtn.setButtonDrawable(null);
                        TypedArray a = mcontext.getTheme().obtainStyledAttributes(R.style.RadioWithTextOnTop, new int[] {android.R.attr.listChoiceIndicatorSingle});
                        int attributeResourceId = a.getResourceId(0, 0);
                        Drawable drawable = mcontext.getResources().getDrawable(attributeResourceId);
                        rdbtn.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
                        rdbtn.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);

                        // encap tag to have tag name and value that will useful to extract next
                        CMTag ctag = new CMTag();
                        ctag.setStrTag(tag_option);
                        ctag.setStrVal(Lanswer_option.get(idx).toString());

                        // only display label on top just first of record
                        if (position == 0) {
                            rdbtn.setText(Lanswer_option.get(idx).toString());
                        }
                        rdbtn.setTag(ctag);
                        rdbtn.setTextSize(24);

                        // ------------- add radio button to radio group -------------

                        holder.answeroption.addView(rdbtn);
                        idx +=1;
                    }
                }
            }
        }
        catch(Exception e){
        }
    }

    @Override
    public int getItemCount() {
        //Log.d("[GudangGaram]", "CustomListAdapterMRadioGroup :: getItemCount");
        return list_pilihan.size();
    }

}

