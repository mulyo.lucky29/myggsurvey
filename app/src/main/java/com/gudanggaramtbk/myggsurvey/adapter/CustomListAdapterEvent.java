package com.gudanggaramtbk.myggsurvey.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.activity.DownloadSurveyActivity;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderEvent;
import java.util.List;

public class CustomListAdapterEvent extends ArrayAdapter<CEvent> {
    private Context mcontext;
    private List<CEvent> filteredData = null;
    private LayoutInflater mInflater;
    private MySQLiteHelper dbHelper;
    private String pType;
    private DownloadSurveyActivity PActivity;

    public CustomListAdapterEvent(Context         context,
                                  MySQLiteHelper  dbHelper,
                                  List<CEvent>     list,
                                  String           type) {
        super(context,0,list);
        this.mcontext = context;
        this.dbHelper = dbHelper;
        this.pType    = type;

        //this.cParent  = oParent;
        mInflater     = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.filteredData  = list;
    }


    public int getCount(){
        return filteredData.size();
    }
    public long getItemId(int position){
        return  position;
    }

    public void setParentAtivity(DownloadSurveyActivity oParent){
        Log.d("[GudangGaram]", "CustomFilterListAdapterEvent :: SetParentActivity");
        this.PActivity = oParent;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolderEvent holder;

        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.rwlist_event, parent, false);
            holder = new ViewHolderEvent();
            holder.Txt_evt_eventid         = (TextView) convertView.findViewById(R.id.txt_evt_eventid);
            holder.Txt_evt_eventname       = (TextView) convertView.findViewById(R.id.txt_evt_eventname);
            holder.Txt_evt_eventdesc       = (TextView) convertView.findViewById(R.id.txt_evt_eventdesc);
            holder.Txt_evt_event_startdate = (TextView) convertView.findViewById(R.id.txt_evt_event_startdate);
            holder.Txt_evt_event_enddate   = (TextView) convertView.findViewById(R.id.txt_evt_event_enddate);
            holder.Txt_evt_surveytag       = (TextView) convertView.findViewById(R.id.txt_evt_surveytag);
            holder.Txt_evt_attribute1      = (TextView) convertView.findViewById(R.id.txt_evt_attribute1);
            holder.Txt_evt_attribute2      = (TextView) convertView.findViewById(R.id.txt_evt_attribute2);
            holder.Txt_evt_attribute3      = (TextView) convertView.findViewById(R.id.txt_evt_attribute3);
            holder.Txt_evt_attribute4      = (TextView) convertView.findViewById(R.id.txt_evt_attribute4);
            holder.Cmd_evt_download        = (ImageButton) convertView.findViewById(R.id.cmd_evt_download);
            holder.Cmd_evt_delete          = (ImageButton) convertView.findViewById(R.id.cmd_evt_delete);


            if(pType.equals("ActiveEvent")){
                holder.Cmd_evt_delete.setVisibility(View.GONE);
                holder.Cmd_evt_download.setVisibility(View.VISIBLE);
            }
            else if(pType.equals("DownloadedEvent")){
                holder.Cmd_evt_delete.setVisibility(View.VISIBLE);
                holder.Cmd_evt_download.setVisibility(View.GONE);
            }

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolderEvent) convertView.getTag();
        }

        holder.Cmd_evt_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", "CustomListAdapterEvent :: Cmd_evt_download OnClick");
                try{
                    // save to db event list selected
                    CEvent oCOvent = new CEvent();
                    oCOvent.setEventID(holder.Txt_evt_eventid.getText().toString());
                    oCOvent.setEventName(holder.Txt_evt_eventname.getText().toString());
                    oCOvent.setEventDescription(holder.Txt_evt_eventdesc.getText().toString());
                    oCOvent.setEventStartDate(holder.Txt_evt_event_startdate.getText().toString());
                    oCOvent.setEventEndDate(holder.Txt_evt_event_enddate.getText().toString());
                    oCOvent.setEventSurveyTag(holder.Txt_evt_surveytag.getText().toString());
                    oCOvent.setAttribute1(holder.Txt_evt_attribute1.getText().toString());
                    oCOvent.setAttribute2(holder.Txt_evt_attribute2.getText().toString());
                    oCOvent.setAttribute3(holder.Txt_evt_attribute3.getText().toString());
                    oCOvent.setAttribute4(holder.Txt_evt_attribute4.getText().toString());

                    dbHelper.AddEvent(oCOvent);
                    // call ws to download the survey set
                    PActivity.EH_CMD_DOWNLOAD_SURVEY_SET(holder.Txt_evt_eventid.getText().toString(), holder.Txt_evt_surveytag.getText().toString());
                }
                catch(Exception e){
                }
            }
        });

        holder.Cmd_evt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("[GudangGaram]", "CustomListAdapterEvent :: Cmd_evt_delete OnClick");

                //PActivity.EH_cmd_close_filter();
            }
        });


        //String creation_date = new SimpleDateFormat("dd-MMM-yy hh:mm:ss a").format(new Date());

        try{
            holder.Txt_evt_eventid.setText(filteredData.get(position).getEventID());
            holder.Txt_evt_eventname.setText(filteredData.get(position).getEventName());
            holder.Txt_evt_eventdesc.setText(filteredData.get(position).getEventDescription());
            holder.Txt_evt_event_startdate.setText(filteredData.get(position).getEventStartDate().replace("T00:00:00+07:00",""));
            holder.Txt_evt_event_enddate.setText(filteredData.get(position).getEventEndDate().replace("T00:00:00+07:00",""));
            holder.Txt_evt_surveytag.setText(filteredData.get(position).getEventSurveyTag());
            holder.Txt_evt_attribute1.setText(filteredData.get(position).getAttribute1());
            holder.Txt_evt_attribute2.setText(filteredData.get(position).getAttribute2());
            holder.Txt_evt_attribute3.setText(filteredData.get(position).getAttribute3());
            holder.Txt_evt_attribute4.setText(filteredData.get(position).getAttribute4());
        }
        catch(Exception e){
        }

        return convertView;
    }


}