package com.gudanggaramtbk.myggsurvey.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.model.CAnswerOption;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderHRadioGroup;
import java.util.ArrayList;
import java.util.List;

public class CustomListAdapterHRadioGroup extends RecyclerView.Adapter<ViewHolderHRadioGroup> {
    private Context mcontext;
    private LayoutInflater mInflater;
    private List<CAnswerOption> list_temp;
    private List<CAnswerOption> list_pilihan;
    private MySQLiteHelper odbhelper;
    private String strEventID;


    public CustomListAdapterHRadioGroup(Context ctx, MySQLiteHelper dbhelper, View vw, List<CAnswerOption> lst,String eventID) {
        Log.d("[GudangGaram]", "CustomListAdapterHRadioGroup");
        list_temp         = lst;
        list_pilihan      = Construct_ListRadioGroup(lst);
        mcontext          = ctx;
        odbhelper         = dbhelper;
        strEventID        = eventID;
    }

    public List<CAnswerOption> getList_pilihan() {
        return list_pilihan;
    }

    private List<CAnswerOption> Construct_ListRadioGroup(List<CAnswerOption> lst){
        String RGroupTag = "R" + lst.get(0).getQuestionTag().toString();
        List<CAnswerOption> result = new ArrayList<CAnswerOption>();
        result.add(new CAnswerOption(RGroupTag,"","",""));

        return result;
    }

    @Override
    public ViewHolderHRadioGroup onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d("[GudangGaram]", "CustomListAdapterHRadioGroup :: onCreateViewHolder");
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.rwlist_hradiogroup, parent, false);
        ViewHolderHRadioGroup viewHolder = new ViewHolderHRadioGroup(view);
        viewHolder.setContext(mcontext);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolderHRadioGroup holder, int position) {
        Log.d("[GudangGaram]", "CustomListAdapterHRadioGroup :: onBindViewHolder");
        String replaceValue;

        holder.answerSet.setGravity(Gravity.CENTER_HORIZONTAL);
        holder.answerSet.setOrientation(LinearLayout.HORIZONTAL);
        for (CAnswerOption pilihan_jawaban : list_temp) {
            // ------------- define radio button  -------------
            RadioButton rdbtn = new RadioButton(mcontext);
            rdbtn.setLayoutParams(new RadioGroup.LayoutParams(200, RadioGroup.LayoutParams.MATCH_PARENT));
            rdbtn.setPadding(0,0,20,0);
            rdbtn.setButtonDrawable(null);
            TypedArray a = mcontext.getTheme().obtainStyledAttributes(R.style.RadioWithTextOnTop, new int[] {android.R.attr.listChoiceIndicatorSingle});
            int attributeResourceId = a.getResourceId(0, 0);
            Drawable drawable = mcontext.getResources().getDrawable(attributeResourceId);
            rdbtn.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
            rdbtn.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
            replaceValue = odbhelper.getReplacedSentenceOverVariable(strEventID, pilihan_jawaban.getAnswerOptionValue().toString());
            rdbtn.setText(replaceValue);
            rdbtn.setTag(pilihan_jawaban.getAnswerOptionTag().toString());
            rdbtn.setTextSize(24);
            // ------------- add radio button to radio group -------------
            holder.answerSet.addView(rdbtn);
        }
    }

    @Override
    public int getItemCount() {
        //Log.d("[GudangGaram]", "CustomListAdapterHRadioGroup :: getItemCount");
        return list_pilihan.size();
    }
}