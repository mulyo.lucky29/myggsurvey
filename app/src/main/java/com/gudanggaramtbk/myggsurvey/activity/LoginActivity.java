package com.gudanggaramtbk.myggsurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.model.CConfig;
import com.gudanggaramtbk.myggsurvey.util.DumpDatabase;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private static Spinner Cbo_event_area;
    private static TextView Txt_pic_name;
    private static Button Cmd_Login;
    private static Button Cmd_Exit;
    private MySQLiteHelper dbHelper;
    private DumpDatabase ddb;
    boolean doubleBackToExitPressedOnce         = false;
    private String StrDeviceID;
    private static final int REQUEST_PERMISSION = 1;

    private static String[] PERMISSIONS_APPS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    void check_apps_permission() {
        // popup permission granted
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        // if dont have permit before then popup
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    PERMISSIONS_APPS,
                    REQUEST_PERMISSION
            );
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        // initialize dbHelper
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        Cbo_event_area  = (Spinner)findViewById(R.id.cbo_event_area);
        Txt_pic_name    = (TextView)findViewById(R.id.txt_nama_presenter);
        Cmd_Login       = (Button)findViewById(R.id.cmd_login);
        Cmd_Exit        = (Button)findViewById(R.id.cmd_exit);

        Cmd_Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // redirect to main page
                EH_cmd_login();
            }
        });

        Cmd_Exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_exit();
            }
        });

        // init combo area
        initComboBox(R.id.cbo_event_area,dbHelper.getListArea());
        // check application permission
        check_apps_permission();
        // create dump
        EH_Create_DumpDB();
    }

    private void EH_Create_DumpDB(){
        try{
            ddb = new DumpDatabase();
            ddb.setAttribute(StrDeviceID);
            ddb.CreateDumpDatabase();
        }
        catch(Exception e){
        }
    }

    @Override
    public void onBackPressed() {
        try{
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
        catch(Exception e){
        }
    }

    public void EH_cmd_exit(){
        Log.d("[GudangGaram]", "LoginActivity :: EH_cmd_exit");
        try{
            if(Build.VERSION.SDK_INT>=16 && Build.VERSION.SDK_INT<21){
                finishAffinity();
            } else if(Build.VERSION.SDK_INT>=21){
                finishAndRemoveTask();
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: EH_cmd_exit Exception : " + e.getMessage().toString());
        }
    }

    public void EH_cmd_login(){
        Log.d("[GudangGaram]", "LoginActivity :: EH_cmd_login");
        String v_txt_event_area = "";
        String v_txt_pic_name = "";

        doAutoCopySetting();
        v_txt_pic_name     = Txt_pic_name.getText().toString();

        // user default no need to access area specific therefore no access to event per area just only system and master download or flush data
        if(v_txt_pic_name.equals("default")){
            v_txt_event_area  = "";
            GoToMainPage(v_txt_pic_name,v_txt_event_area);
        }
        else{
            try{
                // else user like spv or other name presenter need to access their own area
                v_txt_event_area   = Cbo_event_area.getSelectedItem().toString();
                if(v_txt_event_area.equals("")){
                    Toast.makeText(this, "This Is The First Time You Login, Please Contact your Authorities To do Download Master First",Toast.LENGTH_SHORT);
                }
                else{
                    if(v_txt_pic_name.trim().length() > 0){
                        GoToMainPage(v_txt_pic_name,v_txt_event_area);
                    }
                }
            }
            catch (Exception e){
                Toast.makeText(this, "This Is The First Time You Login, Please Contact your Authorities To do Download Master First",Toast.LENGTH_SHORT);
            }
        }

    }

    private void GoToMainPage(String strTxtPicName, String strTxtEventArea){
        Log.d("[GudangGaram]", "LoginActivity :: GoToMainPage");
        try{
            Intent Main_intent = new Intent(this.getBaseContext(), MainActivity.class);
            Main_intent.putExtra("PIC_NAME", strTxtPicName);
            Main_intent.putExtra("EVENT_AREA", strTxtEventArea);
            startActivity(Main_intent);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: GoToMainPage Exception : " + e.getMessage().toString());
        }
    }
    private void initComboBox(final int p_layout, List<String> lvi)
    {
        Spinner spinner = (Spinner) findViewById(p_layout);
        try{
            // Creating adapter for spinner
            //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lvi);
            // Drop down layout style - list view with radio button
            //dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.spinner_item , lvi);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
            // attaching data adapter to spinner
            spinner.setAdapter(dataAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }

            });
        }
        catch(Exception e){
        }
    }

    public void doAutoCopySetting() {
        String file_shared_preference;
        String folder_inject_config;
        String file_inject_config;
        File fs_override;
        File fs_preference;
        CConfig tcofig;

        tcofig = new CConfig();
        Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting");
        try{
            //"/data/data/com.gudanggaramtbk.myggsurvey/shared_prefs/"
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                folder_inject_config = Environment.getExternalStorageDirectory().toString() + File.separator + "ggsurvey" + File.separator;
                file_shared_preference =  "/data/data/" + getApplicationContext().getPackageName() + File.separator + "shared_prefs" + File.separator + getString(R.string.app_config) + ".xml";
                file_inject_config     = folder_inject_config + getString(R.string.app_config) + ".xml";
                Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: file sharedpreference > " + file_shared_preference);
                Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: file_inject_config    > " + file_inject_config);

                fs_override = new File(file_inject_config);
                if(fs_override.exists()) {
                        Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: override_config_file exists");
                        Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: Parsing override_config file");
                        // read override file config
                        tcofig = read_inject_xml(file_inject_config);
                        // do replacement existing shared preference in default path : /data/data/com.gudanggaramtbk.com/myggsurvey/shared_pref/
                        fs_preference = new File(file_shared_preference);
                        if(fs_preference.exists()){
                            Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: file sharedpreference exists");
                            Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: file sharedpreference Reapplied");
                            re_apply_shared_preference(tcofig);
                        }
                        else{
                            Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: file sharedpreference Not exists");
                            // copy file_inject_config into file_shared_preference
                            Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: file sharedpreference Copy From Inject To Existing");
                            copyFile(file_inject_config,file_shared_preference);
                            Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: file sharedpreference Reapplied");
                            re_apply_shared_preference(tcofig);
                        }
                        Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: delete override_config_file");
                        fs_override.delete();
                }
                else{
                        Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: override_config_file Not exists Do Not Change Anthing");
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting Exception : " + e.getMessage().toString());
        }
    }

    public void doDumpDatabase(){
        Log.d("[GudangGaram]", "LoginActivity :: doDumpDatabase");
        /*
        try{
            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                folder_inject_config = Environment.getExternalStorageDirectory().toString() + File.separator + "ggsurvey" + File.separator;
                file_shared_preference = "/data/data/" + getApplicationContext().getPackageName() + File.separator + "shared_prefs" + File.separator + getString(R.string.app_config) + ".xml";
                file_inject_config = folder_inject_config + getString(R.string.app_config) + ".xml";

                fs_override = new File(file_inject_config);
                if (fs_override.exists()) {
                    // read override file config
                    tcofig = read_inject_xml(file_inject_config);
                    // do replacement existing shared preference in default path : /data/data/com.gudanggaramtbk.com/myggsurvey/shared_pref/
                    fs_preference = new File(file_shared_preference);
                    copyFile(file_inject_config, file_shared_preference);
                } else {
                    Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting :: override_config_file Not exists Do Not Change Anthing");
                }
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: doAutoCopySetting Exception : " + e.getMessage().toString());
        }
        */
    }

    public CConfig read_inject_xml(String fileinjectpath){
        Log.d("[GudangGaram]", "LoginActivity :: read_inject_xml");
        int eventType;
        String XML_ROOT_STRING = "map";
        XmlPullParserFactory xmlFactoryObject;
        XmlPullParser parser;
        InputStream fis = null;
        String currentTag = "";
        String currAttribute = "";
        String currentValue  = "";
        CConfig result;

        result = new CConfig();
        try{
            fis = new FileInputStream(fileinjectpath);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: read_inject_xml : fis Exception : " + e.getMessage().toString());
        }

        if(!(fis == null)){
            Log.d("[GudangGaram]", "LoginActivity :: read_inject_xml fis not null");
            try{
                xmlFactoryObject = XmlPullParserFactory.newInstance();
                xmlFactoryObject.setNamespaceAware(true);
                parser = xmlFactoryObject.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES,false);
                parser.setInput(fis, null);
                eventType = parser.getEventType();

                // Loop through the xml document
                while(eventType != XmlPullParser.END_DOCUMENT){
                    if(eventType == XmlPullParser.START_TAG){
                        if(!parser.getName().equals(XML_ROOT_STRING)){
                            currentTag = parser.getName();
                            // Get the attributes value from the current tag
                            currAttribute = parser.getAttributeValue(null,"name");
                        }
                    }
                    else if(eventType == XmlPullParser.END_TAG){
                        currentTag = null;
                    }
                    else if(eventType == XmlPullParser.TEXT){
                        if(currentTag!=null){
                            // Get the text from current tag
                            if(currAttribute.equals("SoapNamespace")){
                                currentValue = parser.getText();
                                result.setCStrSoapNamespace(currentValue);
                                Log.d("[GudangGaram]", "LoginActivity :: read_inject_xml fis SoapNamespace > " + currentValue);
                            }
                            else if(currAttribute.equals("SoapAddress")){
                                currentValue = parser.getText();
                                result.setCstrSoapAddress(currentValue);
                                Log.d("[GudangGaram]", "LoginActivity :: read_inject_xml fis SoapAddress   > " + currentValue);
                            }
                            else if(currAttribute.equals("URLUpdater")){
                                currentValue = parser.getText();
                                result.setCStrUrlUpdater(currentValue);
                                Log.d("[GudangGaram]", "LoginActivity :: read_inject_xml fis UrlUpdater    > " + currentValue);
                            }
                        }
                    }
                    eventType = parser.next();
                }
            }
            catch(XmlPullParserException e){
                e.printStackTrace();
            }
            catch(IOException e){
                e.printStackTrace();
            }
            catch(Exception e){
                Log.d("[GudangGaram]", "LoginActivity :: read_inject_xml Exception : " + e.getMessage().toString());
            }
        }
        else{
            Log.d("[GudangGaram]", "LoginActivity :: read_inject_xml fis null");
        }
        return result;
    }

    public  void re_apply_shared_preference(CConfig oConPig){
        Log.d("[GudangGaram]", "LoginActivity :: re_apply_shared_preference");
        try{
            SharedPreferences config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
            SharedPreferences.Editor editor = config.edit();
            Log.d("[GudangGaram]", "LoginActivity :: re_apply_shared_preference > SoapAddress   : " + oConPig.getCstrSoapAddress());
            Log.d("[GudangGaram]", "LoginActivity :: re_apply_shared_preference > SoapNamespace : " + oConPig.getCStrSoapNamespace());
            Log.d("[GudangGaram]", "LoginActivity :: re_apply_shared_preference > UrlUpdater    : " + oConPig.getCStrUrlUpdater());
            editor.putString("SoapAddress",oConPig.getCstrSoapAddress());
            editor.putString("SoapNamespace",oConPig.getCStrSoapNamespace());
            editor.putString("URLUpdater",oConPig.getCStrUrlUpdater());
            editor.apply();
            editor.commit();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "LoginActivity :: re_apply_shared_preference Exception : " + e.getMessage().toString());
        }
    }
    private void notify_reload_apps(){
        Log.d("[GudangGaram]", "LoginActivity :: notify_reload_apps");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle("Config Loaded Reboot Apps Needed")
                .setMessage("Please Click Ok To Close Your Apps Then Open It Again, Due To Auto Config Load")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                EH_cmd_exit();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void copyFile(String inputPath, String outputPath) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

        } catch (FileNotFoundException fnfe1) {
        } catch (Exception e) {
        }
    }
}

