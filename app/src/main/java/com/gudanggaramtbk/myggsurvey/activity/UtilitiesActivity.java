package com.gudanggaramtbk.myggsurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.util.DumpDatabase;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.util.RestoreDatabase;

public class UtilitiesActivity extends AppCompatActivity {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private String StrSessionName;
    private String StrEventArea;
    private String StrDeviceID;
    private ImageButton Cmd_restoreDB;
    private ImageButton Cmd_dumpDB;
    private ImageButton Cmd_deletedumpDB;

    private UtilitiesActivity pActivity;
    private RestoreDatabase rdb;
    private DumpDatabase ddb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utilities);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        context = UtilitiesActivity.this;

        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        pActivity = this;

        rdb = new RestoreDatabase();
        rdb.setAttribute(pActivity,context,dbHelper,config,StrDeviceID);

        ddb = new DumpDatabase();
        ddb.setAttribute(StrDeviceID);

        Cmd_dumpDB       = (ImageButton) findViewById(R.id.cmd_dumpdb);
        Cmd_restoreDB    = (ImageButton) findViewById(R.id.cmd_restoredb);
        Cmd_deletedumpDB = (ImageButton) findViewById(R.id.cmd_deldumpdb);

        Cmd_restoreDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Restore Database Confirm")
                        .setMessage("Confirm To Restore Database From Server ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_RESTORE_DB();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Cmd_dumpDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Dump Database Confirm")
                        .setMessage("This Will Create Dump Database, Are you want to continue ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_DUMP_DB();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Cmd_deletedumpDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Delete Dump Database Confirm")
                        .setMessage("This Will Delete Dump Database, Are you want to continue ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_DELETE_DUMP_DB();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

    }

    public void EH_CMD_DUMP_DB(){
        try{
            ddb.CreateDumpDatabase();
        }
        catch(Exception e){
        }
    }

    public void EH_CMD_DELETE_DUMP_DB(){
        try{
            ddb.deleteDumpDB();
        }
        catch(Exception e){
        }
    }

    public void EH_CMD_RESTORE_DB(){
        try{
            rdb.do_restore_db();
        }
        catch(Exception e){
        }
    }

    public void EH_CMD_EXIT_TO_APPLY_RESTOREDB(){
        Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_cmd_exit");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder
                .setTitle("Apply Restored Database")
                .setMessage("This will need to be restarted due to apply restored database, Do You Want to Continue ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                try{
                                    if(Build.VERSION.SDK_INT>=16 && Build.VERSION.SDK_INT<21){
                                        finishAffinity();
                                    } else if(Build.VERSION.SDK_INT>=21){
                                        finishAndRemoveTask();
                                    }
                                }
                                catch(Exception e){
                                    Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_cmd_exit Exception : " + e.getMessage().toString());
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "DownloadMasterActivity :: upPressed Sync Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    upIntent.putExtra("EVENT_AREA", StrEventArea);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // left blank to use override and do nothing to prevent back bottom navigation to be used
    }
}
