package com.gudanggaramtbk.myggsurvey.activity;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.util.DownloadUpdate;
import com.gudanggaramtbk.myggsurvey.R;

public class AboutActivity extends AppCompatActivity {
    private DownloadUpdate updaterApk;
    private SharedPreferences config;
    private Button CmdUpdateApk;
    private String StrSessionName;
    private String StrEventArea;
    private String StrDeviceID;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "AboutActivity :: upPressed About Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    upIntent.putExtra("EVENT_AREA", StrEventArea);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);

        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        // initialize updater apk
        updaterApk = new DownloadUpdate(config);
        updaterApk.setContext(this);

        CmdUpdateApk = (Button)findViewById(R.id.cmd_updateAPK);
        CmdUpdateApk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updaterApk.DownloadApk();
            }
        });

    }

    @Override
    public void onBackPressed() {
    }
}