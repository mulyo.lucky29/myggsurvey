package com.gudanggaramtbk.myggsurvey.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import com.google.android.material.navigation.NavigationView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterMain;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterMainSummary;
import com.gudanggaramtbk.myggsurvey.model.CMainSummary;
import com.gudanggaramtbk.myggsurvey.model.CSurveyH;
import com.gudanggaramtbk.myggsurvey.service.SendSurvey;
import com.gudanggaramtbk.myggsurvey.util.Messages;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.viewholder.ViewHolderMain;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private MySQLiteHelper dbHelper;
    private SharedPreferences config;
    private String StrSessionName;
    private String StrEventArea;
    private String StrDeviceID;
    private Context ctx;
    private List<CSurveyH> lvi;
    private List<CMainSummary> lvi2;
    private ListView list1, list2;
    private TabHost thost;
    private Messages msg;
    private CustomListAdapterMain adapter;
    private CustomListAdapterMainSummary adapter2;
    private SendSurvey svcSendSurvey;
    private FloatingActionButton fab;

    private boolean doubleBackToExitPressedOnce = false;

    private static final int REQUEST_PERMISSION = 1;

    private static String[] PERMISSIONS_APPS = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
    };

    void check_apps_permission() {
        // popup permission granted
        int permission = ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        // if dont have permit before then popup
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    MainActivity.this,
                    PERMISSIONS_APPS,
                    REQUEST_PERMISSION
            );
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //if (android.os.Build.VERSION.SDK_INT > 9) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        //}
        Log.d("[GudangGaram]", "Config   : " + getString(R.string.app_config));

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();
        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);

        ctx = this;
        msg = new Messages(ctx);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // -------------- fab ------------
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_cmd_takeSurvey();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hView = navigationView.getHeaderView(0);

        TextView nav_device_id  = (TextView) hView.findViewById(R.id.txt_xdevice_id);
        TextView nav_pic_name   = (TextView) hView.findViewById(R.id.txt_nav_pic_name);
        TextView nav_event_area = (TextView) hView.findViewById(R.id.txt_nav_event_area);
        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        // --------------- set display menu info --------------------
        nav_pic_name.setText(StrSessionName);
        nav_device_id.setText(StrDeviceID);
        nav_event_area.setText(StrEventArea);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        list1 = (ListView) findViewById(R.id.lvi_main);
        list1.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                list1.setSelection(position);
            }
        });
        list2 = (ListView) findViewById(R.id.lvi_summary);
        list2.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        thost = (TabHost)findViewById(R.id.TabDownMain);
        thost.setup();
        TabHost.TabSpec spec = thost.newTabSpec("Download Wizard");
        try{
            //Tab 1
            spec.setContent(R.id.MTab_Summary);
            spec.setIndicator("Survey Summary");
            thost.addTab(spec);

            //Tab 2
            spec = thost.newTabSpec("Downloaded Asset");
            spec.setContent(R.id.MTab_List);
            spec.setIndicator("Survey List");
            thost.addTab(spec);
            thost.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
                @Override
                public void onTabChanged(String tabId) {
                    int tab = thost.getCurrentTab();
                    Log.d("[GudangGaram]", "DownloadSurveyActivity :: TabSelected[" + tab + "]");
                    if(tab == 0){
                        EH_cmd_Refresh_List_Summary();
                    }
                    else if(tab == 1){
                        EH_cmd_Refresh_List();
                    }
                }
            });

        }
        catch(Exception e){
        }

        // ------------- responsibility restriction ---------
        Menu menu = navigationView.getMenu();

        if(StrSessionName.equals("default")){
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_take_survey).setVisible(false);
            menu.findItem(R.id.nav_download_survey).setVisible(false);
            menu.findItem(R.id.nav_download_master).setVisible(true);
            menu.findItem(R.id.nav_utilities).setVisible(true);
            menu.findItem(R.id.nav_log_viewer).setVisible(true);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);
            thost.setVisibility(View.GONE);
            fab.setVisibility(View.GONE);
        }
        else if(StrSessionName.equals("spv")){
            menu.findItem(R.id.nav_setting).setVisible(true);
            menu.findItem(R.id.nav_take_survey).setVisible(false);
            menu.findItem(R.id.nav_download_survey).setVisible(true);
            menu.findItem(R.id.nav_log_viewer).setVisible(false);
            menu.findItem(R.id.nav_download_master).setVisible(false);
            menu.findItem(R.id.nav_utilities).setVisible(false);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);

            thost.setVisibility(View.VISIBLE);
            fab.setVisibility(View.GONE);
            EH_cmd_Refresh();
        }
        else {
            menu.findItem(R.id.nav_setting).setVisible(false);
            menu.findItem(R.id.nav_take_survey).setVisible(true);
            menu.findItem(R.id.nav_download_survey).setVisible(false);
            menu.findItem(R.id.nav_log_viewer).setVisible(false);
            menu.findItem(R.id.nav_download_master).setVisible(false);
            menu.findItem(R.id.nav_utilities).setVisible(false);
            menu.findItem(R.id.nav_about).setVisible(true);
            menu.findItem(R.id.nav_exit).setVisible(true);

            thost.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);

            // refresh
            EH_cmd_Refresh();
        }

        // check application permission
        check_apps_permission();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        if(StrSessionName.equals("default")){
            menu.findItem(R.id.cmd_mn_select_all).setVisible(false);
            menu.findItem(R.id.cmd_mn_clear_all).setVisible(false);
            menu.findItem(R.id.cmd_mn_sync).setVisible(false);
            menu.findItem(R.id.cmd_mn_flush).setVisible(true);
        }
        else if(StrSessionName.equals("spv")){
            menu.findItem(R.id.cmd_mn_select_all).setVisible(true);
            menu.findItem(R.id.cmd_mn_clear_all).setVisible(true);
            menu.findItem(R.id.cmd_mn_sync).setVisible(true);
            menu.findItem(R.id.cmd_mn_flush).setVisible(false);
        }
        else{
            menu.findItem(R.id.cmd_mn_select_all).setVisible(false);
            menu.findItem(R.id.cmd_mn_clear_all).setVisible(false);
            menu.findItem(R.id.cmd_mn_sync).setVisible(false);
            menu.findItem(R.id.cmd_mn_flush).setVisible(false);
        }
        return true;
    }

    public void EH_cmd_Refresh() {
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_refresh");
        try{
            EH_cmd_Refresh_List();
            EH_cmd_Refresh_List_Summary();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_refresh Exception : " + e.getMessage().toString());
        }
    }

    public void EH_cmd_Refresh_List(){
        Integer count;
        try {
            lvi = dbHelper.getListMain(StrEventArea);
            count = lvi.size();
            if(count > 0){
                adapter = new CustomListAdapterMain(ctx, lvi, dbHelper, list1);
                adapter.notifyDataSetChanged();
            }
            else{
                adapter = null;
            }
            list1.setAdapter(adapter);
        } catch (Exception e) {
            Log.d("[GudangGaram]", "EH_cmd_Refresh_List Exception : " + e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void EH_cmd_Refresh_List_Summary(){
        Integer count;
        try{
            lvi2 = dbHelper.getListMainSummary(StrEventArea);
            count = lvi2.size();
            if(count > 0){
                adapter2 = new CustomListAdapterMainSummary(ctx, lvi2, dbHelper, list2);
                adapter2.notifyDataSetChanged();
            }
            else{
                adapter2 = null;
            }
            list2.setAdapter(adapter2);
            Log.d("[GudangGaram]", "EH_cmd_Refresh_List_Summary count : " + list2.getCount());
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "EH_cmd_Refresh_List_Summary Exception : " + e.getMessage().toString()) ;
        }
    }

    /*
    @Override
    public void onBackPressed() {
        // prevent to close using down back button

        //if (doubleBackToExitPressedOnce) {
        //    super.onBackPressed();
        //    return;
        //}
        //this.doubleBackToExitPressedOnce = true;
        //Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        //new Handler().postDelayed(new Runnable() {
        //    @Override
        //    public void run() {
        //        doubleBackToExitPressedOnce = false;
        //    }
        //}, 2000);
    }
    */


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.cmd_mn_select_all) {
            EH_cmd_select_all();
            return true;
        }
        else if(id == R.id.cmd_mn_clear_all){
            EH_cmd_clear_all();
            return true;
        }
        else if(id == R.id.cmd_mn_sync){
            EH_cmd_send_item();
            return true;
        }
        else if(id == R.id.cmd_mn_flush){
            EH_cmd_flush_data();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_setting) {
            EH_cmd_setting();
        }
        else if (id == R.id.nav_take_survey) {
            EH_cmd_takeSurvey();
        }
        else if(id == R.id.nav_download_survey){
            EH_cmd_downloadSurvey();
        }
        else if(id == R.id.nav_download_master){
            EH_cmd_downloadMaster();
        }
        else if(id == R.id.nav_utilities){
            EH_cmd_utilities();
        }
        else if(id == R.id.nav_log_viewer){
            EH_cmd_logViewer();
        }
        else if (id == R.id.nav_about) {
            EH_cmd_about();
        }
        else if (id == R.id.nav_exit) {
            EH_cmd_exit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // -------------------------- Event Handler -----------------------------
    public void EH_cmd_logViewer(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_setting");
        Intent I_LogView;
        I_LogView = new Intent(this, LogcatActivity.class);
        I_LogView.putExtra("PIC_NAME", StrSessionName);
        I_LogView.putExtra("EVENT_AREA", StrEventArea);
        startActivity(I_LogView);
    }

    public void EH_cmd_setting() {
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_setting");
        Intent I_Setting;
        I_Setting = new Intent(this, SettingActivity.class);
        I_Setting.putExtra("PIC_NAME", StrSessionName);
        I_Setting.putExtra("EVENT_AREA", StrEventArea);
        startActivity(I_Setting);
    }

    public void EH_cmd_about() {
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_about");
        Intent I_About;
        I_About = new Intent(this, AboutActivity.class);
        I_About.putExtra("PIC_NAME", StrSessionName);
        I_About.putExtra("EVENT_AREA", StrEventArea);
        startActivity(I_About);
    }

    public void EH_cmd_exit() {
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_exit");
        Intent objsignOut = new Intent(getBaseContext(),LoginActivity.class);
        objsignOut.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(objsignOut);
    }

    public void EH_cmd_takeSurvey() {
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_takeSurvey");
        Intent I_GoToTakeSurvey;
        I_GoToTakeSurvey = new Intent(this, GoToTakeSurveyActivity.class);
        I_GoToTakeSurvey.putExtra("PIC_NAME", StrSessionName);
        I_GoToTakeSurvey.putExtra("EVENT_AREA", StrEventArea);
        startActivity(I_GoToTakeSurvey);
    }

    public void EH_cmd_downloadMaster(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_downloadMaster");
        Intent I_DownloadMaster;
        I_DownloadMaster = new Intent(this, DownloadMasterActivity.class);
        I_DownloadMaster.putExtra("PIC_NAME", StrSessionName);
        I_DownloadMaster.putExtra("EVENT_AREA", StrEventArea);
        startActivity(I_DownloadMaster);
    }

    public void EH_cmd_utilities(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_utilitiesr");
        Intent I_Utilities;
        I_Utilities = new Intent(this, UtilitiesActivity.class);
        I_Utilities.putExtra("PIC_NAME", StrSessionName);
        I_Utilities.putExtra("EVENT_AREA", StrEventArea);
        startActivity(I_Utilities);
    }

    public void EH_cmd_downloadSurvey(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_downloadSurvey");
        Intent I_DownloadSurvey;
        I_DownloadSurvey = new Intent(this, DownloadSurveyActivity.class);
        I_DownloadSurvey.putExtra("PIC_NAME", StrSessionName);
        I_DownloadSurvey.putExtra("EVENT_AREA", StrEventArea);
        startActivity(I_DownloadSurvey);
    }


    // ---------------------- event handler side menu -----------------
    public void EH_cmd_clear_all() {
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_clear_all Begin");
        try{
            thost.setCurrentTab(1);
            adapter.EH_CMD_CLEAR_ALL();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_clear_all Exception : " + e.getMessage());
        }
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_clear_all End");
    }
    public void EH_cmd_select_all() {
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_select_all Begin");
        try{
            thost.setCurrentTab(1);
            adapter.EH_CMD_SELECT_ALL();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_select_all Exception : " + e.getMessage());
        }
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_select_all End");
    }

    public void EH_cmd_flush_data(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_send_item");
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder
                    .setTitle("Flush All Data Confirm ")
                    .setMessage("Warning This will delete all data and its irreversible, Are You Sure Want To Continue To Flush All Data ?")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    EH_cmd_Process_flush_data();
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_send_item Exception : " + e.getMessage());
        }
    }

    public void EH_cmd_send_item() {
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_send_item");
        try{
            thost.setCurrentTab(1);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder
                    .setTitle("Send Data Confirm ")
                    .setMessage("Confirm Send Data ?")
                    .setCancelable(false)
                    .setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    EH_cmd_Process_send();
                                }
                            })
                    .setNegativeButton("Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_send_item Exception : " + e.getMessage());
        }
    }

    public void EH_cmd_Process_flush_data(){
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_flush_data");
        try{
            dbHelper.flush_all();
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_flush_data Exception : " + e.getMessage().toString());
        }
        finally {
            EH_cmd_Refresh();
        }
    }

    public void EH_cmd_Process_send() {
        Integer rown = 0;
        List<CSurveyH> listselected;
        Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send");
        // ------------ init send survey service object first -----------
        svcSendSurvey = new SendSurvey(config, this, dbHelper);
        svcSendSurvey.setParentActivity(this);
        rown = list1.getCount();
        listselected = new ArrayList<CSurveyH>();
        if (rown > 0) {
            try {
                for (Integer idx = 0; idx < rown; idx++) {
                    Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send :: checked idx = " + idx);
                    View HoldView = list1.getAdapter().getView(idx, null, null);
                    ViewHolderMain holder = (ViewHolderMain) HoldView.getTag();

                    CSurveyH csh = dbHelper.getSurveyHByID(holder.Txt_mn_session_id.getText().toString());
                    listselected.add(csh);
                }
                Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send > rown :" + rown);
                // call service to send data to server
                svcSendSurvey.Send(listselected,StrDeviceID);
            } catch (Exception e) {
                Log.d("[GudangGaram]", "MainActivity :: EH_cmd_Process_send Exception : " + e.getMessage().toString());
            }
        } else {
            msg.Show("Item Not Available", "Item Scanned Not Available, Please Scan and Save It First Before Send");
        }
    }
}