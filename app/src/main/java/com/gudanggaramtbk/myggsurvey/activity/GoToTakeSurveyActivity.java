package com.gudanggaramtbk.myggsurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterEvent;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterEventSel;
import com.gudanggaramtbk.myggsurvey.fragment.StartSurvey;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.model.StringWithTag;
import com.gudanggaramtbk.myggsurvey.service.DownloadEvent;
import com.gudanggaramtbk.myggsurvey.service.DownloadSurveySet;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;

import java.util.ArrayList;
import java.util.List;

public class GoToTakeSurveyActivity extends AppCompatActivity {
    private MySQLiteHelper dbHelper;
    private SharedPreferences config;
    private ListView listEvent;
    private Button CmdReload;
    private List<CEvent> SelectedEvent;
    private CustomListAdapterEventSel adapter;
    private List<CEvent> olvi;
    private Integer recordcount;
    private String StrSessionName;
    private String StrEventArea;
    private String StrSurveyType;
    private String StrDeviceID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_go_to_take_survey);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        try{
            // -------------- get session intent --------------
            StrSessionName  = getIntent().getStringExtra("PIC_NAME");
            StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
            StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "GoToTakeSurveyActivity Exception: " + e.getMessage().toString());
        }


        listEvent   = (ListView) findViewById(R.id.lvi_downloaded_event);
        CmdReload   = (Button)   findViewById(R.id.cmd_evt_select_reload);
        listEvent.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listEvent.setClickable(true);

        CmdReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_RELOAD_LIST();
            }
        });
        StrSurveyType = "";
        initKVPComboBox(R.id.cbo_survey_type,getListSurveyType());

        // auto reload for begin open
        EH_CMD_RELOAD_LIST();
    }

    private List<StringWithTag> getListSurveyType(){
        List<StringWithTag> result;
        result = new ArrayList<StringWithTag>();
        try{
            result = dbHelper.getListLookup("GS_SURVEY_TYPE");
        }
        catch(Exception e){
        }
        return result;
    }

    private void initKVPComboBox(final int p_layout, List<StringWithTag> lvi){
        Spinner spinner = (Spinner) findViewById(p_layout);
        // Creating adapter for spinner
        ArrayAdapter<StringWithTag> dataAdapter = new ArrayAdapter<StringWithTag>(this, android.R.layout.simple_spinner_item, lvi);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag swt = (StringWithTag) parent.getItemAtPosition(position);
                String       key = (String) swt.tag;
                StrSurveyType = key;
                Log.d("[GudangGaram]", " KVP Value Selected >> " + swt);
                Log.d("[GudangGaram]", " KVP Key   Selected >> " + key);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void EH_CMD_RELOAD_LIST(){
        Log.d("[GudangGaram]", "GoToTakeSurveyActivity :: EH_CMD_RELOAD_LIST");
        try{
            olvi = dbHelper.getListEvent(StrEventArea,StrSurveyType);
            EH_CMD_REFRESH(olvi);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "GoToTakeSurveyActivity :: EH_CMD_RELOAD_LIST Exception : " + e.getMessage());
        }
    }

    public void EH_CMD_REFRESH(List<CEvent> olvi){
        Log.d("[GudangGaram]", "GoToTakeSurveyActivity :: EH_CMD_REFRESH");
        try
        {
            recordcount = olvi.size();
            if(recordcount > 0){
                adapter = new CustomListAdapterEventSel(this, dbHelper, olvi);
                adapter.setParentAtivity(this);
                adapter.notifyDataSetChanged();
                listEvent.setAdapter(adapter);
            }
            else{
                listEvent.setAdapter(null);
                //msg.Show("Populate","Reload Return Zero Result");
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "GoToTakeSurveyActivity :: EH_CMD_REFRESH Exception :" + e.getMessage().toString());
            e.printStackTrace();
        }
    }

    public void EH_CMD_GO_TO_SELECTED_SURVEY(CEvent oCParam){
        Log.d("[GudangGaram]", "TakeSurveyActivity :: EH_CMD_GO_TO_SELECTED_SURVEY");
        try{
            Intent I_TakeSurvey;
            I_TakeSurvey = new Intent(this, TakeSurveyActivity.class);
            // send bundle in intent
            Bundle eventParam = new Bundle();
            eventParam.putString("strSurveyTag",oCParam.getEventSurveyTag());
            eventParam.putString("strEventID",oCParam.getEventID());
            eventParam.putString("strEventName",oCParam.getEventName());
            eventParam.putString("strEventDesc",oCParam.getEventDescription());
            eventParam.putString("strStartDate",oCParam.getEventStartDate());
            eventParam.putString("strEndDate",oCParam.getEventEndDate());
            eventParam.putString("strAttribute1",oCParam.getAttribute1());
            eventParam.putString("strAttribute2",oCParam.getAttribute2());
            eventParam.putString("strAttribute3",oCParam.getAttribute3());
            eventParam.putString("strAttribute4",oCParam.getAttribute4());

            // sending Intent Param To TakeSurveyActivity
            I_TakeSurvey.putExtra("PIC_NAME", StrSessionName);
            I_TakeSurvey.putExtra("EVENT_AREA", StrEventArea);
            I_TakeSurvey.putExtra("eventParam",eventParam);
            startActivity(I_TakeSurvey);
        }
        catch(Exception e){
        }
    }

    // need to be added if back to home
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "GoToTakeSurveyActivity :: upPressed Setting Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    upIntent.putExtra("EVENT_AREA", StrEventArea);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // left blank to use override and do nothing to prevent back bottom navigation to be used
    }
}
