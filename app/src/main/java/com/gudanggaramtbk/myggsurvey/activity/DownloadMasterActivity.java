package com.gudanggaramtbk.myggsurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.service.DownloadLookup;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;
import com.gudanggaramtbk.myggsurvey.util.RestoreDatabase;

public class DownloadMasterActivity extends AppCompatActivity {
    private SharedPreferences config;
    private Context context;
    private MySQLiteHelper dbHelper;
    private DownloadLookup downloadLOOK;
    private String StrSessionName;
    private String StrEventArea;
    private String StrDeviceID;
    private TextView Lbl_rec_DMLookup;
    private ImageButton Cmd_DMLOOK;
    private ImageButton Cmd_del_DMLookup;
    private ImageButton Cmd_restoreDB;
    private DownloadMasterActivity pActivity;
    private RestoreDatabase rdb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_master);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        context = DownloadMasterActivity.this;

        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        pActivity = this;

        // service to download master lookup
        downloadLOOK = new DownloadLookup(config);
        downloadLOOK.setContext(context);
        downloadLOOK.setParentActivity(pActivity);
        downloadLOOK.setDBHelper(dbHelper);

        Cmd_DMLOOK       = (ImageButton) findViewById(R.id.cmd_down_DMLookup);
        Cmd_del_DMLookup = (ImageButton) findViewById(R.id.cmd_del_DMLookup);
        Lbl_rec_DMLookup = (TextView) findViewById(R.id.lbl_rec_DMLookup);

        Cmd_DMLOOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Download Lookup Data Confirm")
                        .setMessage("Confirm Download Lookup Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_SYNC_M_LOOK();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        Cmd_del_DMLookup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder
                        .setTitle("Delete Lookup Data Confirm")
                        .setMessage("Confirm Delete Lookup Data ?")
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        EH_CMD_DEL_LOOK();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        EH_Reload_Record_Count("look");
    }

    public void EH_Reload_Record_Count(String Type){
        Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_Reload_Record_Count :: Begin");
        try{
            if(Type.equals("look")){
                Lbl_rec_DMLookup.setText(Long.toString(dbHelper.countRecordTable(dbHelper.getTableMLookup())) + " Records");
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_Reload_Record_Count Exception : " + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_Reload_Record_Count :: End");
        }
    }

    public void EH_CMD_DEL_LOOK(){
        Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_CMD_DEL_LOOK :: Begin");
        try{
            dbHelper.flushTable(dbHelper.getTableMLookup());
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_CMD_DEL_LOOK :: Exception : " + e.getMessage().toString());
        }
        finally {
            EH_Reload_Record_Count("look");
            Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_CMD_DEL_LOOK :: End");
        }
    }

    public void EH_CMD_SYNC_M_LOOK() {
        Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_CMD_SYNC_M_LOOK :: Begin");
        try{
            downloadLOOK.Retrieve(StrDeviceID);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_CMD_SYNC_M_LOOK :: Exception : " + e.getMessage().toString());
        }
        finally {
            Log.d("[GudangGaram]", "DownloadMasterActivity :: EH_CMD_SYNC_M_LOOK :: End");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "DownloadMasterActivity :: upPressed Sync Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    upIntent.putExtra("EVENT_AREA", StrEventArea);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // left blank to use override and do nothing to prevent back bottom navigation to be used
    }

}
