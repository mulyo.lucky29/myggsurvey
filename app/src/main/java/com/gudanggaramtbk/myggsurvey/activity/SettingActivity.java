package com.gudanggaramtbk.myggsurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import com.gudanggaramtbk.myggsurvey.R;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class SettingActivity extends AppCompatActivity {
    private Button CmdSaveSetting,CmdTestConn;
    private TextView TxtSoapAddress, TxtSoapNamespace,TxtUrlUpdater;
    String SSoapAddress,SSoapNamespace, SUrlUpdater;
    private String StrSessionName;
    private String StrEventArea;
    private String StrDeviceID;

    SharedPreferences config;
    ProgressDialog pd;

    String SSoapMethodTest        = "Get_Test_Result";

    // need to be added if back to home
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "SettingActivity :: upPressed Setting Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    upIntent.putExtra("EVENT_AREA", StrEventArea);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        CmdSaveSetting      = (Button)findViewById(R.id.cmd_saveSetting);
        CmdTestConn         = (Button)findViewById(R.id.cmd_testConn);
        TxtSoapAddress      = (TextView) findViewById(R.id.txt_SettingSOAPAddress);
        TxtSoapNamespace    = (TextView) findViewById(R.id.txt_SettingSOAPNamespace);
        TxtUrlUpdater       = (TextView) findViewById(R.id.txt_SettingURLUpdaters);

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);
        reload_setting(config);

        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        View.OnClickListener EHSaveListener = new View.OnClickListener() {
            public void onClick(View v) {
                // go to next activity
                save_setting(config);
                Toast.makeText(getApplicationContext(),"Setting Saved", Toast.LENGTH_LONG).show();
                reload_setting(config);
            }
        };

        View.OnClickListener EHTestConnListener = new View.OnClickListener() {
            public void onClick(View v) {
                test_connection();
                //Toast.makeText(getApplicationContext(),"Test Connection", Toast.LENGTH_LONG).show();
            }
        };

        CmdSaveSetting.setOnClickListener(EHSaveListener);
        CmdTestConn.setOnClickListener(EHTestConnListener);
    }


    public String test_connection(){
        String xfactor;
        String Message;
        Message = "Checking Connection ... Please Wait ...";

        xfactor = "S";
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        Log.d("[GudangGaram]", "Soap Address   : " + SSoapAddress);
        Log.d("[GudangGaram]", "Soap Operation : " + SSoapNamespace + SSoapMethodTest);

        try{
            HttpTransportSE httpTransport = new HttpTransportSE(SSoapAddress);
            httpTransport.debug = true;
            httpTransport.call( SSoapNamespace + SSoapMethodTest, envelope);
            httpTransport.setXmlVersionTag("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
        }
        catch(Exception ex){
            xfactor = ex.getMessage().toString();
            Toast.makeText(getApplicationContext(), ex.getMessage().toString(), Toast.LENGTH_LONG).show();
            Log.d("[GudangGaram]", "Catch : " + ex.getMessage().toString());
        }
        if(xfactor == "S"){
            Toast.makeText(getApplicationContext(), "Test Connection Succeed ", Toast.LENGTH_LONG).show();
        }
        return xfactor;
    }

    private void save_setting(SharedPreferences xconfig){
        SharedPreferences.Editor editor = xconfig.edit();
        editor.putString("SoapAddress",TxtSoapAddress.getText().toString());
        editor.putString("SoapNamespace",TxtSoapNamespace.getText().toString());
        editor.putString("URLUpdater",TxtUrlUpdater.getText().toString());
        editor.commit();
    }

    private void reload_setting(SharedPreferences xconfig){
        Log.d("[GudangGaram]", "SettingActivity : reload_Setting");

        SSoapAddress     = xconfig.getString("SoapAddress", "");
        SSoapNamespace   = xconfig.getString("SoapNamespace", "");
        SUrlUpdater      = xconfig.getString("URLUpdater","");

        TxtSoapNamespace.setText(SSoapNamespace);
        TxtSoapAddress.setText(SSoapAddress);
        TxtUrlUpdater.setText(SUrlUpdater);
    }
}
