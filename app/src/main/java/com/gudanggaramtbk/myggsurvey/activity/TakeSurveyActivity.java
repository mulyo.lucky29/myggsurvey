package com.gudanggaramtbk.myggsurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.Fragment;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.fragment.PReuse;
import com.gudanggaramtbk.myggsurvey.fragment.StartSurvey;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.model.StringWithTag;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;

import java.security.Key;
import java.util.ArrayList;
import java.util.List;

public class TakeSurveyActivity extends AppCompatActivity  {
    private MySQLiteHelper dbHelper;
    private Context ctx;
    private CEvent CParamEvent;
    private String StrSessionName;
    private String StrEventArea;
    private String StrDeviceID;
    private String StrSurveyTag;
    private Bundle eventParam;
    private Bundle encap_event_param;

    private FragmentManager manager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_survey);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ctx = this;
        dbHelper = new MySQLiteHelper(this);
        dbHelper.getWritableDatabase();

        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        // -------------- get session intent from GoToTakeSurveyActivity--------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
        eventParam      = getIntent().getBundleExtra("eventParam");

        // encapsulate Intent Info from TakeSurveyActivity into just only bundle
        encap_event_param = eventParam;
        encap_event_param.putString("PIC_NAME", StrSessionName);
        encap_event_param.putString("EVENT_AREA", StrEventArea );
        encap_event_param.putString("DeviceID", StrDeviceID);

        // temp bundle param into CEvent Class
        CParamEvent = new CEvent();
        CParamEvent.setInterviewerName(StrSessionName);
        CParamEvent.setEventSurveyTag(eventParam.getString("strSurveyTag"));
        CParamEvent.setEventID(eventParam.getString("strEventID"));
        CParamEvent.setEventName(eventParam.getString("strEventName"));
        CParamEvent.setEventDescription(eventParam.getString("strEventDesc"));
        CParamEvent.setEventStartDate(eventParam.getString("strStartDate"));
        CParamEvent.setEventEndDate(eventParam.getString("strEndDate"));
        CParamEvent.setAttribute1(eventParam.getString("strAttribute1"));
        CParamEvent.setAttribute2(eventParam.getString("strAttribute2"));
        CParamEvent.setAttribute3(eventParam.getString("strAttribute3"));
        CParamEvent.setAttribute4(eventParam.getString("strAttribute4"));


        EH_CMD_START_SURVEY(CParamEvent);
    }

    public void EH_CMD_START_SURVEY(CEvent CParamEvent){
        Log.d("[GudangGaram]", "TakeSurveyActivity :: EH_CMD_START_SURVEY(" + CParamEvent.getEventSurveyTag() + ")");
        try{
            StartSurvey frag = new StartSurvey(dbHelper,CParamEvent);
            // passing bundle here to fragment startsurvey
            frag.setArguments(encap_event_param);
            manager = getSupportFragmentManager();
            transaction = manager.beginTransaction();
            transaction.add(R.id.fragment_main,frag,"StartSurvey");
            transaction.commit();
        }
        catch (Exception e){
            Log.d("[GudangGaram]", "TakeSurveyActivity :: EH_CMD_START_SURVEY(" + CParamEvent.getEventSurveyTag() + ") Exception : " + e.getMessage().toString());
        }
    }

    @Override
    public void onBackPressed() {
        // left blank to use override and do nothing to prevent back bottom navigation to be used
        //super.onBackPressed();
    }


    // need to be added if back to home
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "TakeSurveyActivity :: upPressed Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    Log.d("[GudangGaram]", "TakeSurveyActivity :: upPressed Called > a");
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                }
                else {
                    try {
                        // distinguish between back from Fragment PReuse or not, if from PReuse then the FP want to cancel the survey otherwise only back
                        Fragment currentFragment =  manager.findFragmentById(R.id.fragment_main);

                        if(currentFragment.getTag().equals("PReuse")){
                            // just do nothin to force user to submit the result
                            // open this comment if back button will cancel
                            //((PReuse) currentFragment).EH_CMD_CANCEL();
                        }
                        else if(currentFragment.getTag().equals("EndSurvey")){
                            // just do nothing to force user to submit the result
                        }
                        else{
                            Log.d("[GudangGaram]", "TakeSurveyActivity :: upPressed Called > b");
                            // passing intent login information before back to GoToTakeSurveyActivity
                            upIntent.putExtra("PIC_NAME", StrSessionName);
                            upIntent.putExtra("EVENT_AREA", StrEventArea);
                            upIntent.putExtra("eventParam", eventParam);
                            NavUtils.navigateUpTo(this, upIntent);
                        }
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]", "TakeSurveyActivity :: upPressed Exception : " + e.getMessage().toString());
                    }
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
