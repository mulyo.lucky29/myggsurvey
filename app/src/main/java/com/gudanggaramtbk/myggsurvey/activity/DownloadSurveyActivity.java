package com.gudanggaramtbk.myggsurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TabHost;

import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterEvent;
import com.gudanggaramtbk.myggsurvey.adapter.CustomListAdapterEventDel;
import com.gudanggaramtbk.myggsurvey.model.CEvent;
import com.gudanggaramtbk.myggsurvey.service.DownloadEvent;
import com.gudanggaramtbk.myggsurvey.service.DownloadSurveySet;
import com.gudanggaramtbk.myggsurvey.service.SendDatabaseTaskAsync;
import com.gudanggaramtbk.myggsurvey.service.SendEventDownloadLogTaskAsync;
import com.gudanggaramtbk.myggsurvey.util.MySQLiteHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DownloadSurveyActivity extends AppCompatActivity {
    private MySQLiteHelper dbHelper;
    private SharedPreferences config;
    private ListView listEvent;
    private ListView listSavedEvent;
    private Button CmdReload;
    private Button CmdReloadSaved;
    private List<CEvent> SelectedEvent;
    private CustomListAdapterEventDel adaptersvd;
    private CustomListAdapterEvent adapter;
    private DownloadSurveySet downloadSurvey;
    private DownloadEvent reloadEvent;
    private List<CEvent> lvi;
    private List<CEvent> lvi_saved;
    private Integer recordcount;
    private String StrSessionName;
    private String StrEventArea;
    private String StrDeviceID;
    private TabHost thost;
    private Context ctx;
    private DownloadSurveyActivity act;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download_survey);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        ctx      = this;
        act      = this;

        dbHelper = new MySQLiteHelper(ctx);
        dbHelper.getWritableDatabase();
        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);

        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        listEvent      = (ListView) findViewById(R.id.lvi_downloaded_event);
        listSavedEvent = (ListView) findViewById(R.id.lvi_saved_event);
        CmdReload      = (Button)   findViewById(R.id.cmd_evt_select_reload);
        CmdReloadSaved = (Button)   findViewById(R.id.cmd_evt_save_reload);

        listEvent.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        listEvent.setClickable(true);

        thost = (TabHost)findViewById(R.id.TabDownEvent);
        thost.setup();
        TabHost.TabSpec spec = thost.newTabSpec("Download Wizard");
        try{
            //Tab 1
            spec.setContent(R.id.MTabExist);
            spec.setIndicator("Existing Event List");
            thost.addTab(spec);
            //Tab 2
            spec = thost.newTabSpec("Downloaded Asset");
            spec.setContent(R.id.MTabDown);
            spec.setIndicator("Downloaded Event");
            thost.addTab(spec);

            thost.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
                @Override
                public void onTabChanged(String tabId) {
                    int tab = thost.getCurrentTab();
                    Log.d("[GudangGaram]", "DownloadSurveyActivity :: TabSelected[" + tab + "]");
                    if(tab == 1){
                        EH_CMD_RELOAD_LIST_DOWNLOADED();
                    }
                }
            });
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "DownloadSurveyActivity Exception : " + e.getMessage().toString());
        }

        CmdReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_RELOAD_LIST();
            }
        });

        CmdReloadSaved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_RELOAD_LIST_DOWNLOADED();
            }
        });
    }

    private void EH_CMD_RELOAD_LIST(){
        Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_RELOAD_LIST");
        try{
            // ---------- init service for active event from server -----------
            reloadEvent = new DownloadEvent(config, ctx,dbHelper);
            reloadEvent.setParentActivity(this);
            reloadEvent.Retrieve(StrEventArea.trim(), StrDeviceID);
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_RELOAD_LIST Exception : " + e.getMessage());
        }
    }

    private void EH_CMD_RELOAD_LIST_DOWNLOADED(){
        Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_RELOAD_LIST_DOWNLOADED");
        try{
            lvi_saved = dbHelper.getListEvent(StrEventArea,"ALL");
            recordcount = lvi_saved.size();
            if(recordcount > 0){
                adaptersvd = new CustomListAdapterEventDel(ctx,dbHelper,lvi_saved);
                adaptersvd.setParentAtivity(this);
                adaptersvd.notifyDataSetChanged();
                listSavedEvent.setAdapter(adaptersvd);
            }
            else{
                listSavedEvent.setAdapter(null);
                //msg.Show("Populate","Reload Return Zero Result");
            }
        }
        catch(Exception e){
            Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_RELOAD_LIST_DOWNLOADED Exception : " + e.getMessage());
        }
    }

    public List<CEvent> intersection(List<CEvent> listfromdb, List<CEvent> listfromserver) {
        List<CEvent> list = new ArrayList<CEvent>();
        if(listfromdb.size() > 0){
            for (CEvent t : listfromserver) {
                if(!listfromdb.contains(t)) {
                    Log.d("[GudangGaram]", "DownloadSurveyActivity :: intersection : ");
                    list.add(t);
                }
            }
        }
        else{
            list = listfromserver;
        }

        return list;
    }

    public void EH_CMD_REFRESH(List<CEvent> olvi){
        List<CEvent> list_not_yet_downloaded;
        List<CEvent> list_from_server;
        List<CEvent> list_from_db;

        list_from_server        = olvi;
        list_from_db            = dbHelper.getListEvent(StrEventArea,"ALL");
        //list_not_yet_downloaded = intersection(list_from_db,list_from_server);

        Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_REFRESH  list_from_db     : " + list_from_db.size());
        Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_REFRESH  list_from_server : " + list_from_server.size());
        //Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_REFRESH  list_intersect   : " + list_not_yet_downloaded.size());

        try
        {
            recordcount = list_from_server.size();
            if(recordcount > 0){
                adapter = new CustomListAdapterEvent(ctx, dbHelper, list_from_server,"ActiveEvent");
                adapter.setParentAtivity(this);
                adapter.notifyDataSetChanged();
                listEvent.setAdapter(adapter);
            }
            else{
                listEvent.setAdapter(null);
                //msg.Show("Populate","Reload Return Zero Result");
            }
        }
        catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_RELOAD_LIST Exception :" + e.getMessage().toString());
            e.printStackTrace();
        }
    }

    // event called from outer
    public void EH_CMD_DELETE_EVENT_SAVED(final String StrEventID){
        Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_DELETE_EVENT_SAVED EventID = " + StrEventID);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ctx);
        alertDialogBuilder
                .setTitle("Delete Event ID [" + StrEventID + "] Confirm ")
                .setMessage("Are You Sure Want To Continue To Delete This Event ?")
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dbHelper.DeleteEvent(StrEventID);
                                EH_CMD_RELOAD_LIST_DOWNLOADED();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void EH_CMD_DOWNLOAD_SURVEY_SET(String strEventID, final String strSurveyTag){
        Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_DOWNLOAD_SURVEY_SET");
        try {
            // call WS To send Database
            SendEventDownloadLogTaskAsync SoapRequest = new SendEventDownloadLogTaskAsync(new SendEventDownloadLogTaskAsync.SendEventDownloadLogTaskAsyncResponse() {
                @Override
                public void PostSentAction(String output) {
                    Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_DOWNLOAD_SURVEY_SET : DownloadSurveySet");
                    // continue to download survey
                    try{
                        downloadSurvey = new DownloadSurveySet(config, ctx, dbHelper);
                        downloadSurvey.setParentActivity(act);
                        downloadSurvey.Download(strSurveyTag, StrDeviceID);
                    }
                    catch(Exception e){
                        Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_DOWNLOAD_SURVEY_SET : DownloadSurveySet Exception : " + e.getMessage().toString());
                    }
                }
            });
            SoapRequest.setAttribute(ctx, dbHelper, config, StrDeviceID, strEventID);
            if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB){
                //work on sgs3 android 4.0.4
                //SoapRequest.executeOnExecutor( AsyncTask.THREAD_POOL_EXECUTOR);
                Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_DOWNLOAD_SURVEY_SET : Send Using HC Higer");
                SoapRequest.executeOnExecutor( AsyncTask.SERIAL_EXECUTOR);
            }
            else {
                Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_DOWNLOAD_SURVEY_SET : Send Using HC Lower");
                SoapRequest.execute(); // work on sgs2 android 2.3
            }
        } catch (Exception e) {
            Log.d("[GudangGaram]", "DownloadSurveyActivity :: EH_CMD_DOWNLOAD_SURVEY_SET Exception :" + e.getMessage().toString());
        }
    }

    // need to be added if back to home
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "upPressed Setting Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                } else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    upIntent.putExtra("EVENT_AREA", StrEventArea);
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // left blank to use override and do nothing to prevent back bottom navigation to be used
    }

}