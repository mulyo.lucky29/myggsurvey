package com.gudanggaramtbk.myggsurvey.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import com.gudanggaramtbk.myggsurvey.R;
import com.gudanggaramtbk.myggsurvey.util.Logsutil;

public class LogcatActivity extends AppCompatActivity {
    SharedPreferences config;
    Button      CmdCloseLog;
    Button      CmdReloadLog;
    TextView    TxtLogValue;
    String      StrSessionName;
    String      StrEventArea;
    String      StrDeviceID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logcat);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        CmdReloadLog     = (Button)findViewById(R.id.cmd_reloadLog);
        TxtLogValue      = (TextView) findViewById(R.id.txt_log_vlue);
        TxtLogValue.setMovementMethod(new ScrollingMovementMethod());

        config = getSharedPreferences(getString(R.string.app_config), MODE_PRIVATE);

        // -------------- get session intent --------------
        StrSessionName  = getIntent().getStringExtra("PIC_NAME");
        StrEventArea    = getIntent().getStringExtra("EVENT_AREA");
        StrDeviceID     = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        CmdReloadLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EH_CMD_Reload_Log();
            }
        });
    }

    public void EH_CMD_Reload_Log(){
        Log.d("[GudangGaram]", "LogcatActivity :: reload_log");
        String strLogValue;
        try{
            TxtLogValue.setText("");
            Logsutil logcat = new Logsutil();
            strLogValue = logcat.readLogs().toString();
            TxtLogValue.setText(strLogValue);
        }
        catch(Exception e){
        }
    }

    // need to be added if back to home
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d("[GudangGaram]", "LogcatActivity :: upPressed Setting Page Called");
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent upIntent = NavUtils.getParentActivityIntent(this);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    TaskStackBuilder.create(this)
                            .addNextIntentWithParentStack(upIntent)
                            .startActivities();
                }
                else {
                    // passing intent login information before back
                    upIntent.putExtra("PIC_NAME", StrSessionName);
                    upIntent.putExtra("EVENT_AREA", StrEventArea);
                    NavUtils.navigateUpTo(this, upIntent);
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
