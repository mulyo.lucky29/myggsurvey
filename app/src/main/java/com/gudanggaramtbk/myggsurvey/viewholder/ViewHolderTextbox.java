package com.gudanggaramtbk.myggsurvey.viewholder;

import android.graphics.Typeface;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.textfield.TextInputLayout;
import com.gudanggaramtbk.myggsurvey.R;

public class ViewHolderTextbox extends RecyclerView.ViewHolder {
    public EditText answerSet;

    public ViewHolderTextbox(View view) {
        super(view);
        Log.d("[GudangGaram]", "CustomListAdapterTextbox :: ViewHolder");
        answerSet = (EditText) view.findViewById(R.id.txt_textbox);
        answerSet.setTextSize(24);
    }

    public ViewHolderTextbox(View view, String strValidation){
        super(view);
        Log.d("[GudangGaram]", "CustomListAdapterTextbox :: ViewHolder");
        answerSet = (EditText) view.findViewById(R.id.txt_textbox);
        answerSet.setTextSize(24);
        if(strValidation.equals("TYPE_CLASS_NUMBER")){
            answerSet.setInputType((InputType.TYPE_CLASS_NUMBER));
        }
        else{
            answerSet.setInputType((InputType.TYPE_CLASS_TEXT));
        }
    }

    /*
     public static void enforceEditTextUseNumberOnly(EditText field) {
            Typeface existingTypeface = field.getTypeface();
            field.setInputType((InputType.TYPE_CLASS_NUMBER));
            field.setTransformationMethod(new NumericKeyBoardTransformationMethod());
            field.setTypeface(existingTypeface);
        }

      private static class NumericKeyBoardTransformationMethod extends PasswordTransformationMethod {
            @Override
            public CharSequence getTransformation(CharSequence source, View view) {
                return source;
            }
        }
    */
}
