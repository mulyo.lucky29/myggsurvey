package com.gudanggaramtbk.myggsurvey.viewholder;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;

public class ViewHolderRadioGroup extends RecyclerView.ViewHolder {
    public RadioGroup answerSet;
    public EditText answerText;

    private Context mcontext;

    public void setContext(Context ctx){
        mcontext = ctx;
    }
    public ViewHolderRadioGroup(View view) {
        super(view);
        Log.d("[GudangGaram]", "CustomListAdapterRadioGroup :: ViewHolder");
        answerSet  = (RadioGroup) view.findViewById(R.id.rg_answer_set);
        answerText = (EditText) view.findViewById(R.id.rg_textbox);

        /*
        answerSet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
              @Override
              public void onCheckedChanged(RadioGroup group, int checkedId)
              {
                  //Toast.makeText(mcontext, "Radio button clicked " + answerSet.getCheckedRadioButtonId(), Toast.LENGTH_SHORT).show();
              }
        }
        );
        */
    }
}

