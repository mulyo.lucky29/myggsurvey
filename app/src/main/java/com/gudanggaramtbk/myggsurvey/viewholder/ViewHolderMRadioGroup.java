package com.gudanggaramtbk.myggsurvey.viewholder;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;

public class ViewHolderMRadioGroup extends RecyclerView.ViewHolder {
public RadioGroup answeroption;
public TextView answercaption;
public LinearLayout option_title;


    public ViewHolderMRadioGroup(View view) {
        super(view);
        Log.d("[GudangGaram]", "CustomListAdapterMRadioGroup :: ViewHolder");
        answercaption = (TextView) view.findViewById(R.id.mrg_MQuestionLabel);
        answeroption  = (RadioGroup) view.findViewById(R.id.mrg_answer_set);
        //option_title  = (LinearLayout) view.findViewById(R.id.mrg_judul_option);

        answeroption.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                //Toast.makeText(mcontext, "Radio button clicked " + answerSet.getCheckedRadioButtonId(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
