package com.gudanggaramtbk.myggsurvey.viewholder;

import android.widget.TextView;

public class ViewHolderMainSummary {
    public TextView Txt_mns_eventid;
    public TextView Txt_mns_eventname;
    public TextView Txt_mns_event_date;
    public TextView Txt_mns_rec_val1;
    public TextView Txt_mns_rec_val2;
    public TextView Txt_mns_rec_val3;
    public TextView Txt_mns_rec_val4;
}
