package com.gudanggaramtbk.myggsurvey.viewholder;

import android.widget.ImageButton;
import android.widget.TextView;

public class ViewHolderEvent {
    public TextView Txt_evt_eventid;
    public TextView Txt_evt_eventname;
    public TextView Txt_evt_eventdesc;
    public TextView Txt_evt_event_startdate;
    public TextView Txt_evt_event_enddate;
    public TextView Txt_evt_surveytag;
    public TextView Txt_evt_attribute1;
    public TextView Txt_evt_attribute2;
    public TextView Txt_evt_attribute3;
    public TextView Txt_evt_attribute4;
    public ImageButton Cmd_evt_download;
    public ImageButton Cmd_evt_delete;

}
