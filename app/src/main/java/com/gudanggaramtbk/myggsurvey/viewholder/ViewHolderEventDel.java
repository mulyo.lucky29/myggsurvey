package com.gudanggaramtbk.myggsurvey.viewholder;

import android.widget.ImageButton;
import android.widget.TextView;

public class ViewHolderEventDel {
    public TextView Txt_evtdel_eventid;
    public TextView Txt_evtdel_eventname;
    public TextView Txt_evtdel_eventdesc;
    public TextView Txt_evtdel_event_startdate;
    public TextView Txt_evtdel_event_enddate;
    public TextView Txt_evtdel_surveytag;
    public TextView Txt_evtdel_attribute1;
    public TextView Txt_evtdel_attribute2;
    public TextView Txt_evtdel_attribute3;
    public TextView Txt_evtdel_attribute4;
    public ImageButton Cmd_evtdel_delete;
}
