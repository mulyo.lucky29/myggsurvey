package com.gudanggaramtbk.myggsurvey.viewholder;

import android.widget.ImageButton;
import android.widget.TextView;

public class ViewHolderEventSel {
    public TextView Txt_evtsel_eventid;
    public TextView Txt_evtsel_eventname;
    public TextView Txt_evtsel_eventdesc;
    public TextView Txt_evtsel_event_startdate;
    public TextView Txt_evtsel_event_enddate;
    public TextView Txt_evtsel_surveytag;
    public TextView Txt_evtsel_attribute1;
    public TextView Txt_evtsel_attribute2;
    public TextView Txt_evtsel_attribute3;
    public TextView Txt_evtsel_attribute4;
    public ImageButton Cmd_evtsel_goTo;
}