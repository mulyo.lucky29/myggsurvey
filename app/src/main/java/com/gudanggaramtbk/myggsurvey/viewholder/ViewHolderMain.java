package com.gudanggaramtbk.myggsurvey.viewholder;

import android.widget.CheckBox;
import android.widget.TextView;

public class ViewHolderMain {
    public CheckBox Chk;
    public TextView Txt_mn_eventid;
    public TextView txt_mn_surveytag;
    public TextView Txt_mn_session_id;
    public TextView Txt_mn_event_name;
    public TextView Txt_mn_survey_startdate;
    public TextView Txt_mn_survey_enddate;
    public TextView Txt_mn_survey_status;


}
