package com.gudanggaramtbk.myggsurvey.viewholder;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;

public class ViewHolderHRadioGroup extends RecyclerView.ViewHolder {
    public RadioGroup answerSet;
    public LinearLayout frameanswerSet;

    private Context mcontext;

    public void setContext(Context ctx){
        mcontext = ctx;
    }
    public ViewHolderHRadioGroup(View view) {
        super(view);
        Log.d("[GudangGaram]", "ViewHolderHRadioGroup");
        answerSet      = (RadioGroup) view.findViewById(R.id.hrg_answer_set);
        answerSet.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                                                 @Override
                                                 public void onCheckedChanged(RadioGroup group, int checkedId)
                                                 {
                                                 }
                                             }
        );
    }
}