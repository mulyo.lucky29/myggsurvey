package com.gudanggaramtbk.myggsurvey.viewholder;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import com.gudanggaramtbk.myggsurvey.R;

public class ViewHolderCheckBoxGroup extends RecyclerView.ViewHolder {
    public TextView  answerClass;
    public CheckBox  answerSelect;
    public EditText  answerText;
    public ImageView answerImage;


    public ViewHolderCheckBoxGroup(View view) {
        super(view);
        Log.d("[GudangGaram]", "ViewHolderCheckBoxGroup :: ViewHolder");
        answerClass  = (TextView)  view.findViewById(R.id.ck_classtext);
        answerSelect = (CheckBox)  view.findViewById(R.id.ck_chkbox);
        answerText   = (EditText)  view.findViewById(R.id.ck_textbox);
        answerImage  = (ImageView) view.findViewById(R.id.ck_image);

    }
}
